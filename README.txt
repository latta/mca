Aplikace pro tvorbu map z dat projektu OpenStreetMap
Martin Latta
xlatta00@stud.fit.vutbr.cz
Bakalarska prace 2013
---------------------


Preklad:
        - make 

Spusteni:
        - make run

Vycisteni:
        - make clean
        
Dokumentace:
        - make doxygen
        
        

Potrebne zavisloti:

Ke spravnemu prekladu aplikace je nutne mit nainstalovanou knihovnu 
Mapnik (https://github.com/mapnik/mapnik/wiki/Mapnik-Installation)
alespon verzi >= 2.0.0. V pripadě kompilovani Mapniku primo ze 
zdrojoveho kodu je potřeba zajistit i instalaci pluginu osm potrebneho 
pro zakladni funkcnost (vhodne je take mit pluginy postgis a shape).
