# Aplikace pro tvorbu map
# Martin Latta
# xlatta00@stud.fit.vutbr.cz
# Bakalarska prace 2013


ibp:
	qmake -o src/Makefile src/BP.pro
	make -C src

run: 
	#src/BP
	cd src;	./BP `mapnik-config --input-plugins` `mapnik-config --fonts` 

clean:
	rm -rf src/*.o src/BP src/Makefile* src/moc_*.c* src/ui*.h doc/* src/*pro.user* src/tmp*
    
doxygen:
	doxygen Doxyfile
