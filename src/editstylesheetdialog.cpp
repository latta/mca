/**
  * @file editstylesheetdialog.cpp
  * @author Martin Latta
  * Map Creation Application from OpenStreetMap Project Data
  * Faculty of Information Technology
  * Brno University of Technology
  * 2013
  * @brief dialog to edit stylesheet
  */

#include "editstylesheetdialog.h"
#include "ui_editstylesheetdialog.h"

/**
 * @brief EditStylesheetDialog::EditStylesheetDialog constructor method
 * @param name stylesheet name
 * @param a stylesheet parameters
 * @param parent
 */
EditStylesheetDialog::EditStylesheetDialog(QString name, QHash<QString, QString> a, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::EditStylesheetDialog)
{
    ui->setupUi(this);

    this->setWindowTitle("Edit stylesheet");

    ui->lineEditName->setText(name);
    attribs = a;

    ui->lineEditBgColor->setText(a.value("background-color",QString()));
    ui->textEditSrs->setText(a.value("srs",QString()));
    ui->lineEditBgImage->setText(a.value("background-image",QString()));
    ui->lineEditFontDir->setText(a.value("font-directory",QString()));
    ui->lineEditMaxExt->setText(a.value("maximum-extent",QString()));


    connect(ui->pushButtonSrs, SIGNAL(clicked()),this, SLOT(selectSrs()));
    connect(ui->pushButtonBgImage, SIGNAL(clicked()),this, SLOT(browseFile()));
    connect(ui->pushButtonFontDir, SIGNAL(clicked()),this, SLOT(browseDir()));

    connect(ui->pushButtonColor, SIGNAL(clicked()), this, SLOT(colorEdit()));
    connect(this, SIGNAL(accepted()),this, SLOT(save()));
}

/**
 * @brief EditStylesheetDialog::~EditStylesheetDialog destructor
 */
EditStylesheetDialog::~EditStylesheetDialog()
{
    delete ui;
}

/**
 * @brief EditStylesheetDialog::save saves dialog content
 */
void EditStylesheetDialog::save()
{

    if (!ui->lineEditBgColor->text().isEmpty())
        attribs.insert("background-color",ui->lineEditBgColor->text());
    else
        attribs.remove("background-color");

    if (!ui->textEditSrs->toPlainText().isEmpty())
        attribs.insert("srs",ui->textEditSrs->toPlainText());
    else
        attribs.remove("srs");

    if (!ui->lineEditBgImage->text().isEmpty())
        attribs.insert("background-image",ui->lineEditBgImage->text());
    else
        attribs.remove("background-image");

    if (!ui->lineEditFontDir->text().isEmpty())
        attribs.insert("font-directory",ui->lineEditFontDir->text());
    else
        attribs.remove("font-directory");

    if (!ui->lineEditMaxExt->text().isEmpty())
        attribs.insert("maximum-extent",ui->lineEditMaxExt->text());
    else
        attribs.remove("maximum-extent");

}

/**
 * @brief EditStylesheetDialog::colorEdit handles color button click event
 */
void EditStylesheetDialog::colorEdit()
{
    QColor initColor;
    if (ui->lineEditBgColor->text() == "white")
        initColor=Qt::white;
    else
    {
        QString s=ui->lineEditBgColor->text();
        s.remove(0,4);
        s.remove(s.length()-1,1);
        if (s.split(",").count()!=3)
            initColor=Qt::white;
        else
        {
            initColor.setRed(s.split(",").at(0).toInt());
            initColor.setGreen(s.split(",").at(1).toInt());
            initColor.setBlue(s.split(",").at(2).toInt());
        }
    }
    QColor color = QColorDialog::getColor(initColor, this);
    if (color.isValid())
    {
        ui->lineEditBgColor->setText("rgb("+QString::number(color.red())+","+QString::number(color.green())+
                                     ","+QString::number(color.blue())+")");
    }
}

/**
 * @brief EditStylesheetDialog::browseFile handles browse file button click event
 */
void EditStylesheetDialog::browseFile()
{
    QString name = QFileDialog::getOpenFileName(this, tr("Choose file"), QDir::homePath()+"/",
                                                tr("Pictures (*.jpg *.jpeg *.gif *.png *.svg);;All files (*.*)"));
    if (!name.isEmpty())
    {
        ui->lineEditBgImage->setText(name);
    }
}

/**
 * @brief EditStylesheetDialog::browseDir handles browse directory button click event
 */
void EditStylesheetDialog::browseDir()
{
    QString name = QFileDialog::getExistingDirectory(this, tr("Choose folder"), QDir::homePath()+"/");
    if (!name.isEmpty())
    {
        ui->lineEditFontDir->setText(name);
    }
}

/**
 * @brief EditStylesheetDialog::selectSrs user can choose from default srs strings
 */
void EditStylesheetDialog::selectSrs()
{
    QStringList items;
    items << "+proj=merc +a=6378137 +b=6378137 +lat_ts=0.0 +lon_0=0.0 +x_0=0.0 +y_0=0 +k=1.0 +units=m +nadgrids=@null +no_defs +over";
    items << "+proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs";
    items << "+proj=merc +lon_0=0 +k=1 +x_0=0 +y_0=0 +ellps=WGS84 +datum=WGS84 +units=m +no_defs";


    bool ok;
    QString item = QInputDialog::getItem(this, tr("Select srs"),
                                         tr("Spatial reference system"), items, 0, false, &ok);
    if (ok && !item.isEmpty())
        ui->textEditSrs->setPlainText(item);
}
