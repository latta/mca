/**
  * @file stylexmlparser.cpp
  * @author Martin Latta
  * Map Creation Application from OpenStreetMap Project Data
  * Faculty of Information Technology
  * Brno University of Technology
  * 2013
  * @brief this class converts XML file into StyleSheet class representation and back
  */

#include "stylexmlparser.h"


/**
 * @brief StyleXmlParser::StyleXmlParser constructor method
 * @param styleWidget list of styles
 * @param layerWidget list of layers
 * @param slist StyleSheet class object
 * @param sname default stylesheet filename
 */
StyleXmlParser::StyleXmlParser(QListWidget *styleWidget, QListWidget *layerWidget, StyleSheet *slist, QString sname)
{
    styleListWidget = styleWidget;
    layerListWidget = layerWidget;

    styleSheet = slist;
    defaultStylesheetName=sname;

}

/**
 * @brief StyleXmlParser::readFile reads stylesheet file
 * @return true if successful
 */
bool StyleXmlParser::readFile()
{
    QFile tempfile(defaultStylesheetName);

    if (!tempfile.open(QIODevice::ReadOnly))
        return false;

    QString errmsg;
    int errline, errcol;

    if (!doc.setContent(&tempfile,&errmsg,&errline,&errcol))
    {
        tempfile.close();
        return false;
    }
    tempfile.close();

    root = doc.documentElement();
    if (root.tagName() != "Map")
    {
        std::cout << "Error: Not a mapnik stylesheet file" << std::endl;
        return false;
    }

    parseStyleSheetElements();
    return true;
}

/**
 * @brief StyleXmlParser::writeFile writes QDomDocument into file
 * @param filename name of the file
 * @return true if successful
 */
bool StyleXmlParser::writeFile(QString filename)
{
    QDomDocument document = updateStyleSheet();
    QFile tempfile(filename);

    if (!tempfile.open(QIODevice::WriteOnly))
        return false;

    QTextStream stream(&tempfile);
    document.save(stream,4);
    tempfile.close();

    return true;
}

/**
 * @brief StyleXmlParser::createFile creates a new stylesheet
 */
void StyleXmlParser::createFile()
{
    QDomDocument document;

    QDomNode xmlNode = document.createProcessingInstruction("xml", "version=\"1.0\" encoding=\"utf-8\"");
    document.appendChild(xmlNode);

    QDomElement elem = document.createElement("Map");
    elem.setAttribute("background-color", "#ffffff");
    elem.setAttribute("srs","+proj=latlong+datum=WGS84");
    document.appendChild(elem);

    QFile tempfile(defaultStylesheetName);

    if (!tempfile.open(QIODevice::WriteOnly))
        return;

    QTextStream stream(&tempfile);
    document.save(stream,4);
    tempfile.close();
}

/**
 * @brief StyleXmlParser::parseStyleSheetElements converts XML file to StyleSheet class
 */
void StyleXmlParser::parseStyleSheetElements()
{
    if (root.toElement().hasAttributes())
    {
        QDomNamedNodeMap attribs = root.toElement().attributes();
        for (int i=0; i < attribs.count(); i++)
        {
            styleSheet->attribs.insert(attribs.item(i).nodeName(), attribs.item(i).nodeValue());
        }
    }

    QDomNode node = root.firstChild();

    while (!node.isNull() && node.nodeName()!="Style")
    {
        // fonts
        node=node.nextSibling();
    }

    while (node.nodeName()=="Style")
    {
        QString stylename = node.attributes().namedItem("name").nodeValue();

        styleSheet->addStyle(node.toElement());

        QListWidgetItem *styleListWidgetItem = new QListWidgetItem(stylename,styleListWidget);
        styleListWidgetItem->setFlags(styleListWidgetItem->flags()|Qt::ItemIsUserCheckable);
        styleListWidgetItem->setCheckState(Qt::Checked);

        node=node.nextSibling();
    }

    while (node.nodeName()=="Layer")
    {
        QString layername = node.attributes().namedItem("name").nodeValue();

        styleSheet->addLayer(node.toElement());

        QListWidgetItem *layerListWidgetItem = new QListWidgetItem(layername,layerListWidget);
        layerListWidgetItem->setFlags(layerListWidgetItem->flags()|Qt::ItemIsUserCheckable);
        layerListWidgetItem->setCheckState(Qt::Checked);

        node=node.nextSibling();
    }
}


/**
 * @brief StyleXmlParser::updateStyleSheet
 * @return output QDomDocument
 */
QDomDocument StyleXmlParser::updateStyleSheet()
{
    QDomDocument document=doc.cloneNode().toDocument();

    if (document.isNull())
    {

    }

    QDomElement root = document.documentElement();

    foreach (const QString &s,styleSheet->attribs.keys())
    {
        root.setAttribute(s, styleSheet->attribs.value(s));
    }

    QDomNode node = root.firstChild();

    while (!node.isNull() && node.nodeName()!="Style")
    {
        // skip fonts
        node=node.nextSibling();
    }

    while (!node.isNull())
    {
        QDomNode nodeToRemove = node;
        node=node.nextSibling();
        root.removeChild(nodeToRemove);
    }

    for (int i=0; i<styleListWidget->count(); i++)
    {

        if (styleListWidget->item(i)->checkState() == Qt::Checked)
        {
            TStyleElement styleElem=styleSheet->getStyleElem(styleListWidget->item(i)->text());
            QDomElement elem = styleSheet->convertStyleToElement(styleElem,document);
            root.appendChild(elem);
        }
    }

    for (int i=0; i<layerListWidget->count(); i++)
    {

        if (layerListWidget->item(i)->checkState() == Qt::Checked)
        {
            TLayerElement layerElem=styleSheet->getLayerElem(layerListWidget->item(i)->text());
            QDomElement elem = styleSheet->convertLayerToElement(layerElem,document);
            root.appendChild(elem);
        }
    }
    return document;
}
