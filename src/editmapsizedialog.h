/**
  * @file editmapsizedialog.h
  * @author Martin Latta
  * Map Creation Application from OpenStreetMap Project Data
  * Faculty of Information Technology
  * Brno University of Technology
  * 2013
  * @brief dialog to adjust map size
  */

#ifndef EDITMAPSIZEDIALOG_H
#define EDITMAPSIZEDIALOG_H

#include <QDialog>

namespace Ui {
class EditMapSizeDialog;
}

/**
 * @brief The EditMapSizeDialog class representing dialog to adjust map size
 */
class EditMapSizeDialog : public QDialog
{
    Q_OBJECT
    
public:
    explicit EditMapSizeDialog(int w1, int h1, int a1 , QWidget *parent = 0);
    ~EditMapSizeDialog();

    int w; /**< map width */
    int h; /**< map height */
    int a; /**< map aspect fix mode */
    
private:
    Ui::EditMapSizeDialog *ui;
private slots:
    void save();

};

#endif // EDITMAPSIZEDIALOG_H
