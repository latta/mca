/**
  * @file editstyletextdialog.h
  * @author Martin Latta
  * Map Creation Application from OpenStreetMap Project Data
  * Faculty of Information Technology
  * Brno University of Technology
  * 2013
  * @brief dialog to edit TextSymbolizer
  */

#ifndef EDITSTYLETEXTDIALOG_H
#define EDITSTYLETEXTDIALOG_H

#include <QDialog>
#include <QtCore>
#include <QtGui>

namespace Ui {
class EditStyleTextDialog;
}

/**
 * @brief The EditStyleTextDialog class representing dialog to edit TextSymbolizer
 */
class EditStyleTextDialog : public QDialog
{
    Q_OBJECT
    
public:
    explicit EditStyleTextDialog(QHash<QString, QString> a, QWidget *parent = 0);
    ~EditStyleTextDialog();
    
    QHash<QString, QString> attribs; /**< all dialog paramaters */

private slots:
    void save() ;
    void colorFill();
    void colorHalo();

private:
    Ui::EditStyleTextDialog *ui;
};

#endif // EDITSTYLETEXTDIALOG_H
