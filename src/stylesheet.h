/**
  * @file stylesheet.h
  * @author Martin Latta
  * Map Creation Application from OpenStreetMap Project Data
  * Faculty of Information Technology
  * Brno University of Technology
  * 2013
  * @brief this class represents Mapnik XML file structure
  */

#ifndef STYLESHEET_H
#define STYLESHEET_H

#include <QtCore>
#include <QtGui>
#include <QDomDocument>
#include <iostream>

/**
 * @brief The classRule class represents single rule
 */
class classRule {
public:
    classRule();

    QString filter; /**< filter attribute */
    bool elseFilter; /**< elsefilter attribute */
    int maxScaleDenominator; /**< maxScaleDenominator attribute */
    int minScaleDenominator; /**< minScaleDenominator attribute */
    QHash<QString, QString> pointSymbolizer; /**< pointSymbolizer attributes */
    QString linePatternSymbolizer; /**< linePatternSymbolizer attribute */
    QString polygonPatternSymbolizer; /**< polygonPatternSymbolizer attribute */
    QHash<QString, QString> textSymbolizer; /**< textSymbolizer attributes */
    QHash<QString, QString> shieldSymbolizer; /**< shieldSymbolizer attributes */
    QHash<QString, QString> lineSymbolizer1; /**< lineSymbolizer1 attributes */
    QHash<QString, QString> lineSymbolizer2; /**< lineSymbolizer2 attributes */
    QHash<QString, QString> lineSymbolizer3; /**< lineSymbolizer3 attributes */
    QHash<QString, QString> polygonSymbolizer; /**< polygonSymbolizer attributes */
    QHash<QString, QString> buildingSymbolizer; /**< buildingSymbolizer attributes */
    QHash<QString, QString> markersSymbolizer; /**< markersSymbolizer attributes */
    bool debugSymbolizer; /**< debugSymbolizer attribute */
};

typedef struct tstyleelement {
    QString name; /**< style name */
    QList<classRule> rules; /**< list of rules */
} TStyleElement;

typedef struct tlayerelement {
    QHash<QString, QString> attributes; /**< layer attributes */
    QString name; /**< layer name */
    QList<QString> styleNames; /**< list od used styles */
    QHash<QString, QString> datasources; /**< list of datasources */
} TLayerElement;

/**
 * @brief The StyleSheet class represents Mapnik XML file structure
 */
class StyleSheet
{
public:
    StyleSheet();
    void clearAll();

    void addStyle(QDomElement domElem);
    void addStyle(QString name);
    void addLayer(QDomElement domElem);
    void addLayer(QString name);

    void modifyStyle(QString s, TStyleElement elem);
    void modifyLayer(QString s, TLayerElement elem);

    void removeStyle(QString s);
    void removeLayer(QString s);

    bool containsStyle(QString s);
    bool containsLayer(QString s);

    void moveLayerUp(int row);
    void moveLayerDown(int row);
    void moveStyleUp(int row);
    void moveStyleDown(int row);

    QDomElement convertRuleToElement(classRule rule, QDomDocument doc);
    QDomElement convertStyleToElement(TStyleElement style, QDomDocument doc);
    QDomElement convertLayerToElement(TLayerElement layer, QDomDocument doc);

    TStyleElement getStyleElem(QString s);
    TLayerElement getLayerElem(QString s);

    QList<TStyleElement> styleList; /**< list of styles */
    QList<TLayerElement> layerList; /**< list of layers */
    QHash<QString, QString> attribs; /**< stylesheet attributes */

};

#endif // STYLESHEET_H
