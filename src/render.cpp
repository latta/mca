/**
  * @file render.cpp
  * @author Martin Latta
  * Map Creation Application from OpenStreetMap Project Data
  * Faculty of Information Technology
  * Brno University of Technology
  * 2013
  * @brief this class renders image using Mapnik library
  */

#include "render.h"

/**
 * @brief Render::Render constructor method
 * @param mutex1 shared mutex
 * @param parent
 */
Render::Render(QMutex *mutex1, QObject *parent)
    : QThread(parent)
{

    mutex=mutex1;
}

/**
 * @brief Render::~Render destructor
 */
Render::~Render()
{
}

/**
 * @brief Render::render constructor method
 * @param fname default OSM filename
 * @param sname default stylesheet filename
 * @param ww map width
 * @param hh map height
 * @param aa map aspect fix mode
 * @param minlon1 minimal longitude
 * @param minlat1 minimal latitude
 * @param maxlon1 maximal longitude
 * @param maxlat1 maximal latitude
 * @param gridParams1 grid parameters
 */
void Render::render(QString fname, QString sname, int ww, int hh, int aa, double minlon1, double minlat1,
                    double maxlon1, double maxlat1, QHash<QString, QString> gridParams1)
{
    mutex->lock();

    defOsmFile=fname;
    defStyleFile=sname;
    w=ww;
    h=hh;
    a=aa;
    minlon=minlon1;
    minlat=minlat1;
    maxlon=maxlon1;
    maxlat=maxlat1;
    gridParams=gridParams1;

    mutex->unlock();
}

/**
 * @brief Render::run starts render thread
 */
void Render::run()
{
    QImage image;
    QString str="Render finished";

    mutex->lock();
    double minlon=this->minlon;
    double minlat=this->minlat;
    double maxlon=this->maxlon;
    double maxlat=this->maxlat;
    int w=this->w;
    int h=this->h;
    int a=this->a;
    QHash<QString, QString> gridParams=this->gridParams;

    mapnik::Map m;

    QString defStyleFile=this->defStyleFile;
    QString defOsmFile=this->defOsmFile;

    double scale=0;

    if (!gridParams.empty())
    {
        emit renStatus(tr("Generating grid"));
        generateGrid(gridParams, minlon, minlat, maxlon, maxlat);
    }
    else
    {
        QFile file("grid.osm");
        file.remove();
    }
    mutex->unlock();

    using namespace mapnik;

    minlon = (minlon == -180.0) ? minlon+0.000001 : minlon;
    minlat = (minlat == -90.0) ? minlat+0.000001 : minlat;
    maxlon = (maxlon == 180.0) ? maxlon-0.000001 : maxlon;
    maxlat = (maxlat == 90.0) ? maxlat-0.000001 : maxlat;

    try {

        emit renStatus(tr("RENDERING ... please wait"));

        double merc = 1.0 / cos(minlat*PI/180.0);

        m.set_height(h);
        m.set_width(w);
        m.remove_all();
        m.set_aspect_fix_mode((Map::aspect_fix_mode)a);
        m.set_background(parse_color("white"));

        load_map(m, defStyleFile.toStdString());

        mapnik::projection prj(m.srs());

        prj.forward(minlon,minlat);
        prj.forward(maxlon,maxlat);
        box2d<double> default_extent=box2d<double>(minlon,minlat,maxlon,maxlat);

        QFile file("grid.osm");
        if (file.exists())
        {
            feature_type_style gridStyle;
            rule gridRule;
            stroke str;

            if (gridParams.value("color") == "black")
                str.set_color(color(0,0,0));
            else
            {
                QString s = gridParams.value("color");
                s.remove(0,4);
                s.remove(s.length()-1,1);
                if (s.split(",").count()!=3)
                    str.set_color(color(0,0,0));
                else
                {
                    str.set_color(color(s.split(",").at(0).toInt(), s.split(",").at(1).toInt(), s.split(",").at(2).toInt()));
                }
            }
            str.set_opacity(gridParams.value("opacity","1.0").toDouble());
            str.set_width(gridParams.value("width","1.0").toDouble());
            line_symbolizer gridLineSymbolizer(str);

            gridRule.append(gridLineSymbolizer);
            gridStyle.add_rule(gridRule);

            m.insert_style("grid", gridStyle);

            parameters p;
            p["type"]="osm";
            p["file"]="grid.osm";
            layer l("grid");
            l.set_maximum_extent(default_extent);
            l.set_srs("+proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs");
            l.set_datasource(datasource_cache::instance().create(p));
            l.add_style("grid");
            m.addLayer(l);
        }

        if (QFile(defOsmFile).exists())
        {
            parameters p_osm;
            p_osm["type"]="osm";
            p_osm["file"]=defOsmFile.toStdString();
            datasource_cache::instance().create(p_osm);
        }

        m.zoom_to_box(default_extent);

        int x= (int) m.scale_denominator();

        if (x !=0)
        {
            QString num= QString::number(x);
            int digcnt = floor(log10(abs(x)));
            x = (x / (int)pow(10,digcnt)) * (int)pow(10,digcnt);

            if (QString(num.at(1)).toInt() >= 5)
                x = x + (int)pow(10,digcnt)/2;

            double factor = (double)x / m.scale_denominator();

            m.zoom(factor);
        }

        scale=m.scale_denominator();

        image_32 buf(m.width(),m.height());
        agg_renderer<image_32> ren(m,buf);
        ren.apply();

        image = QImage((uchar*)buf.raw_data(),m.width(),m.height(),QImage::Format_ARGB32);
        image=image.rgbSwapped();

        double resolution=MAPNIK_PPI*merc;
        emit renFinished(image,str, resolution, scale);

    }
    catch ( const mapnik::config_error & ex )
    {
        std::cerr << "### Configuration error: " << ex.what() << std::endl;

        str=QString(ex.what());
        emit renFailed(str);
    }
    catch ( const std::exception & ex )
    {
        std::cerr << "### std::exception: " << ex.what() << std::endl;

        str=QString(ex.what());
        emit renFailed(str);
    }
    catch ( ... )
    {
        std::cerr << "### Unknown exception." << std::endl;
        emit renFailed("### Unknown exception.");
    }

}

/**
 * @brief Render::generateGrid generates grid
 * @param gridParams1 grid parameters
 * @param minlon minimal longitude
 * @param minlat minimal latitude
 * @param maxlon maximal longitude
 * @param maxlat maximal latitude
 */
void Render::generateGrid(QHash<QString, QString> gridParams, double minlon, double minlat, double maxlon, double maxlat)
{
    QFile file("grid.osm");
    if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
        return;

    if (minlon > 0)
        minlon = (int)((minlon - floor(minlon))*60)/60.0+(int)minlon;
    else
    {
        minlon = -minlon;
        minlon = ((int)((minlon - floor(minlon))*60)+1)/60.0+(int)minlon;
        minlon = -minlon;
    }

    if (minlat > 0)
        minlat = (int)((minlat - floor(minlat))*60)/60.0+(int)minlat;
    else
    {
        minlat = -minlat;
        minlat = ((int)((minlat - floor(minlat))*60)+1)/60.0+(int)minlat;
        minlat = -minlat;
    }

    if (maxlon > 0)
        maxlon = ((int)((maxlon - floor(maxlon))*60)+1)/60.0+(int)maxlon;
    else
    {
        maxlon = - maxlon;
        maxlon = (int)((maxlon - floor(maxlon))*60)/60.0+(int)maxlon;
        maxlon = - maxlon;
    }

    if (maxlat > 0)
        maxlat = ((int)((maxlat - floor(maxlat))*60)+1)/60.0+(int)maxlat;
    else
    {
        maxlat= - maxlat;
        maxlat = (int)((maxlat - floor(maxlat))*60)/60.0+(int)maxlat;
        maxlat= - maxlat;
    }

    double step=gridParams.value("deg").toDouble()+gridParams.value("min").toDouble()/60.0+gridParams.value("sec").toDouble()/3600.0;


    QTextStream stream(&file);
    stream << "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<osm version=\"0.6\">\n";

    stream << " <bounds minlat=\""<< minlat<<"\" minlon=\""<<minlon<<"\" maxlat=\""<<maxlat<<"\" maxlon=\""<<maxlon<<"\"/>\n";

    double sum= minlat;
    int nodeLatCount=0;
    int nodeLonCount=0;

    while (sum < maxlat)
    {
        stream << " <node id=\""<< nodeLatCount++ <<"\" lat=\""<<sum<<"\" lon=\""<< minlon <<"\" visible=\"true\" />\n";
        stream << " <node id=\""<< nodeLatCount++ <<"\" lat=\""<<sum<<"\" lon=\""<< maxlon <<"\" visible=\"true\" />\n";
        sum+=step;
    }

    sum= minlon;
    nodeLonCount = nodeLatCount;
    while (sum < maxlon)
    {
        stream << " <node id=\""<< nodeLonCount++ <<"\" lat=\""<<minlat<<"\" lon=\""<< sum <<"\" visible=\"true\" />\n";
        stream << " <node id=\""<< nodeLonCount++ <<"\" lat=\""<<maxlat<<"\" lon=\""<< sum <<"\" visible=\"true\" />\n";
        sum+=step;
    }

    int i=0;
    while (i < nodeLatCount)
    {
        stream << " <way id=\""<< i <<"\" visible=\"true\">\n";
        stream << "  <nd ref=\""<<i++ <<"\"/>\n";
        stream << "  <nd ref=\""<<i++ <<"\"/>\n";
        stream << " </way>\n";
    }
    while (i < nodeLonCount)
    {
        stream << " <way id=\""<< i <<"\" visible=\"true\">\n";
        stream << "  <nd ref=\""<<i++ <<"\"/>\n";
        stream << "  <nd ref=\""<<i++ <<"\"/>\n";
        stream << " </way>\n";
    }
    stream << "</osm>";

    file.close();
}
