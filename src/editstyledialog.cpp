/**
  * @file editstyledialog.cpp
  * @author Martin Latta
  * Map Creation Application from OpenStreetMap Project Data
  * Faculty of Information Technology
  * Brno University of Technology
  * 2013
  * @brief dialog to edit selected style
  */

#include "editstyledialog.h"
#include "ui_editstyledialog.h"


/**
 * @brief EditStyleDialog::EditStyleDialog constructor method
 * @param elem style element to edit
 * @param parent
 */
EditStyleDialog::EditStyleDialog(TStyleElement elem, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::EditStyleDialog)
{
    ui->setupUi(this);

    this->setWindowTitle("Edit style");

    element=elem;
    ruleCounter=0;

    currentRule=0;
    moving=false;

    ui->lineEditStyleName->setText(element.name);

    for (int i=0; i < element.rules.count(); i++)
    {
        ruleCounter++;
        QListWidgetItem *item= new QListWidgetItem("Rule "+QString::number(ruleCounter));
        ui->listWidget->addItem(item);
    }

    ui->groupBoxFilter->setChecked(false);
    ui->groupBoxElseFilter->setChecked(false);
    ui->groupBoxMinScaleDenominator->setChecked(false);
    ui->groupBoxMaxScaleDenominator->setChecked(false);
    ui->groupBoxPointSymbolizer->setChecked(false);
    ui->groupBoxLinePatternSymbolizer->setChecked(false);
    ui->groupBoxPolygonPatternSymbolizer->setChecked(false);
    ui->groupBoxTextSymbolizer->setChecked(false);
    ui->groupBoxShieldSymbolizer->setChecked(false);
    ui->groupBoxLineSymbolizer->setChecked(false);
    ui->groupBoxPolygonSymbolizer->setChecked(false);
    ui->groupBoxBuildingSymbolizer->setChecked(false);
    ui->groupBoxMarkersSymbolizer->setChecked(false);
    ui->groupBoxDebugSymbolizer->setChecked(false);

    if (ui->listWidget->count() !=0)
    {
        ui->listWidget->setCurrentRow(0);
        updateDialog();
    }

    connect(ui->lineEditStyleName, SIGNAL(textEdited(QString)), this, SLOT(styleNameEdited(QString)));
    connect(ui->pushButtonAdd, SIGNAL(clicked()), this, SLOT(addNewRule()));
    connect(ui->pushButtonRemove, SIGNAL(clicked()), this, SLOT(removeRule()));
    connect(ui->pushButtonSave, SIGNAL(clicked()), this, SLOT(saveRule()));
    connect(ui->listWidget, SIGNAL(itemSelectionChanged()), this, SLOT(currentSelectionChanged()));

    connect(ui->pushButtonUp, SIGNAL(clicked()),this, SLOT(moveUp()));
    connect(ui->pushButtonDown, SIGNAL(clicked()),this, SLOT(moveDown()));

    connect(ui->pushButtonPointSymbolizer, SIGNAL(clicked()), this, SLOT(browsePointSymbolizer()));
    connect(ui->pushButtonLinePatternSymbolizer, SIGNAL(clicked()), this, SLOT(browseLinePatternSymbolizer()));
    connect(ui->pushButtonPolygonPatternSymbolizer, SIGNAL(clicked()), this, SLOT(browsePolygonPatternSymbolizer()));

    connect(ui->pushButtonLineSymbolizer, SIGNAL(clicked()), this, SLOT(colorLineSymbolizer()));
    connect(ui->pushButtonLineSymbolizer2, SIGNAL(clicked()), this, SLOT(colorLineSymbolizer2()));
    connect(ui->pushButtonLineSymbolizer3, SIGNAL(clicked()), this, SLOT(colorLineSymbolizer3()));
    connect(ui->pushButtonPolygonSymbolizer, SIGNAL(clicked()), this, SLOT(colorPolygonSymbolizer()));
    connect(ui->pushButtonBuildingSymbolizer, SIGNAL(clicked()), this, SLOT(colorBuildingSymbolizer()));

    // edit in new dialog
    connect(ui->pushButtonTextSymbolizer, SIGNAL(clicked()), this, SLOT(editTextSymbolizer()));
    connect(ui->pushButtonShieldSymbolizer, SIGNAL(clicked()), this, SLOT(editShieldSymbolizer()));
    connect(ui->pushButtonMarkersSymbolizer, SIGNAL(clicked()), this, SLOT(editMarkersSymbolizer()));

    connect(this, SIGNAL(accepted()),this, SLOT(saveRule()));

}

/**
 * @brief EditStyleDialog::~EditStyleDialog destructor
 */
EditStyleDialog::~EditStyleDialog()
{
    delete ui;
}

/**
 * @brief EditStyleDialog::updateDialog updates dialog content
 */
void EditStyleDialog::updateDialog()
{
    ui->groupBoxFilter->setChecked(false);
    ui->groupBoxElseFilter->setChecked(false);
    ui->groupBoxMinScaleDenominator->setChecked(false);
    ui->groupBoxMaxScaleDenominator->setChecked(false);
    ui->groupBoxPointSymbolizer->setChecked(false);
    ui->groupBoxLinePatternSymbolizer->setChecked(false);
    ui->groupBoxPolygonPatternSymbolizer->setChecked(false);
    ui->groupBoxTextSymbolizer->setChecked(false);
    ui->groupBoxShieldSymbolizer->setChecked(false);
    ui->groupBoxLineSymbolizer->setChecked(false);
    ui->groupBoxPolygonSymbolizer->setChecked(false);
    ui->groupBoxBuildingSymbolizer->setChecked(false);
    ui->groupBoxMarkersSymbolizer->setChecked(false);
    ui->groupBoxDebugSymbolizer->setChecked(false);

    // Filter
    ui->lineEditFilter->clear();
    ui->lineEditMin->clear();
    ui->lineEditMax->clear();

    // PointSymbolizer
    ui->lineEditPointSymbolizer->clear();
    ui->lineEditPSplacement->clear();
    ui->doubleSpinBoxPointSymbolizer->setValue(1.0);
    ui->checkBoxPSallowoverlap->setChecked(false);
    ui->checkBoxPSignoreplacement->setChecked(false);

    // LinePatternSymbolizer
    ui->lineEditLinePatternSymbolizer->clear();

    // PolygonPatternSymbolizer
    ui->lineEditPolygonPatternSymbolizer->clear();

    ui->labelTextSymbolizer->setText("<font color='red'>(Not set)</font><");
    ui->labelShieldSymbolizer->setText("<font color='red'>(Not set)</font><");

    // LineSymbolizer

    ui->tabWidgetLS->setCurrentIndex(0);

    ui->lineEditLineSymbolizer->setText("black");
    ui->doubleSpinBoxLSwidth->setValue(1.0);
    ui->doubleSpinBoxLSopacity->setValue(1.0);
    ui->comboBoxLSlinejoin->setCurrentIndex(0);
    ui->comboBoxLSlinecap->setCurrentIndex(0);
    ui->lineEditLSdasharray->clear();

    ui->lineEditLineSymbolizer2->setText("");
    ui->doubleSpinBoxLSwidth2->setValue(1.0);
    ui->doubleSpinBoxLSopacity2->setValue(1.0);
    ui->comboBoxLSlinejoin2->setCurrentIndex(0);
    ui->comboBoxLSlinecap2->setCurrentIndex(0);
    ui->lineEditLSdasharray2->clear();

    ui->lineEditLineSymbolizer3->setText("");
    ui->doubleSpinBoxLSwidth3->setValue(1.0);
    ui->doubleSpinBoxLSopacity3->setValue(1.0);
    ui->comboBoxLSlinejoin3->setCurrentIndex(0);
    ui->comboBoxLSlinecap3->setCurrentIndex(0);
    ui->lineEditLSdasharray3->clear();

    // PolygonSymbolizer
    ui->lineEditPolygonSymbolizer->setText("gray");
    ui->doubleSpinBoxPSopacity->setValue(1.0);
    ui->doubleSpinBoxPSgamma->setValue(1.0);

    // BuildingSymbolizer
    ui->lineEditBuildingSymbolizer->setText("gray");
    ui->doubleSpinBoxBSopacity->setValue(1.0);
    ui->doubleSpinBoxBSheight->setValue(0.0);

    // MarkersSymbolizer
    ui->labelMarkersSymbolizer->setText("<font color='red'>(Not set)</font><");


    if (ui->listWidget->selectedItems().count() >1)
        return;
    if (ui->listWidget->count()==0)
        return;

    if (ui->listWidget->selectedItems().count() == 0)
        ui->listWidget->setCurrentRow(0);

    currentRule=ui->listWidget->currentRow();

    classRule rule=element.rules.at(ui->listWidget->currentRow());

    textHash=rule.textSymbolizer;
    shieldHash=rule.shieldSymbolizer;
    markersHash=rule.markersSymbolizer;


    //########## Filter
    if (rule.filter != "")
    {
        ui->groupBoxFilter->setChecked(true);
        ui->lineEditFilter->setText(rule.filter);
    }

    //########## ElseFilter
    if (rule.elseFilter != false)
    {
        ui->groupBoxElseFilter->setChecked(true);
    }

    //########## MinScaleDenominator
    if (rule.minScaleDenominator != 0)
    {
        ui->groupBoxMinScaleDenominator->setChecked(true);
        ui->lineEditMin->setText(QString::number(rule.minScaleDenominator));
    }

    //########## MaxScaleDenominator
    if (rule.maxScaleDenominator != 0)
    {
        ui->groupBoxMaxScaleDenominator->setChecked(true);
        ui->lineEditMax->setText(QString::number(rule.maxScaleDenominator));
    }


    //########## PointSymbolizer
    if (!rule.pointSymbolizer.isEmpty())
    {
        ui->groupBoxPointSymbolizer->setChecked(true);

        if (rule.pointSymbolizer.contains("file"))
            ui->lineEditPointSymbolizer->setText(rule.pointSymbolizer.value("file"));

        if (rule.pointSymbolizer.contains("placement"))
            ui->lineEditPSplacement->setText(rule.pointSymbolizer.value("placement"));

        if (rule.pointSymbolizer.value("allow-overlap") == "true")
            ui->checkBoxPSallowoverlap->setChecked(true);
        if (rule.pointSymbolizer.value("ignore-placement") == "true")
            ui->checkBoxPSignoreplacement->setChecked(true);

        if (rule.pointSymbolizer.contains("opacity"))
            ui->doubleSpinBoxPointSymbolizer->setValue(rule.pointSymbolizer.value("opacity").toDouble());
    }

    //########## LinePatternSymbolizer
    if (!rule.linePatternSymbolizer.isEmpty())
    {
        ui->groupBoxLinePatternSymbolizer->setChecked(true);
        ui->lineEditLinePatternSymbolizer->setText(rule.linePatternSymbolizer);
    }

    //########## PolygonPatternSymbolizer
    if (rule.polygonPatternSymbolizer != "")
    {
        ui->groupBoxPolygonPatternSymbolizer->setChecked(true);
        ui->lineEditPolygonPatternSymbolizer->setText(rule.polygonPatternSymbolizer);
    }

    //########## TextSymbolizer
    if (!rule.textSymbolizer.isEmpty())
    {
        ui->groupBoxTextSymbolizer->setChecked(true);
        ui->labelTextSymbolizer->setText("<font color='green'>(is set)</font><");
    }

    //########## ShieldSymbolizer
    if (!rule.shieldSymbolizer.isEmpty())
    {
        ui->groupBoxShieldSymbolizer->setChecked(true);
        ui->labelShieldSymbolizer->setText("<font color='green'>(is set)</font><");
    }

    //########## LineSymbolizer
    if (!rule.lineSymbolizer1.isEmpty())
    {
        ui->groupBoxLineSymbolizer->setChecked(true);

        if (rule.lineSymbolizer1.contains("stroke"))
            ui->lineEditLineSymbolizer->setText(rule.lineSymbolizer1.value("stroke"));

        if (rule.lineSymbolizer1.contains("stroke-width"))
            ui->doubleSpinBoxLSwidth->setValue(rule.lineSymbolizer1.value("stroke-width").toDouble());
        if (rule.lineSymbolizer1.contains("stroke-opacity"))
            ui->doubleSpinBoxLSopacity->setValue(rule.lineSymbolizer1.value("stroke-opacity").toDouble());

        if (rule.lineSymbolizer1.contains("stroke-linejoin"))
        {
            QString s=rule.lineSymbolizer1.value("stroke-linejoin");
            if (s == "miter")
                ui->comboBoxLSlinejoin->setCurrentIndex(0);
            else if (s == "round")
                ui->comboBoxLSlinejoin->setCurrentIndex(1);
            else if (s == "bevel")
                ui->comboBoxLSlinejoin->setCurrentIndex(2);
        }

        if (rule.lineSymbolizer1.contains("stroke-linecap"))
        {
            QString s=rule.lineSymbolizer1.value("stroke-linecap");
            if (s == "butt")
                ui->comboBoxLSlinecap->setCurrentIndex(0);
            else if (s == "round")
                ui->comboBoxLSlinecap->setCurrentIndex(1);
            else if (s == "square")
                ui->comboBoxLSlinecap->setCurrentIndex(2);
        }

        if (rule.lineSymbolizer1.contains("stroke-dasharray"))
            ui->lineEditLSdasharray->setText(rule.lineSymbolizer1.value("stroke-dasharray"));
    }

    if (!rule.lineSymbolizer2.isEmpty())
    {
        ui->groupBoxLineSymbolizer->setChecked(true);

        if (rule.lineSymbolizer2.contains("stroke"))
            ui->lineEditLineSymbolizer2->setText(rule.lineSymbolizer2.value("stroke"));

        if (rule.lineSymbolizer2.contains("stroke-width"))
            ui->doubleSpinBoxLSwidth2->setValue(rule.lineSymbolizer2.value("stroke-width").toDouble());
        if (rule.lineSymbolizer2.contains("stroke-opacity"))
            ui->doubleSpinBoxLSopacity2->setValue(rule.lineSymbolizer2.value("stroke-opacity").toDouble());

        if (rule.lineSymbolizer2.contains("stroke-linejoin"))
        {
            QString s=rule.lineSymbolizer2.value("stroke-linejoin");
            if (s == "miter")
                ui->comboBoxLSlinejoin2->setCurrentIndex(0);
            else if (s == "round")
                ui->comboBoxLSlinejoin2->setCurrentIndex(1);
            else if (s == "bevel")
                ui->comboBoxLSlinejoin2->setCurrentIndex(2);
        }

        if (rule.lineSymbolizer2.contains("stroke-linecap"))
        {
            QString s=rule.lineSymbolizer2.value("stroke-linecap");
            if (s == "butt")
                ui->comboBoxLSlinecap2->setCurrentIndex(0);
            else if (s == "round")
                ui->comboBoxLSlinecap2->setCurrentIndex(1);
            else if (s == "square")
                ui->comboBoxLSlinecap2->setCurrentIndex(2);
        }

        if (rule.lineSymbolizer2.contains("stroke-dasharray"))
            ui->lineEditLSdasharray2->setText(rule.lineSymbolizer2.value("stroke-dasharray"));
    }

    if (!rule.lineSymbolizer3.isEmpty())
    {
        ui->groupBoxLineSymbolizer->setChecked(true);

        if (rule.lineSymbolizer3.contains("stroke"))
            ui->lineEditLineSymbolizer3->setText(rule.lineSymbolizer3.value("stroke"));

        if (rule.lineSymbolizer3.contains("stroke-width"))
            ui->doubleSpinBoxLSwidth3->setValue(rule.lineSymbolizer3.value("stroke-width").toDouble());
        if (rule.lineSymbolizer3.contains("stroke-opacity"))
            ui->doubleSpinBoxLSopacity3->setValue(rule.lineSymbolizer3.value("stroke-opacity").toDouble());

        if (rule.lineSymbolizer3.contains("stroke-linejoin"))
        {
            QString s=rule.lineSymbolizer3.value("stroke-linejoin");
            if (s == "miter")
                ui->comboBoxLSlinejoin3->setCurrentIndex(0);
            else if (s == "round")
                ui->comboBoxLSlinejoin3->setCurrentIndex(1);
            else if (s == "bevel")
                ui->comboBoxLSlinejoin3->setCurrentIndex(2);
        }

        if (rule.lineSymbolizer3.contains("stroke-linecap"))
        {
            QString s=rule.lineSymbolizer3.value("stroke-linecap");
            if (s == "butt")
                ui->comboBoxLSlinecap3->setCurrentIndex(0);
            else if (s == "round")
                ui->comboBoxLSlinecap3->setCurrentIndex(1);
            else if (s == "square")
                ui->comboBoxLSlinecap3->setCurrentIndex(2);
        }

        if (rule.lineSymbolizer3.contains("stroke-dasharray"))
            ui->lineEditLSdasharray3->setText(rule.lineSymbolizer3.value("stroke-dasharray"));

    }

    //########## PolygonSymbolizer
    if (!rule.polygonSymbolizer.isEmpty())
    {
        ui->groupBoxPolygonSymbolizer->setChecked(true);

        if (rule.polygonSymbolizer.contains("fill"))
            ui->lineEditPolygonSymbolizer->setText(rule.polygonSymbolizer.value("fill"));

        if (rule.polygonSymbolizer.contains("fill-opacity"))
            ui->doubleSpinBoxPSopacity->setValue(rule.polygonSymbolizer.value("fill-opacity").toDouble());
        if (rule.polygonSymbolizer.contains("gamma"))
            ui->doubleSpinBoxPSgamma->setValue(rule.polygonSymbolizer.value("gamma").toDouble());
    }

    //########## BuildingSymbolizer
    if (!rule.buildingSymbolizer.isEmpty())
    {
        ui->groupBoxBuildingSymbolizer->setChecked(true);

        if (rule.buildingSymbolizer.contains("fill"))
            ui->lineEditBuildingSymbolizer->setText(rule.buildingSymbolizer.value("fill"));

        if (rule.buildingSymbolizer.contains("fill-opacity"))
            ui->doubleSpinBoxBSopacity->setValue(rule.buildingSymbolizer.value("fill-opacity").toDouble());
        if (rule.buildingSymbolizer.contains("height"))
            ui->doubleSpinBoxBSheight->setValue(rule.buildingSymbolizer.value("height").toDouble());
    }


    //########## MarkersSymbolizer
    if (!rule.markersSymbolizer.isEmpty())
    {
        ui->groupBoxMarkersSymbolizer->setChecked(true);
        ui->labelMarkersSymbolizer->setText("<font color='green'>(is set)</font><");
    }


    //########## DebugSymbolizer
    if (rule.debugSymbolizer != false)
    {
        ui->groupBoxDebugSymbolizer->setChecked(true);
    }

}

/**
 * @brief EditStyleDialog::styleNameEdited edits style name
 * @param s new style name
 */
void EditStyleDialog::styleNameEdited(QString s)
{
    element.name=s;
}

/**
 * @brief EditStyleDialog::addNewRule adds new rule
 */
void EditStyleDialog::addNewRule()
{
    ruleCounter++;
    QListWidgetItem *item= new QListWidgetItem("Rule "+QString::number(ruleCounter));

    ui->listWidget->addItem(item);

    classRule rule;
    element.rules.append(rule);

    ui->listWidget->setCurrentRow(ui->listWidget->count() - 1);
    currentRule=ui->listWidget->currentRow();
}

/**
 * @brief EditStyleDialog::removeRule remove selected rule
 */
void EditStyleDialog::removeRule()
{
    if(ui->listWidget->count() == 0)
        return;
    if (ui->listWidget->selectedItems().count() !=1)
        return;

    int row=ui->listWidget->currentRow();

    ui->listWidget->takeItem(row);
    element.rules.removeAt(row);

    currentRule=ui->listWidget->currentRow();
    updateDialog();
}

/**
 * @brief EditStyleDialog::saveRule saves dialog content into selected rule
 */
void EditStyleDialog::saveRule()
{
    if (ui->listWidget->selectedItems().count() !=1)
        return;

    classRule rule;

    if (ui->groupBoxFilter->isChecked())
    {
        if (!ui->lineEditFilter->text().isEmpty())
            rule.filter=ui->lineEditFilter->text();
    }

    if (ui->groupBoxElseFilter->isChecked())
    {
        rule.elseFilter=true;
    }

    if (ui->groupBoxMinScaleDenominator->isChecked())
    {
        if (!ui->lineEditMin->text().isEmpty())
            rule.minScaleDenominator = ui->lineEditMin->text().toInt();
    }

    if (ui->groupBoxMaxScaleDenominator->isChecked())
    {
        if (!ui->lineEditMax->text().isEmpty())
            rule.maxScaleDenominator = ui->lineEditMax->text().toInt();
    }

    if (ui->groupBoxPointSymbolizer->isChecked())
    {
        if (!ui->lineEditPointSymbolizer->text().isEmpty())
            rule.pointSymbolizer.insert("file",ui->lineEditPointSymbolizer->text());
        if (!ui->lineEditPSplacement->text().isEmpty())
            rule.pointSymbolizer.insert("placement",ui->lineEditPointSymbolizer->text());
        if (ui->checkBoxPSallowoverlap->isChecked())
            rule.pointSymbolizer.insert("allow-overlap","true");
        if (ui->checkBoxPSignoreplacement->isChecked())
            rule.pointSymbolizer.insert("ignore-placement","true");
        if (ui->doubleSpinBoxPointSymbolizer->value()!=1.0)
            rule.pointSymbolizer.insert("opacity",QString::number(ui->doubleSpinBoxPointSymbolizer->value()));

    }

    if (ui->groupBoxLinePatternSymbolizer->isChecked())
    {
        if (!ui->lineEditLinePatternSymbolizer->text().isEmpty())
            rule.linePatternSymbolizer=ui->lineEditLinePatternSymbolizer->text();
    }

    if (ui->groupBoxPolygonPatternSymbolizer->isChecked())
    {
        if (!ui->lineEditPolygonPatternSymbolizer->text().isEmpty())
            rule.polygonPatternSymbolizer=ui->lineEditPolygonPatternSymbolizer->text();
    }

    if (ui->groupBoxTextSymbolizer->isChecked())
    {
        rule.textSymbolizer=textHash;
    }

    if (ui->groupBoxShieldSymbolizer->isChecked())
    {
        rule.shieldSymbolizer=shieldHash;
    }

    if (ui->groupBoxLineSymbolizer->isChecked())
    {
        if (!ui->lineEditLineSymbolizer->text().isEmpty())
            rule.lineSymbolizer1.insert("stroke",ui->lineEditLineSymbolizer->text());
        if (ui->doubleSpinBoxLSwidth->value()!=1.0)
            rule.lineSymbolizer1.insert("stroke-width",QString::number(ui->doubleSpinBoxLSwidth->value()));
        if (ui->doubleSpinBoxLSopacity->value()!=1.0)
            rule.lineSymbolizer1.insert("stroke-opacity",QString::number(ui->doubleSpinBoxLSopacity->value()));
        if (!ui->lineEditLSdasharray->text().isEmpty())
            rule.lineSymbolizer1.insert("stroke-dasharray",ui->lineEditLSdasharray->text());
        if (ui->comboBoxLSlinejoin->currentIndex()==1)
            rule.lineSymbolizer1.insert("stroke-linejoin","round");
        if (ui->comboBoxLSlinejoin->currentIndex()==2)
            rule.lineSymbolizer1.insert("stroke-linejoin","bevel");
        if (ui->comboBoxLSlinecap->currentIndex()==1)
            rule.lineSymbolizer1.insert("stroke-linecap","round");
        if (ui->comboBoxLSlinecap->currentIndex()==2)
            rule.lineSymbolizer1.insert("stroke-linecap","square");


        if (!ui->lineEditLineSymbolizer2->text().isEmpty())
        {
            rule.lineSymbolizer2.insert("stroke",ui->lineEditLineSymbolizer2->text());
            if (ui->doubleSpinBoxLSwidth2->value()!=1.0)
                rule.lineSymbolizer2.insert("stroke-width",QString::number(ui->doubleSpinBoxLSwidth2->value()));
            if (ui->doubleSpinBoxLSopacity2->value()!=1.0)
                rule.lineSymbolizer2.insert("stroke-opacity",QString::number(ui->doubleSpinBoxLSopacity2->value()));
            if (!ui->lineEditLSdasharray2->text().isEmpty())
                rule.lineSymbolizer2.insert("stroke-dasharray",ui->lineEditLSdasharray2->text());
            if (ui->comboBoxLSlinejoin2->currentIndex()==1)
                rule.lineSymbolizer2.insert("stroke-linejoin","round");
            if (ui->comboBoxLSlinejoin2->currentIndex()==2)
                rule.lineSymbolizer2.insert("stroke-linejoin","bevel");
            if (ui->comboBoxLSlinecap2->currentIndex()==1)
                rule.lineSymbolizer2.insert("stroke-linecap","round");
            if (ui->comboBoxLSlinecap2->currentIndex()==2)
                rule.lineSymbolizer2.insert("stroke-linecap","square");
        }

        if (!ui->lineEditLineSymbolizer3->text().isEmpty())
        {
            rule.lineSymbolizer3.insert("stroke",ui->lineEditLineSymbolizer3->text());
            if (ui->doubleSpinBoxLSwidth3->value()!=1.0)
                rule.lineSymbolizer3.insert("stroke-width",QString::number(ui->doubleSpinBoxLSwidth3->value()));
            if (ui->doubleSpinBoxLSopacity3->value()!=1.0)
                rule.lineSymbolizer3.insert("stroke-opacity",QString::number(ui->doubleSpinBoxLSopacity3->value()));
            if (!ui->lineEditLSdasharray3->text().isEmpty())
                rule.lineSymbolizer3.insert("stroke-dasharray",ui->lineEditLSdasharray3->text());
            if (ui->comboBoxLSlinejoin3->currentIndex()==1)
                rule.lineSymbolizer3.insert("stroke-linejoin","round");
            if (ui->comboBoxLSlinejoin3->currentIndex()==2)
                rule.lineSymbolizer3.insert("stroke-linejoin","bevel");
            if (ui->comboBoxLSlinecap3->currentIndex()==1)
                rule.lineSymbolizer3.insert("stroke-linecap","round");
            if (ui->comboBoxLSlinecap3->currentIndex()==2)
                rule.lineSymbolizer3.insert("stroke-linecap","square");
        }

    }

    if (ui->groupBoxPolygonSymbolizer->isChecked())
    {
        if (!ui->lineEditPolygonSymbolizer->text().isEmpty())
            rule.polygonSymbolizer.insert("fill",ui->lineEditPolygonSymbolizer->text());
        if (ui->doubleSpinBoxPSopacity->value()!=1.0)
            rule.polygonSymbolizer.insert("fill-opacity",QString::number(ui->doubleSpinBoxPSopacity->value()));
        if (ui->doubleSpinBoxPSgamma->value()!=1.0)
            rule.polygonSymbolizer.insert("gamma",QString::number(ui->doubleSpinBoxPSgamma->value()));
    }

    if (ui->groupBoxBuildingSymbolizer->isChecked())
    {
        if (!ui->lineEditBuildingSymbolizer->text().isEmpty())
            rule.buildingSymbolizer.insert("fill",ui->lineEditBuildingSymbolizer->text());
        if (ui->doubleSpinBoxBSopacity->value()!=1.0)
            rule.buildingSymbolizer.insert("fill-opacity",QString::number(ui->doubleSpinBoxBSopacity->value()));
        if (ui->doubleSpinBoxBSheight->value()!=1.0)
            rule.buildingSymbolizer.insert("height",QString::number(ui->doubleSpinBoxBSheight->value()));
    }

    if (ui->groupBoxMarkersSymbolizer->isChecked())
    {
        rule.markersSymbolizer=markersHash;
    }

    if (ui->groupBoxDebugSymbolizer->isChecked())
    {
        rule.debugSymbolizer=true;
    }

    if (currentRule < 0)
        element.rules.replace(0,rule);
    else
        element.rules.replace(currentRule,rule);

    updateDialog();
}

/**
 * @brief EditStyleDialog::currentSelectionChanged handles rule selection change
 */
void EditStyleDialog::currentSelectionChanged()
{
    if (ui->listWidget->selectedItems().count() !=1)
        return;
    if (moving)
        return;

    saveRule();
    currentRule=ui->listWidget->currentRow();
    updateDialog();
}


/**
 * @brief EditStyleDialog::moveUp moves rule up
 */
void EditStyleDialog::moveUp()
{
    if(ui->listWidget->count() == 0)
        return;
    if (ui->listWidget->selectedItems().count() !=1)
        return;
    int row=ui->listWidget->currentRow();
    if (row == 0)
        return;

    moving=true;
    QListWidgetItem *item=ui->listWidget->takeItem(row);
    ui->listWidget->insertItem(row-1,item);

    element.rules.move(row,row-1);

    ui->listWidget->setCurrentRow(row-1);
    moving=false;

    currentRule=ui->listWidget->currentRow();
}

/**
 * @brief EditStyleDialog::moveDown moves rule down
 */
void EditStyleDialog::moveDown()
{
    if(ui->listWidget->count() == 0)
        return;
    if (ui->listWidget->selectedItems().count() !=1)
        return;
    int row=ui->listWidget->currentRow();
    if (row == ui->listWidget->count()-1)
        return;

    moving=true;
    QListWidgetItem *item=ui->listWidget->takeItem(row);
    ui->listWidget->insertItem(row+1,item);

    element.rules.move(row,row+1);

    ui->listWidget->setCurrentRow(row+1);
    moving=false;

    currentRule=ui->listWidget->currentRow();
}

/**
 * @brief EditStyleDialog::editTextSymbolizer edits TextSymbolizer
 */
void EditStyleDialog::editTextSymbolizer()
{
    if(ui->listWidget->count() == 0)
        return;

    int row = ui->listWidget->currentRow();
    EditStyleTextDialog *editStyleTextDialog = new EditStyleTextDialog(element.rules.at(row).textSymbolizer,this);

    if (editStyleTextDialog->exec() == 1)
    {
        textHash=editStyleTextDialog->attribs;
    }

    saveRule();
    updateDialog();
    delete editStyleTextDialog;
}

/**
 * @brief EditStyleDialog::editShieldSymbolizer edits ShieldSymbolizer
 */
void EditStyleDialog::editShieldSymbolizer()
{
    if(ui->listWidget->count() == 0)
        return;

    int row = ui->listWidget->currentRow();
    EditStyleShieldDialog *editStyleShieldDialog = new EditStyleShieldDialog(element.rules.at(row).shieldSymbolizer,this);

    if (editStyleShieldDialog->exec() == 1)
    {
        shieldHash=editStyleShieldDialog->attribs;
    }

    saveRule();
    updateDialog();
    delete editStyleShieldDialog;
}

/**
 * @brief EditStyleDialog::editMarkersSymbolizer edits MarkersSymbolizer
 */
void EditStyleDialog::editMarkersSymbolizer()
{
    if(ui->listWidget->count() == 0)
        return;

    int row = ui->listWidget->currentRow();
    EditStyleMarkersDialog *editStyleMarkersDialog = new EditStyleMarkersDialog(element.rules.at(row).markersSymbolizer,this);

    if (editStyleMarkersDialog->exec() == 1)
    {
        markersHash=editStyleMarkersDialog->attribs;
    }

    saveRule();
    updateDialog();
    delete editStyleMarkersDialog;
}

/**
 * @brief EditStyleDialog::colorLineSymbolizer handles color button click event
 */
void EditStyleDialog::colorLineSymbolizer()
{
    QColor initColor;
    if (ui->lineEditLineSymbolizer->text() == "black")
        initColor=Qt::red;
    else
    {
        QString s=ui->lineEditLineSymbolizer->text();
        s.remove(0,4);
        s.remove(s.length()-1,1);
        if (s.split(",").count()!=3)
            initColor=Qt::yellow;
        else
        {
            initColor.setRed(s.split(",").at(0).toInt());
            initColor.setGreen(s.split(",").at(1).toInt());
            initColor.setBlue(s.split(",").at(2).toInt());
        }
    }
    QColor color = QColorDialog::getColor(initColor, this);
    if (color.isValid())
    {
        ui->lineEditLineSymbolizer->setText("rgb("+QString::number(color.red())+","+QString::number(color.green())+
                                            ","+QString::number(color.blue())+")");
    }
}

/**
 * @brief EditStyleDialog::colorLineSymbolizer2 handles color button click event
 */
void EditStyleDialog::colorLineSymbolizer2()
{
    QColor initColor;
    if (ui->lineEditLineSymbolizer2->text() == "black")
        initColor=Qt::red;
    else
    {
        QString s=ui->lineEditLineSymbolizer2->text();
        s.remove(0,4);
        s.remove(s.length()-1,1);
        if (s.split(",").count()!=3)
            initColor=Qt::yellow;
        else
        {
            initColor.setRed(s.split(",").at(0).toInt());
            initColor.setGreen(s.split(",").at(1).toInt());
            initColor.setBlue(s.split(",").at(2).toInt());
        }
    }
    QColor color = QColorDialog::getColor(initColor, this);
    if (color.isValid())
    {
        ui->lineEditLineSymbolizer2->setText("rgb("+QString::number(color.red())+","+QString::number(color.green())+
                                             ","+QString::number(color.blue())+")");
    }
}


/**
 * @brief EditStyleDialog::colorLineSymbolizer3 handles color button click event
 */
void EditStyleDialog::colorLineSymbolizer3()
{
    QColor initColor;
    if (ui->lineEditLineSymbolizer3->text() == "black")
        initColor=Qt::red;
    else
    {
        QString s=ui->lineEditLineSymbolizer3->text();
        s.remove(0,4);
        s.remove(s.length()-1,1);
        if (s.split(",").count()!=3)
            initColor=Qt::yellow;
        else
        {
            initColor.setRed(s.split(",").at(0).toInt());
            initColor.setGreen(s.split(",").at(1).toInt());
            initColor.setBlue(s.split(",").at(2).toInt());
        }
    }
    QColor color = QColorDialog::getColor(initColor, this);
    if (color.isValid())
    {
        ui->lineEditLineSymbolizer3->setText("rgb("+QString::number(color.red())+","+QString::number(color.green())+
                                             ","+QString::number(color.blue())+")");
    }
}


/**
 * @brief EditStyleDialog::colorPolygonSymbolizer handles color button click event
 */
void EditStyleDialog::colorPolygonSymbolizer()
{
    QColor initColor;
    if (ui->lineEditPolygonSymbolizer->text() == "gray")
        initColor=Qt::red;
    else
    {
        QString s=ui->lineEditPolygonSymbolizer->text();
        s.remove(0,4);
        s.remove(s.length()-1,1);
        if (s.split(",").count()!=3)
            initColor=Qt::yellow;
        else
        {
            initColor.setRed(s.split(",").at(0).toInt());
            initColor.setGreen(s.split(",").at(1).toInt());
            initColor.setBlue(s.split(",").at(2).toInt());
        }
    }
    QColor color = QColorDialog::getColor(initColor, this);
    if (color.isValid())
    {
        ui->lineEditPolygonSymbolizer->setText("rgb("+QString::number(color.red())+","+QString::number(color.green())+
                                               ","+QString::number(color.blue())+")");
    }
}

/**
 * @brief EditStyleDialog::colorBuildingSymbolizer handles color button click event
 */
void EditStyleDialog::colorBuildingSymbolizer()
{
    QColor initColor;
    if (ui->lineEditBuildingSymbolizer->text() == "gray")
        initColor=Qt::red;
    else
    {
        QString s=ui->lineEditBuildingSymbolizer->text();
        s.remove(0,4);
        s.remove(s.length()-1,1);
        if (s.split(",").count()!=3)
            initColor=Qt::yellow;
        else
        {
            initColor.setRed(s.split(",").at(0).toInt());
            initColor.setGreen(s.split(",").at(1).toInt());
            initColor.setBlue(s.split(",").at(2).toInt());
        }
    }
    QColor color = QColorDialog::getColor(initColor, this);
    if (color.isValid())
    {
        ui->lineEditBuildingSymbolizer->setText("rgb("+QString::number(color.red())+","+QString::number(color.green())+
                                                ","+QString::number(color.blue())+")");
    }
}

/**
 * @brief EditStyleDialog::browsePointSymbolizer handles browse file button click event
 */
void EditStyleDialog::browsePointSymbolizer()
{
    QString name = QFileDialog::getOpenFileName(this, tr("Choose file"), QDir::homePath()+"/",
                                                tr("Pictures (*.jpg *.jpeg *.gif *.png);;All files (*.*)"));
    if (!name.isEmpty())
    {
        ui->lineEditPointSymbolizer->setText(name);
    }
}

/**
 * @brief EditStyleDialog::browseLinePatternSymbolizer handles browse file button click event
 */
void EditStyleDialog::browseLinePatternSymbolizer()
{
    QString name = QFileDialog::getOpenFileName(this, tr("Choose file"), QDir::homePath()+"/",
                                                tr("Pictures (*.jpg *.jpeg *.gif *.png);;All files (*.*)"));
    if (!name.isEmpty())
    {
        ui->lineEditLinePatternSymbolizer->setText(name);
    }
}

/**
 * @brief EditStyleDialog::browsePolygonPatternSymbolizer handles browse file button click event
 */
void EditStyleDialog::browsePolygonPatternSymbolizer()
{
    QString name = QFileDialog::getOpenFileName(this, tr("Choose file"), QDir::homePath()+"/",
                                                tr("Pictures (*.jpg *.jpeg *.gif *.png);;All files (*.*)"));
    if (!name.isEmpty())
    {
        ui->lineEditPolygonPatternSymbolizer->setText(name);
    }
}

