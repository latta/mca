/**
  * @file mainwindow.cpp
  * @author Martin Latta
  * Map Creation Application from OpenStreetMap Project Data
  * Faculty of Information Technology
  * Brno University of Technology
  * 2013
  * @brief class representing application main window
  */

#include "mainwindow.h"
#include "ui_mainwindow.h"


#include <mapnik/cairo_renderer.hpp>
#include <mapnik/cairo_context.hpp>

#include <iostream>

/**
 * @brief MainWindow::MainWindow constructor method
 * @param parent
 */
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    setWindowTitle(tr("Map Creation Application"));

    readSettings();

    path=QDir::homePath();
    path = QApplication::applicationDirPath();


    splitter = new QSplitter(this);     // mainwindow splitter
    widgetSidebar = new QWidget();
    splitter->addWidget(widgetSidebar); // insert sidebar widget into splitter

    vbox= new QVBoxLayout();
    widgetSidebar->setLayout(vbox);     // set widget layout

    stylesheetLabel = new QLabel();
    styleListWidget = new QListWidget();
    styleListWidget->setSelectionMode(QAbstractItemView::SingleSelection);
    layerListWidget = new QListWidget();
    layerListWidget->setSelectionMode(QAbstractItemView::SingleSelection);
    hboxStyle = new QHBoxLayout();
    hboxStylesheet = new QHBoxLayout();
    hboxLayer = new QHBoxLayout();

    editStylesheetButton=new  QPushButton(tr("Edit stylesheet"));
    newStylesheetButton=new  QPushButton(tr("New stylesheet"));
    connect(editStylesheetButton, SIGNAL(clicked()), this, SLOT(editStylesheetSlot()));
    connect(newStylesheetButton, SIGNAL(clicked()), this, SLOT(newStylesheetSlot()));

    editStyleButton=new QPushButton(tr("Edit style"));
    addStyleButton=new QToolButton();
    addStyleButton->setIcon(QIcon(":/icons/add.png"));
    addStyleButton->setToolTip(tr("Add style"));
    removeStyleButton=new QToolButton();
    removeStyleButton->setIcon(QIcon(":/icons/remove.png"));
    removeStyleButton->setToolTip(tr("Remove"));
    moveUpStyleButton=new QToolButton();
    moveUpStyleButton->setIcon(QIcon(":/icons/moveup.png"));
    moveUpStyleButton->setToolTip(tr("Move up"));
    moveDownStyleButton=new QToolButton();
    moveDownStyleButton->setIcon(QIcon(":/icons/movedown.png"));
    moveDownStyleButton->setToolTip(tr("Move down"));
    selectAllStyleButton=new QToolButton();
    selectAllStyleButton->setIcon(QIcon(":/icons/selectall.png"));
    selectAllStyleButton->setToolTip(tr("Select all"));

    connect(editStyleButton, SIGNAL(clicked()), this, SLOT(editStyleSlot()));
    connect(addStyleButton, SIGNAL(clicked()), this, SLOT(addStyleSlot()));
    connect(removeStyleButton, SIGNAL(clicked()), this, SLOT(removeStyleSlot()));
    connect(moveUpStyleButton, SIGNAL(clicked()), this, SLOT(moveUpStyleSlot()));
    connect(moveDownStyleButton, SIGNAL(clicked()), this, SLOT(moveDownStyleSlot()));
    connect(selectAllStyleButton, SIGNAL(clicked()), this, SLOT(selectAllStyleSlot()));

    editLayerButton=new QPushButton(tr("Edit layer"));
    addLayerButton=new QToolButton();
    addLayerButton->setIcon(QIcon(":/icons/add.png"));
    addLayerButton->setToolTip(tr("Add layer"));
    removeLayerButton=new QToolButton();
    removeLayerButton->setIcon(QIcon(":/icons/remove.png"));
    removeLayerButton->setToolTip(tr("Remove"));
    moveUpLayerButton=new QToolButton();
    moveUpLayerButton->setIcon(QIcon(":/icons/moveup.png"));
    moveUpLayerButton->setToolTip(tr("Move up"));
    moveDownLayerButton=new QToolButton();
    moveDownLayerButton->setIcon(QIcon(":/icons/movedown.png"));
    moveDownLayerButton->setToolTip(tr("Move down"));
    selectAllLayerButton=new QToolButton();
    selectAllLayerButton->setIcon(QIcon(":/icons/selectall.png"));
    selectAllLayerButton->setToolTip(tr("Select all"));

    connect(editLayerButton, SIGNAL(clicked()), this, SLOT(editLayerSlot()));
    connect(addLayerButton, SIGNAL(clicked()), this, SLOT(addLayerSlot()));
    connect(removeLayerButton, SIGNAL(clicked()), this, SLOT(removeLayerSlot()));
    connect(moveUpLayerButton, SIGNAL(clicked()), this, SLOT(moveUpLayerSlot()));
    connect(moveDownLayerButton, SIGNAL(clicked()), this, SLOT(moveDownLayerSlot()));
    connect(selectAllLayerButton, SIGNAL(clicked()), this, SLOT(selectAllLayerSlot()));

    vbox->addLayout(hboxStylesheet);
    vbox->addWidget(stylesheetLabel);
    vbox->addLayout(hboxStyle);
    vbox->addWidget(styleListWidget);
    vbox->addLayout(hboxLayer);
    vbox->addWidget(layerListWidget);

    hboxStylesheet->addWidget(newStylesheetButton);
    hboxStylesheet->addWidget(editStylesheetButton);

    hboxStyle->addWidget(editStyleButton);
    hboxStyle->addWidget(addStyleButton);
    hboxStyle->addWidget(removeStyleButton);
    hboxStyle->addWidget(moveUpStyleButton);
    hboxStyle->addWidget(moveDownStyleButton);
    hboxStyle->addWidget(selectAllStyleButton);

    hboxLayer->addWidget(editLayerButton);
    hboxLayer->addWidget(addLayerButton);
    hboxLayer->addWidget(removeLayerButton);
    hboxLayer->addWidget(moveUpLayerButton);
    hboxLayer->addWidget(moveDownLayerButton);
    hboxLayer->addWidget(selectAllLayerButton);

    mapImageLabel = new QLabel();
    scrollArea = new QScrollArea();

    scrollArea->setBackgroundRole(QPalette::Dark);
    scrollArea->setWidget(mapImageLabel);
    scrollArea->setAlignment(Qt::AlignCenter);

    splitter->addWidget(scrollArea);

    QList<int> list;
    list.push_back(200);
    list.push_back(1000);
    splitter->setSizes(list);

    this->setCentralWidget(splitter);

    minlon=-180.0;
    minlat=-90.0;
    maxlon=180.0;
    maxlat=90.0;

    createActions();
    createToolBar();
    createMenus();
    createStatusBar();

    styleSheet=new StyleSheet();
    xmlparser=new StyleXmlParser(styleListWidget, layerListWidget, styleSheet, defStyleFile);

    connect(styleListWidget, SIGNAL(itemDoubleClicked(QListWidgetItem*)), this, SLOT(styleDoubleClicked(QListWidgetItem*)));
    connect(layerListWidget, SIGNAL(itemDoubleClicked(QListWidgetItem*)), this, SLOT(layerDoubleClicked(QListWidgetItem*)));

    // HTTP section
    http = new QHttp(this);
    connect(http, SIGNAL(requestFinished(int,bool)), this, SLOT(downloadFinished(int,bool)));
    connect(http, SIGNAL(stateChanged(int)), this, SLOT(downloadStateChanged(int)));

    // Render section
    mutex=new QMutex();
    ren = new Render(mutex);
    qRegisterMetaType<QImage>("QImage");
    connect(ren, SIGNAL(renFinished(QImage,QString,double,double)), this, SLOT(renderFinished(QImage, QString,double,double)));
    connect(ren, SIGNAL(renFailed(QString)), this, SLOT(renderFailed(QString)));
    connect(ren, SIGNAL(renStatus(QString)), this, SLOT(renderStatus(QString)));

    stylesheetLabel->setText(QString(tr("Stylesheet: ")));

    mapWidth=1000;
    mapHeight=800;
    aspect=0;
    resolution=MAPNIK_PPI;

}

/**
 * @brief MainWindow::~MainWindow destructor
 */
MainWindow::~MainWindow()
{
    if (ren->isRunning())
        ren->terminate();

    delete mutex;
    delete ren;

    delete http;

    delete xmlparser;
    delete styleSheet;

    delete statusBarLabel;
    delete statusBar;

    delete fileMenu;
    delete styleMenu;
    delete mapMenu;
    delete helpMenu;

    delete labelBBox;
    delete minlatLineEdit;
    delete minlonLineEdit;
    delete maxlatLineEdit;
    delete maxlonLineEdit;

    delete reloadButton;
    delete downloadButton;

    delete actionOpenMap;
    delete actionDownloadMap;
    delete actionExportMap;

    delete actionNewStylesheet;
    delete actionLoadStylesheet;
    delete actionSaveStylesheet;
    delete actionEditStylesheet;
    delete actionEditStyle;
    delete actionEditLayer;

    delete actionOptions;

    delete actionQuit;
    delete actionReload;

    delete actionSetSize;
    delete actionEditGrid;

    delete mapImageLabel;
    delete scrollArea;

    delete editStylesheetButton;
    delete newStylesheetButton;

    delete editStyleButton;
    delete addStyleButton;
    delete removeStyleButton;
    delete moveUpStyleButton;
    delete moveDownStyleButton;
    delete selectAllStyleButton;

    delete editLayerButton;
    delete addLayerButton;
    delete removeLayerButton;
    delete moveUpLayerButton;
    delete moveDownLayerButton;
    delete selectAllLayerButton;

    delete hboxStylesheet;
    delete hboxStyle;
    delete hboxLayer;

    delete styleListWidget;
    delete layerListWidget;

    delete stylesheetLabel;
    delete vbox;

    delete widgetSidebar;
    delete splitter;

    delete mainToolBar;

    delete ui;
}

/**
 * @brief MainWindow::closeEvent handle window close event
 * @param event
 */
void MainWindow::closeEvent(QCloseEvent* event)
{
    writeSettings();
    event->accept();
}

/**
 * @brief MainWindow::openMapSlot handles open map request
 */
void MainWindow::openMapSlot()
{
    try{
        mapFilename = QFileDialog::getOpenFileName(this, tr("Open file"), path+"/", tr("OSM XML Files (*.osm);;All files (*)"));

        if (mapFilename.isEmpty())
            return;

        QFile tmpfile(defOsmFile);

        if (tmpfile.exists())
            tmpfile.remove();

        QFile::copy(mapFilename, defOsmFile);
        QFile tempfile(defOsmFile);

        if (!tempfile.open(QIODevice::ReadOnly))
        {
            QMessageBox::warning(this,"Open file","Can't load the file "+ mapFilename);
            return;
        }

        QDomDocument doc("mydocument");

        if (!doc.setContent(&tempfile)) {
            tempfile.close();
            return;
        }
        tempfile.close();

        QDomElement elem = doc.documentElement();
        QDomNamedNodeMap nodemap= elem.firstChild().attributes();

        minlat=nodemap.namedItem("minlat").nodeValue().toDouble();
        minlon=nodemap.namedItem("minlon").nodeValue().toDouble();
        maxlat=nodemap.namedItem("maxlat").nodeValue().toDouble();
        maxlon=nodemap.namedItem("maxlon").nodeValue().toDouble();

        minlonLineEdit->setValue(minlon);
        minlatLineEdit->setValue(minlat);
        maxlonLineEdit->setValue(maxlon);
        maxlatLineEdit->setValue(maxlat);

        getInfoFromMap();
    }
    catch (...){
        std::cerr<< "Error while loading map file";
    }
}

/**
 * @brief MainWindow::downloadMapSlot handles download map request
 */
void MainWindow::downloadMapSlot()
{
    downloadButton->setDisabled(true);

    QString copyofURL=defaultURL;
    copyofURL.replace("minlon",QString::number(minlon));
    copyofURL.replace("minlat",QString::number(minlat));
    copyofURL.replace("maxlon",QString::number(maxlon));
    copyofURL.replace("maxlat",QString::number(maxlat));

    QUrl url(copyofURL);

    // http://www.openstreetmap.org/api/0.6/map?bbox=minlon,minlat,maxlon,maxlat

    http->setHost(url.host());
    http->get(url.toEncoded());
}

/**
 * @brief MainWindow::downloadFinished  handles download finished event
 * @param id request ID
 * @param error
 */
void MainWindow::downloadFinished(int id, bool error)
{
    Q_UNUSED(id)
    downloadButton->setDisabled(false);

    if(error)
    {
        statusBarLabel->setText(tr("Download error"));
    }
    else
    {
        statusBarLabel->setText(tr("Download finished!"));
        QByteArray data= http->readAll();

        if (http->lastResponse().statusCode()== 400)
        {
            QMessageBox::information(this,tr("Download error"),tr("Bounding box is too big! Select a smaller area."));
            return;
        }
        QFile file(defOsmFile);

        file.open(QIODevice::WriteOnly);
        file.write(data);
        file.close();

        mapFilename=defOsmFile;

        getInfoFromMap();
    }
}

/**
 * @brief MainWindow::downloadStateChanged handles download state change event
 * @param state new state
 */
void MainWindow::downloadStateChanged(int state)
{
    switch(state) {
    case 0:
        break;
    case 1:
    case 2:
        statusBarLabel->setText(tr("Connecting to OpenStreetMap.org ..."));
        break;
    case 3:
        statusBarLabel->setText(tr("Sending request ..."));
        break;
    case 4:
        statusBarLabel->setText(tr("Downloading data ..."));
        break;
    case 5:
        statusBarLabel->setText(tr("Connected to OpenStreetMap.org ..."));
        break;
    case 6:
        break;
    }
}

/**
 * @brief MainWindow::exportMapSlot handles export map request
 */
void MainWindow::exportMapSlot()
{
    if (mapImageLabel->pixmap() == 0)
    {
        QMessageBox::information(this,tr("Export"),tr("You must render map first"));
        return;
    }

    ExportMapDialog *exportMapDialog = new ExportMapDialog(*mapImageLabel->pixmap(),scale, resolution,
                                                           exportRatio, infoMapOther, infoMapWiki, this);

    if (exportMapDialog->exec() == 1)
    {
        //qDebug()<<"export";
    }

    delete exportMapDialog;
}

/**
 * @brief MainWindow::reloadSlot handles reload image request
 */
void MainWindow::reloadSlot()
{
    if (stylesheetFilename.isEmpty())
    {
        QMessageBox::information(this,tr("No stylesheet set"),tr("You must open or create stylesheet"));
        return;
    }

    xmlparser->writeFile(defStyleFile);

    statusBarLabel->setText(tr("Rendering started"));

    ren->render(defOsmFile, defStyleFile, mapWidth, mapHeight, aspect, minlon, minlat, maxlon, maxlat, gridParams);
    ren->start();
}

/**
 * @brief MainWindow::renderFinished handles render finished event
 * @param image new rendered image
 * @param s error string
 * @param d map scale
 */
void MainWindow::renderFinished(const QImage &image, QString str,double res, double d)
{
    scale=d;
    resolution=res;
    QPixmap pixmap(QPixmap::fromImage(image));

    mapImageLabel->clear();
    mapImageLabel->setPixmap(pixmap);

    QSize originalSize=pixmap.size();

    mapImageLabel->resize(originalSize);

    ren->quit();
    statusBarLabel->setText(str + QString("   scale 1:") + QString::number(scale) + ("   size ")
                            + QString::number(pixmap.width())+"x"+ QString::number(pixmap.height()));
}

/**
 * @brief MainWindow::renderStatus sets status while rendering
 * @param s message string
 */
void MainWindow::renderStatus(QString s)
{
    statusBarLabel->setText(s);
}

/**
 * @brief MainWindow::renderFailed handles render failed event
 * @param s error string
 */
void MainWindow::renderFailed(QString s)
{
    QMessageBox::warning(this,tr("Render failed"),s);
    ren->quit();
    statusBarLabel->setText(tr("Render failed!"));
}

/**
 * @brief MainWindow::minlonChange handles minlon value change
 * @param d new value
 */
void MainWindow::minlonChange(double d)
{
    minlon=d;
}

/**
 * @brief MainWindow::minlatChange handles minlat value change
 * @param d new value
 */
void MainWindow::minlatChange(double d)
{
    minlat=d;
}

/**
 * @brief MainWindow::maxlonChange handles maxlon value change
 * @param d new value
 */
void MainWindow::maxlonChange(double d)
{
    maxlon=d;
}

/**
 * @brief MainWindow::maxlatChange handles maxlat value change
 * @param d new value
 */
void MainWindow::maxlatChange(double d)
{
    maxlat=d;
}

/**
 * @brief MainWindow::openStylesheetSlot handles open stylesheet request
 */
void MainWindow::openStylesheetSlot()
{
    try{
        QString s= QFileDialog::getOpenFileName(this, tr("Open file"), path+"/", tr("XML Files (*.xml)"));

        if (s.isEmpty())
            return;
        else
            stylesheetFilename=s;

        QFileInfo info(stylesheetFilename);
        QString stylename= info.fileName();

        QFile tmpfile(defStyleFile);
        if (tmpfile.exists())
        {
            tmpfile.remove();
        }

        QFile::copy(stylesheetFilename, defStyleFile);
        QFile tempfile(defStyleFile);

        if (!tempfile.open(QIODevice::ReadOnly))
        {
            QMessageBox::warning(this,"Open file","Can't load the file "+ stylesheetFilename);
            return;
        }
        tempfile.close();

        styleSheet->clearAll();
        styleListWidget->clear();
        layerListWidget->clear();

        if (!xmlparser->readFile())
            QMessageBox::warning(this,"Load file","Error while reading file "+ stylesheetFilename);

        stylesheetLabel->setText(QString(tr("Stylesheet: ")) + stylename);
    }
    catch (...)
    {
        std::cerr<< "Error while loading stylesheet";
    }
}


/**
 * @brief MainWindow::newStylesheetSlot handles new stylesheet request
 */
void MainWindow::newStylesheetSlot()
{
    bool ok;
    QString stylesheetname = QInputDialog::getText(this, tr("New stylesheet"),
                                                   tr("New stylesheet name:"), QLineEdit::Normal,
                                                   QString(), &ok);
    if (ok && !stylesheetname.isEmpty())
    {
        QFile tmpfile(defStyleFile);
        if (tmpfile.exists())
            tmpfile.remove();

        xmlparser->createFile();

        styleSheet->clearAll();
        layerListWidget->clear();
        styleListWidget->clear();

        xmlparser->readFile();
        stylesheetLabel->setText(QString(tr("Stylesheet: ")) + stylesheetname);
        stylesheetFilename=stylesheetname;
    }
}


/**
 * @brief MainWindow::saveStylesheetSlot handles save stylesheet request
 */
void MainWindow::saveStylesheetSlot()
{
    if (stylesheetFilename.isEmpty())
    {
        QMessageBox::information(this,tr("Map Creation Application"),tr("No stylesheet to save!"));
        return;
    }

    QString fname = QFileDialog::getSaveFileName(this, tr("Save stylesheet as .."),path,tr("XML files (*.xml)"));

    QDomDocument document = xmlparser->updateStyleSheet(); //
    QFile tempfile(fname);

    if (!tempfile.open(QIODevice::WriteOnly))
        return;

    QTextStream stream(&tempfile);
    document.save(stream,4);
    tempfile.close();

    QFileInfo info(fname);
    fname= info.fileName();

    stylesheetLabel->setText(QString(tr("Stylesheet: ")) + fname);
    stylesheetFilename=fname;
}


/**
 * @brief MainWindow::editStylesheetSlot handles edit stylesheet request
 */
void MainWindow::editStylesheetSlot()
{
    if (stylesheetFilename.isEmpty())
    {
        QMessageBox::information(this,tr("Map Creation Application"),tr("No stylesheet to edit!"));
        return;
    }

    EditStylesheetDialog *editStylesheetDialog = new EditStylesheetDialog(stylesheetFilename, styleSheet->attribs,this);

    if (editStylesheetDialog->exec() == 1)
    {

        styleSheet->attribs=editStylesheetDialog->attribs;

    }
    delete editStylesheetDialog;
}


/**
 * @brief MainWindow::addStyleSlot handles add style request
 */
void MainWindow::addStyleSlot()
{
    if (stylesheetFilename.isEmpty())
        return;

    bool ok;
    QString text = QInputDialog::getText(this, tr("New style"),
                                         tr("New style name:"), QLineEdit::Normal,
                                         QString(), &ok);
    if (ok && !text.isEmpty())
    {
        if (styleSheet->containsStyle(text))
        {
            QMessageBox::information(this,tr("Map Creation Application"),tr("Style ") + text + tr(" already exists"));
            return;
        }

        styleSheet->addStyle(text);

        QListWidgetItem *item = new QListWidgetItem(text,styleListWidget);
        item->setFlags(item->flags()|Qt::ItemIsUserCheckable);
        item->setCheckState(Qt::Checked);

        styleListWidget->clearSelection();
        item->setSelected(true);
    }
}


/**
 * @brief MainWindow::editStyleSlot handles edit style request
 */
void MainWindow::editStyleSlot()
{
    if (styleListWidget->selectedItems().isEmpty())
        return;

    QListWidgetItem *item=styleListWidget->selectedItems().first();
    QString selectedItem=item->text();

    TStyleElement styleElem= styleSheet->getStyleElem(selectedItem);

    EditStyleDialog *editStyleDialog = new EditStyleDialog(styleElem, this);

    if (editStyleDialog->exec() == 1)
    {
        styleSheet->modifyStyle(editStyleDialog->element.name, editStyleDialog->element);
        item->setText(editStyleDialog->element.name); // update tree widget
    }
    delete editStyleDialog;
}


/**
 * @brief MainWindow::removeStyleSlot handles remove style request
 */
void MainWindow::removeStyleSlot()
{
    if (styleListWidget->selectedItems().isEmpty())
        return;

    QListWidgetItem *item= styleListWidget->selectedItems().first();

    //qDebug()<<"remove style "<< item->text();

    styleSheet->removeStyle(item->text());
    delete item;
}

/**
 * @brief MainWindow::moveUpStyleSlot handles moveup style request
 */
void MainWindow::moveUpStyleSlot()
{
    if (styleListWidget->selectedItems().isEmpty())
        return;

    int row=styleListWidget->currentRow();
    if (row == 0)
        return;

    QListWidgetItem *item=styleListWidget->takeItem(row);
    styleListWidget->insertItem(row-1,item);

    styleSheet->moveStyleUp(row);
    styleListWidget->setCurrentRow(row-1);
}

/**
 * @brief MainWindow::moveDownStyleSlot handles movedown style request
 */
void MainWindow::moveDownStyleSlot()
{
    if (styleListWidget->selectedItems().isEmpty())
        return;

    int row=styleListWidget->currentRow();
    if (row == styleListWidget->count()-1)
    {
        return;
    }

    QListWidgetItem *item=styleListWidget->takeItem(row);
    styleListWidget->insertItem(row+1,item);

    styleSheet->moveStyleDown(row);
    styleListWidget->setCurrentRow(row+1);
}

/**
 * @brief MainWindow::selectAllStyleSlot handles select all styles request
 */
void MainWindow::selectAllStyleSlot()
{
    bool ok=false;
    for (int i=0; i < styleListWidget->count(); i++)
    {
        if (styleListWidget->item(i)->checkState() == Qt::Unchecked)
        {
            ok = true;
            break;
        }
    }

    if (ok)
        for (int i=0; i < styleListWidget->count(); i++)
        {
            styleListWidget->item(i)->setCheckState(Qt::Checked);
        }
    else
        for (int i=0; i < styleListWidget->count(); i++)
        {
            styleListWidget->item(i)->setCheckState(Qt::Unchecked);
        }
}


/**
 * @brief MainWindow::addLayerSlot handles add layer request
 */
void MainWindow::addLayerSlot()
{
    if (stylesheetFilename.isEmpty())
        return;

    bool ok;
    QString text = QInputDialog::getText(this, tr("New layer"),
                                         tr("New layer name:"), QLineEdit::Normal,
                                         QString(), &ok);
    if (ok && !text.isEmpty())
    {
        if (styleSheet->containsLayer(text))
        {
            QMessageBox::information(this,tr("Map Creation Application"),tr("Layer ") + text + tr(" already exists"));
            return;
        }

        styleSheet->addLayer(text);

        QListWidgetItem *item = new QListWidgetItem(text,layerListWidget);
        item->setFlags(item->flags()|Qt::ItemIsUserCheckable);
        item->setCheckState(Qt::Checked);

        layerListWidget->clearSelection();
        item->setSelected(true);
    }
}


/**
 * @brief MainWindow::editLayerSlot handles edit layer request
 */
void MainWindow::editLayerSlot()
{
    if (layerListWidget->selectedItems().isEmpty())
        return;

    QListWidgetItem *item=layerListWidget->selectedItems().first();

    QString selectedItem=item->text();

    TLayerElement layerElem= styleSheet->getLayerElem(selectedItem);

    QStringList slist;
    for (int i=0; i<styleListWidget->count(); i++)
    {
        slist << styleListWidget->item(i)->text();
    }

    EditLayerDialog *editLayerDialog = new EditLayerDialog(layerElem, slist, this);

    if (editLayerDialog->exec() == 1)
    {
        styleSheet->modifyLayer(editLayerDialog->element.name, editLayerDialog->element);
        item->setText(editLayerDialog->element.name); // update tree widget
    }

    delete editLayerDialog;
}


/**
 * @brief MainWindow::removeLayerSlot handles remove layer request
 */
void MainWindow::removeLayerSlot()
{
    if (layerListWidget->selectedItems().isEmpty())
        return;

    QListWidgetItem *item= layerListWidget->selectedItems().first();

    styleSheet->removeLayer(item->text());
    delete item;
}

/**
 * @brief MainWindow::moveUpLayerSlot handles moveup layer request
 */
void MainWindow::moveUpLayerSlot()
{
    if (layerListWidget->selectedItems().isEmpty())
        return;

    int row=layerListWidget->currentRow();
    if (row == 0)
        return;

    QListWidgetItem *item=layerListWidget->takeItem(row);
    layerListWidget->insertItem(row-1,item);

    styleSheet->moveLayerUp(row);
    layerListWidget->setCurrentRow(row-1);
}

/**
 * @brief MainWindow::moveDownLayerSlot handles movedown layer request
 */
void MainWindow::moveDownLayerSlot()
{
    if (layerListWidget->selectedItems().isEmpty())
        return;

    int row=layerListWidget->currentRow();
    if (row == layerListWidget->count()-1)
    {
        return;
    }

    QListWidgetItem *item=layerListWidget->takeItem(row);
    layerListWidget->insertItem(row+1,item);

    styleSheet->moveLayerDown(row);
    layerListWidget->setCurrentRow(row+1);
}

/**
 * @brief MainWindow::selectAllLayerSlot handles select all layer request
 */
void MainWindow::selectAllLayerSlot()
{
    bool ok=false;
    for (int i=0; i < layerListWidget->count(); i++)
    {
        if (layerListWidget->item(i)->checkState() == Qt::Unchecked)
        {
            ok = true;
            break;
        }
    }

    if (ok)
        for (int i=0; i < layerListWidget->count(); i++)
        {
            layerListWidget->item(i)->setCheckState(Qt::Checked);
        }
    else
        for (int i=0; i < layerListWidget->count(); i++)
        {
            layerListWidget->item(i)->setCheckState(Qt::Unchecked);
        }
}

/**
 * @brief MainWindow::editMapSize handles edit map size request
 */
void MainWindow::editMapSize()
{
    EditMapSizeDialog *editMapSizeDialog = new EditMapSizeDialog(mapWidth,mapHeight,aspect,this);

    if (editMapSizeDialog->exec() == 1)
    {
        mapWidth=editMapSizeDialog->w;
        mapHeight=editMapSizeDialog->h;
        aspect=editMapSizeDialog->a;
    }
    delete editMapSizeDialog;
}

/**
 * @brief MainWindow::editGrid handles edit grid request
 */
void MainWindow::editGrid()
{
    EditGridDialog *editGridDialog = new EditGridDialog(gridParams);

    if (editGridDialog->exec() == 1)
    {
        gridParams= editGridDialog->gridParams;
    }

    delete editGridDialog;
}

/**
 * @brief MainWindow::optionsSlot handles edit options request
 */
void MainWindow::optionsSlot()
{
    OptionsDialog *optionsDialog = new OptionsDialog(defOsmFile, defStyleFile, defaultURL, exportRatio);

    if (optionsDialog->exec() == 1)
    {
        defOsmFile= optionsDialog->defaultOSMfilename;
        defStyleFile= optionsDialog->defaultStylesheetName;
        defaultURL= optionsDialog->defaultURL;
        exportRatio= optionsDialog->exportRatio;
    }
    delete optionsDialog;
}


/**
 * @brief MainWindow::styleDoubleClicked handle style edit request
 * @param item clicked item
 */
void MainWindow::styleDoubleClicked(QListWidgetItem* item)
{
    if (item != 0)
        editStyleSlot();
}

/**
 * @brief MainWindow::layerDoubleClicked handle layer edit request
 * @param item clicked item
 */
void MainWindow::layerDoubleClicked(QListWidgetItem *item)
{
    if (item != 0)
        editLayerSlot();
}


/**
 * @brief MainWindow::createActions
 */
void MainWindow::createActions()
{
    actionOpenMap = new QAction(QIcon(":/icons/open.png"),tr("&Open OSM file"),this);
    actionOpenMap->setShortcut(tr("Ctrl+O"));
    connect(actionOpenMap, SIGNAL(triggered()), this, SLOT(openMapSlot()));

    actionDownloadMap = new QAction(QIcon(":/icons/download.png"),tr("&Download OSM file"),this);
    actionDownloadMap->setShortcut(tr("Ctrl+D"));
    connect(actionDownloadMap, SIGNAL(triggered()), this, SLOT(downloadMapSlot()));

    actionExportMap = new QAction(QIcon(":/icons/export.png"),tr("&Export Map"),this);
    actionExportMap->setShortcut(tr("Ctrl+E"));
    connect(actionExportMap, SIGNAL(triggered()), this, SLOT(exportMapSlot()));


    actionNewStylesheet = new QAction(QIcon(":/icons/new.png"),tr("New Stylesheet"),this);
    connect(actionNewStylesheet, SIGNAL(triggered()), this, SLOT(newStylesheetSlot()));

    actionLoadStylesheet = new QAction(tr("&Load Stylesheet"),this);
    actionLoadStylesheet->setShortcut(tr("Ctrl+L"));
    connect(actionLoadStylesheet, SIGNAL(triggered()), this, SLOT(openStylesheetSlot()));

    actionSaveStylesheet = new QAction(tr("Save Stylesheet"),this);
    connect(actionSaveStylesheet, SIGNAL(triggered()), this, SLOT(saveStylesheetSlot()));

    actionEditStylesheet = new QAction(tr("Edit Stylesheet"),this);
    connect(actionEditStylesheet, SIGNAL(triggered()), this, SLOT(editStylesheetSlot()));

    actionEditStyle = new QAction(tr("Edit Style"),this);
    connect(actionEditStyle, SIGNAL(triggered()), this, SLOT(editStyleSlot()));

    actionEditLayer = new QAction(tr("Edit Layer"),this);
    connect(actionEditLayer, SIGNAL(triggered()), this, SLOT(editLayerSlot()));

    actionOptions = new QAction(tr("Options"),this);
    connect(actionOptions, SIGNAL(triggered()), this, SLOT(optionsSlot()));

    actionReload = new QAction(QIcon(":/icons/reload.png"),tr("&Reload"),this);
    actionReload->setShortcut(tr("Ctrl+R"));
    connect(actionReload, SIGNAL(triggered()), this, SLOT(reloadSlot()));

    actionSetSize= new QAction(tr("Edit Map Size"),this);
    connect(actionSetSize, SIGNAL(triggered()), this, SLOT(editMapSize()));

    actionEditGrid= new QAction(tr("Edit Grid"),this);
    actionEditGrid->setShortcut(tr("Ctrl+G"));
    connect(actionEditGrid, SIGNAL(triggered()), this, SLOT(editGrid()));

    actionQuit = new QAction(tr("E&xit"), this);
    actionQuit->setShortcut(tr("Ctrl+Q"));
    connect(actionQuit, SIGNAL(triggered()), this, SLOT(close()));

    actionAbout = new QAction(tr("About"), this);
    connect(actionAbout, SIGNAL(triggered()), this, SLOT(helpSlot()));

}

/**
 * @brief MainWindow::createToolBar
 */
void MainWindow::createToolBar()
{
    mainToolBar = addToolBar(tr("Main toolbar"));


    openButton = new QToolButton(this);
    openButton->setIcon(QIcon(":/icons/open.png"));
    openButton->setText(tr("Open"));
    openButton->setToolButtonStyle(Qt::ToolButtonTextBesideIcon);

    exportButton = new QToolButton(this);
    exportButton->setIcon(QIcon(":/icons/export.png"));
    exportButton->setText(tr("Export"));
    exportButton->setToolButtonStyle(Qt::ToolButtonTextBesideIcon);

    reloadButton = new QToolButton(this);
    reloadButton->setIcon(QIcon(":/icons/reload.png"));
    reloadButton->setText(tr("Reload"));
    reloadButton->setToolButtonStyle(Qt::ToolButtonTextBesideIcon);

    downloadButton = new QToolButton(this);
    downloadButton->setText(tr("Download"));
    downloadButton->setIcon(QIcon(":/icons/download.png"));
    downloadButton->setToolButtonStyle(Qt::ToolButtonTextBesideIcon);


    mainToolBar->addWidget(openButton);
    mainToolBar->addWidget(exportButton);
    mainToolBar->addSeparator();
    mainToolBar->addWidget(reloadButton);

    minlonLineEdit = new QDoubleSpinBox(this);
    minlonLineEdit->setMaximum(180.0);
    minlonLineEdit->setMinimum(-180.0);
    minlonLineEdit->setDecimals(7);
    minlonLineEdit->setSingleStep(0.0000001);
    minlonLineEdit->setValue(minlon);
    minlonLineEdit->setToolTip(tr("Minimum longitude"));

    minlatLineEdit = new QDoubleSpinBox(this);
    minlatLineEdit->setMaximum(90.0);
    minlatLineEdit->setMinimum(-90.0);
    minlatLineEdit->setDecimals(7);
    minlatLineEdit->setSingleStep(0.0000001);
    minlatLineEdit->setValue(minlat);
    minlatLineEdit->setToolTip(tr("Minimum latitude"));

    maxlonLineEdit = new QDoubleSpinBox(this);
    maxlonLineEdit->setMaximum(180.0);
    maxlonLineEdit->setMinimum(-180.0);
    maxlonLineEdit->setDecimals(7);
    maxlonLineEdit->setSingleStep(0.0000001);
    maxlonLineEdit->setValue(maxlon);
    maxlonLineEdit->setToolTip(tr("Maximum longitude"));

    maxlatLineEdit = new QDoubleSpinBox(this);
    maxlatLineEdit->setMaximum(90.0);
    maxlatLineEdit->setMinimum(-90.0);
    maxlatLineEdit->setDecimals(7);
    maxlatLineEdit->setSingleStep(0.0000001);
    maxlatLineEdit->setValue(maxlat);
    maxlatLineEdit->setToolTip(tr("Maximum latitude"));

    mainToolBar->addSeparator();

    labelBBox= new QLabel(tr("Bounding box:"),this);

    mainToolBar->addWidget(labelBBox);
    mainToolBar->addWidget(minlonLineEdit);
    mainToolBar->addWidget(minlatLineEdit);
    mainToolBar->addWidget(maxlonLineEdit);
    mainToolBar->addWidget(maxlatLineEdit);

    mainToolBar->addWidget(downloadButton);

    connect(minlonLineEdit, SIGNAL(valueChanged(double)), this, SLOT(minlonChange(double)));
    connect(minlatLineEdit, SIGNAL(valueChanged(double)), this, SLOT(minlatChange(double)));
    connect(maxlonLineEdit, SIGNAL(valueChanged(double)), this, SLOT(maxlonChange(double)));
    connect(maxlatLineEdit, SIGNAL(valueChanged(double)), this, SLOT(maxlatChange(double)));

    connect(reloadButton,SIGNAL(clicked()),this, SLOT(reloadSlot()));
    connect(downloadButton,SIGNAL(clicked()),this, SLOT(downloadMapSlot()));
    connect(openButton,SIGNAL(clicked()),this, SLOT(openMapSlot()));
    connect(exportButton,SIGNAL(clicked()),this, SLOT(exportMapSlot()));

}


/**
 * @brief MainWindow::createMenus
 */
void MainWindow::createMenus()
{
    fileMenu = new QMenu(tr("&File"),this);
    styleMenu = new QMenu(tr("&Stylesheet"),this);
    mapMenu = new QMenu(tr("&Map"),this);
    helpMenu = new QMenu(tr("&Help"), this);

    fileMenu->addAction(actionOpenMap);
    fileMenu->addAction(actionDownloadMap);
    fileMenu->addAction(actionExportMap);
    fileMenu->addSeparator();
    fileMenu->addAction(actionOptions);
    fileMenu->addSeparator();
    fileMenu->addAction(actionQuit);

    mapMenu->addAction(actionReload);
    mapMenu->addAction(actionSetSize);
    mapMenu->addAction(actionEditGrid);

    styleMenu->addAction(actionNewStylesheet);
    styleMenu->addAction(actionLoadStylesheet);
    styleMenu->addAction(actionSaveStylesheet);
    styleMenu->addAction(actionEditStylesheet);
    styleMenu->addSeparator();
    styleMenu->addAction(actionEditStyle);
    styleMenu->addAction(actionEditLayer);

    helpMenu->addAction(actionAbout);

    menuBar()->addMenu(fileMenu);
    menuBar()->addMenu(styleMenu);
    menuBar()->addMenu(mapMenu);
    menuBar()->addMenu(helpMenu);
}

/**
 * @brief MainWindow::createStatusBar
 */
void MainWindow::createStatusBar()
{
    statusBar=new QStatusBar(this);

    statusBarLabel = new QLabel(tr("Ready"));

    setStatusBar(statusBar);
    statusBar->addWidget(statusBarLabel);
}

/**
 * @brief MainWindow::getInfoFromMap gets local names from map data
 */
void MainWindow::getInfoFromMap()
{
    QFile tempfile(defOsmFile);

    if (!tempfile.open(QIODevice::ReadOnly))
        return;

    infoMapOther.clear();
    infoMapWiki.clear();

    QTextStream in(&tempfile);
    QString line = in.readLine();

    QRegExp regWiki("<tag k=.wikipedia:?([^\"']*). v=.([^\"']*).*>");
    QRegExp regHttp("^https?:");
    QRegExp regPre("^([^:]+):");

    QRegExp regOther("<tag k=.name. v=.([^\"']*).*>");
    int pos;
    while (!line.isNull())
    {
        pos = regOther.indexIn(line);
        if (pos > -1)
        {
            infoMapOther<<regOther.cap(1).replace(" ","_");
        }
        pos = regWiki.indexIn(line);
        if (pos > -1)
        {
            QString text=regWiki.cap(2);

            if (regHttp.indexIn(text) > -1)
                infoMapWiki << text.replace("https:","http:");

            else if (regWiki.cap(1).isEmpty())
            {
                if (regPre.indexIn(text) > -1)
                {
                    QString s=regPre.cap(1);
                    text.replace(s+":","http://"+ s +".wikipedia.org/wiki/");
                    infoMapWiki << text.replace(" ","_");
                }
            }
            else
            {
                text = "http://"+ regWiki.cap(1).replace(":","") +".wikipedia.org/wiki/" + text;
                infoMapWiki << text.replace(" ","_");
            }
        }

        line = in.readLine();
    }

    tempfile.close();
}

/**
 * @brief MainWindow::writeSettings write application setting (window position and default variables)
 */
void MainWindow::writeSettings()
 {
     QSettings settings("IBP", "MCA");

     settings.beginGroup("GUI");
     settings.setValue("size", size());
     settings.setValue("pos", pos());
     settings.setValue("defOSMfile", defOsmFile);
     settings.setValue("defStylesheet", defStyleFile);
     settings.setValue("defURL", defaultURL);
     settings.setValue("expRatio", exportRatio);
     settings.endGroup();
 }

/**
  * @brief MainWindow::readSettings read application settings (window position and default variables)
  */
 void MainWindow::readSettings()
 {
     QSettings settings("IBP", "MCA");

     settings.beginGroup("GUI");
     resize(settings.value("size", QSize(800, 600)).toSize());
     move(settings.value("pos", QPoint(150, 150)).toPoint());
     exportRatio=settings.value("expRatio",1.414213).toDouble();
     defOsmFile=settings.value("defOSMfile",QString("tmp_map.osm")).toString();
     defStyleFile=settings.value("defStylesheet",QString("tmp_osm.xml")).toString();
//     defaultURL=settings.value("defURL",QString("http://www.openstreetmap.org/api/0.6/map?bbox=minlon,minlat,maxlon,maxlat")).toString();
     defaultURL=settings.value("defURL",QString("http://open.mapquestapi.com/xapi/api/0.6/*[bbox=minlon,minlat,maxlon,maxlat]")).toString();
     settings.endGroup();
 }

 /**
  * @brief MainWindow::helpSlot
  */
 void MainWindow::helpSlot()
 {
    QTextEdit* help=new QTextEdit(this);
    help->setWindowFlags(Qt::Dialog);
    help->setReadOnly(true);
    help->append("<h1>Map Creation Application</h1>\n\nAuthor: Martin Latta\n");
    help->append("E-mail: xlatta00@stud.fit.vutbr.cz\nDate:12.5.2013\nFor more info see Readme file");
    help->show();
 }
