/**
  * @file mainwindow.h
  * @author Martin Latta
  * Map Creation Application from OpenStreetMap Project Data
  * Faculty of Information Technology
  * Brno University of Technology
  * 2013
  * @brief class representing application main window
  */

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtCore>
#include <QtGui>
#include <QtNetwork/QHttp>

#include "stylexmlparser.h"
#include "editstyledialog.h"
#include "editstylesheetdialog.h"
#include "editlayerdialog.h"
#include "stylesheet.h"
#include "render.h"
#include "editgriddialog.h"
#include "exportmapdialog.h"
#include "editmapsizedialog.h"
#include "optionsdialog.h"

#include <iostream>
class QImage;
namespace Ui {
class MainWindow;
}

/**
 * @brief The MainWindow class representing application main window
 */
class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

protected:
    void closeEvent(QCloseEvent* event);

private slots:
    void openMapSlot();
    void downloadMapSlot();
    void downloadFinished(int id, bool error);
    void downloadStateChanged(int state);
    void exportMapSlot();
    void reloadSlot();
    void renderFinished(const QImage &image, QString str, double res, double d);
    void renderStatus(QString s);
    void renderFailed(QString s);

    void minlonChange(double);
    void minlatChange(double);
    void maxlonChange(double);
    void maxlatChange(double);

    void openStylesheetSlot();
    void newStylesheetSlot();
    void saveStylesheetSlot();
    void editStylesheetSlot();

    void addStyleSlot();
    void editStyleSlot();
    void removeStyleSlot();
    void moveUpStyleSlot();
    void moveDownStyleSlot();
    void selectAllStyleSlot();

    void addLayerSlot();
    void editLayerSlot();
    void removeLayerSlot();
    void moveUpLayerSlot();
    void moveDownLayerSlot();
    void selectAllLayerSlot();

    void editMapSize();
    void editGrid();
    void optionsSlot();
    void helpSlot();

    void styleDoubleClicked(QListWidgetItem *item);
    void layerDoubleClicked(QListWidgetItem *item);


private:
    void createActions();
    void createMenus();
    void createToolBar();
    void createStatusBar();

    void editStyle(QListWidgetItem *item);
    void editLayer(QListWidgetItem *item);

    void getInfoFromMap();
    void writeSettings();
    void readSettings();

    Ui::MainWindow *ui;

    QToolBar *mainToolBar; /**< main toolbar widget */

    // menus
    QMenu *fileMenu;
    QMenu *styleMenu;
    QMenu *mapMenu;
    QMenu *helpMenu;

    QStatusBar *statusBar; /**< statusbar widget */
    QLabel *statusBarLabel; /**< statusbar label */

    // actions
    QAction *actionOpenMap;
    QAction *actionDownloadMap;
    QAction *actionExportMap;
    QAction *actionNewStylesheet;
    QAction *actionLoadStylesheet;
    QAction *actionSaveStylesheet;
    QAction *actionEditStylesheet;
    QAction *actionEditStyle;
    QAction *actionEditLayer;
    QAction *actionOptions;
    QAction *actionQuit;
    QAction *actionReload;
    QAction *actionSetSize;
    QAction *actionEditGrid;
    QAction *actionAbout;

    QSplitter *splitter; /**< mainwindow splitter */

    // sidebar elements
    QWidget *widgetSidebar; /**< sidebar widget */
    QVBoxLayout *vbox;
    QLabel *stylesheetLabel;
    QHBoxLayout *hboxStylesheet;
    QHBoxLayout *hboxStyle;
    QHBoxLayout *hboxLayer;
    QListWidget *styleListWidget; /**< list of styles */
    QListWidget *layerListWidget; /**< list of layers */
    QPushButton *editStylesheetButton;
    QPushButton *newStylesheetButton;

    // sidebar style buttons
    QPushButton *editStyleButton;
    QToolButton *addStyleButton;
    QToolButton *removeStyleButton;
    QToolButton *moveUpStyleButton;
    QToolButton *moveDownStyleButton;
    QToolButton *selectAllStyleButton;

    // sidebar layer buttons
    QPushButton *editLayerButton;
    QToolButton *addLayerButton;
    QToolButton *removeLayerButton;
    QToolButton *moveUpLayerButton;
    QToolButton *moveDownLayerButton;
    QToolButton *selectAllLayerButton;

    // toolbar elements
    QLabel *labelBBox;
    QDoubleSpinBox *minlatLineEdit;
    QDoubleSpinBox *minlonLineEdit;
    QDoubleSpinBox *maxlatLineEdit;
    QDoubleSpinBox *maxlonLineEdit;
    QToolButton *reloadButton;
    QToolButton *downloadButton;
    QToolButton *openButton;
    QToolButton *exportButton;

    // image area widgets
    QScrollArea *scrollArea;
    QLabel * mapImageLabel;


    StyleXmlParser *xmlparser; /**< StyleXmlParser class object */
    StyleSheet *styleSheet; /**< StyleSheet class object */

    // render
    Render *ren;
    QMutex *mutex;

    QHttp *http; /**< http */

    double scale, resolution, exportRatio; /**< map parameters */
    double minlon, minlat, maxlon, maxlat; /**< bounding box */
    int mapWidth, mapHeight, aspect; /**< map parameters */

    QHash<QString, QString> gridParams; /**< grid parameters */
    QString mapFilename; /**< current map filename */
    QString stylesheetFilename; /**< current stylesheet filename */

    QString defOsmFile; /**< default OSM filename */
    QString defStyleFile; /**< default stylesheet filename */
    QString defaultURL; /**< default URL */
    QString path; /** current path */

    QStringList infoMapOther; /**< list with map names */
    QStringList infoMapWiki; /**< list with URLs */

};

#endif // MAINWINDOW_H
