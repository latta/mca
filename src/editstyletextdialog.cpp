/**
  * @file editstyletextdialog.cpp
  * @author Martin Latta
  * Map Creation Application from OpenStreetMap Project Data
  * Faculty of Information Technology
  * Brno University of Technology
  * 2013
  * @brief dialog to edit TextSymbolizer
  */

#include "editstyletextdialog.h"
#include "ui_editstyletextdialog.h"

/**
 * @brief EditStyleTextDialog::EditStyleTextDialog constructor method
 * @param a default parameters
 * @param parent
 */
EditStyleTextDialog::EditStyleTextDialog(QHash<QString, QString> a,QWidget *parent) :
    QDialog(parent),
    ui(new Ui::EditStyleTextDialog)
{
    ui->setupUi(this);

    this->setWindowTitle("Edit TextSymbolizer");
    ui->lineEdit1->setText(a.value("name",QString()));
    ui->lineEdit2->setText(a.value("face-name",QString()));
    ui->lineEdit3->setText(a.value("fontset-name",QString()));
    ui->lineEdit4->setText(a.value("size",QString()));
    ui->lineEdit5->setText(a.value("text-ratio",QString()));
    ui->lineEdit6->setText(a.value("wrap-character",QString()));
    ui->lineEdit7->setText(a.value("wrap-before",QString()));
    ui->lineEdit8->setText(a.value("fill",QString()));
    ui->lineEdit9->setText(a.value("halo-fill",QString()));

    ui->lineEdit10->setText(a.value("placement",QString()));
    ui->spinBox1->setValue(a.value("wrap-width","0").toInt());
    ui->spinBox2->setValue(a.value("line-spacing","0").toInt());
    ui->spinBox3->setValue(a.value("character-spacing","0").toInt());
    ui->spinBox4->setValue(a.value("spacing","0").toInt());
    ui->spinBox5->setValue(a.value("halo-radius","0").toInt());
    ui->spinBox6->setValue(a.value("orientation","0").toInt());

    ui->doubleSpinBox1->setValue(a.value("dx","0.0").toDouble());
    ui->doubleSpinBox2->setValue(a.value("dy","0.0").toDouble());
    ui->doubleSpinBox3->setValue(a.value("minimum-distance","0.0").toDouble());
    ui->doubleSpinBox4->setValue(a.value("opacity","1.0").toDouble());
    ui->doubleSpinBox5->setValue(a.value("minimum-padding","0.0").toDouble());
    ui->doubleSpinBox6->setValue(a.value("minimum-path-length","0.0").toDouble());

    if (a.value("avoid-edges") == "true")
        ui->checkBox1->setChecked(true);
    else
        ui->checkBox1->setChecked(false);

    if (a.value("allow-overlap") == "true")
        ui->checkBox2->setChecked(true);
    else
        ui->checkBox2->setChecked(false);

    if (a.value("clip") == "false")
        ui->checkBox3->setChecked(false);
    else
        ui->checkBox3->setChecked(true);

    QString s;
    s=a.value("text-transform");
    if (s == "none")
        ui->comboBox1->setCurrentIndex(0);
    else if (s == "uppercase")
        ui->comboBox1->setCurrentIndex(1);
    else if (s == "lowercase")
        ui->comboBox1->setCurrentIndex(2);
    else if (s == "capitalize")
        ui->comboBox1->setCurrentIndex(3);

    s=a.value("vertical-alignment","auto");
    if (s == "auto")
        ui->comboBox2->setCurrentIndex(0);
    else if (s == "middle")
        ui->comboBox2->setCurrentIndex(1);
    else if (s == "bottom")
        ui->comboBox2->setCurrentIndex(2);
    else if (s == "top")
        ui->comboBox2->setCurrentIndex(3);

    s=a.value("horizontal-alignment","auto");
    if (s == "auto")
        ui->comboBox3->setCurrentIndex(0);
    else if (s == "middle")
        ui->comboBox3->setCurrentIndex(1);
    else if (s == "left")
        ui->comboBox3->setCurrentIndex(2);
    else if (s == "right")
        ui->comboBox3->setCurrentIndex(3);

    s=a.value("justify-alignment","auto");
    if (s == "auto")
        ui->comboBox4->setCurrentIndex(0);
    else if (s == "middle")
        ui->comboBox4->setCurrentIndex(1);
    else if (s == "left")
        ui->comboBox4->setCurrentIndex(2);
    else if (s == "right")
        ui->comboBox4->setCurrentIndex(3);

    connect(ui->pushButtonFill, SIGNAL(clicked()),this, SLOT(colorFill()));
    connect(ui->pushButtonHalo, SIGNAL(clicked()),this, SLOT(colorHalo()));
    connect(this, SIGNAL(accepted()),this, SLOT(save()));
}

/**
 * @brief EditStyleTextDialog::~EditStyleTextDialog destructor
 */
EditStyleTextDialog::~EditStyleTextDialog()
{
    delete ui;
}

/**
 * @brief EditStyleTextDialog::save saves dialog content
 */
void EditStyleTextDialog::save()
{

    if (!ui->lineEdit1->text().isEmpty())
        attribs.insert("name",ui->lineEdit1->text());
    if (!ui->lineEdit2->text().isEmpty())
        attribs.insert("face-name",ui->lineEdit2->text());
    if (!ui->lineEdit3->text().isEmpty())
        attribs.insert("fontset-name",ui->lineEdit3->text());
    if (!ui->lineEdit4->text().isEmpty())
        attribs.insert("size",ui->lineEdit4->text());
    if (!ui->lineEdit5->text().isEmpty())
        attribs.insert("text-ratio",ui->lineEdit5->text());
    if (!ui->lineEdit6->text().isEmpty())
        attribs.insert("wrap-character",ui->lineEdit6->text());
    if (!ui->lineEdit7->text().isEmpty())
        attribs.insert("wrap-before",ui->lineEdit7->text());
    if (!ui->lineEdit8->text().isEmpty())
        attribs.insert("fill",ui->lineEdit8->text());
    if (!ui->lineEdit9->text().isEmpty())
        attribs.insert("halo-fill",ui->lineEdit9->text());
    if (!ui->lineEdit10->text().isEmpty())
        attribs.insert("placement",ui->lineEdit10->text());


    if (ui->spinBox1->value()!=0)
        attribs.insert("wrap-width",QString::number(ui->spinBox1->value()));
    if (ui->spinBox2->value()!=0)
        attribs.insert("line-spacing",QString::number(ui->spinBox2->value()));
    if (ui->spinBox3->value()!=0)
        attribs.insert("character-spacing",QString::number(ui->spinBox3->value()));
    if (ui->spinBox4->value()!=0)
        attribs.insert("spacing",QString::number(ui->spinBox4->value()));
    if (ui->spinBox5->value()!=0)
        attribs.insert("halo-radius",QString::number(ui->spinBox5->value()));
    if (ui->spinBox6->value()!=0)
        attribs.insert("orientation",QString::number(ui->spinBox6->value()));


    if (ui->doubleSpinBox1->value()!=0.0)
        attribs.insert("dx",QString::number(ui->doubleSpinBox1->value()));
    if (ui->doubleSpinBox2->value()!=0.0)
        attribs.insert("dy",QString::number(ui->doubleSpinBox2->value()));
    if (ui->doubleSpinBox3->value()!=0.0)
        attribs.insert("minimum-distance",QString::number(ui->doubleSpinBox3->value()));
    if (ui->doubleSpinBox4->value()!=1.0)
        attribs.insert("opacity",QString::number(ui->doubleSpinBox4->value()));
    if (ui->doubleSpinBox5->value()!=0.0)
        attribs.insert("minimum-padding",QString::number(ui->doubleSpinBox5->value()));
    if (ui->doubleSpinBox6->value()!=0.0)
        attribs.insert("minimum-path-length",QString::number(ui->doubleSpinBox6->value()));


    if (ui->checkBox1->isChecked())
        attribs.insert("avoid-edges","true");
    if (ui->checkBox2->isChecked())
        attribs.insert("allow-overlap","true");
    if (!ui->checkBox3->isChecked())
        attribs.insert("clip","false");


    switch (ui->comboBox1->currentIndex())
    {
    case 1: attribs.insert("text-transform","uppercase");
        break;
    case 2: attribs.insert("text-transform","lowercase");
        break;
    case 3: attribs.insert("text-transform","capitalize");
        break;
    }

    switch (ui->comboBox2->currentIndex())
    {
    case 1: attribs.insert("vertical-alignment","middle");
        break;
    case 2: attribs.insert("vertical-alignment","bottom");
        break;
    case 3: attribs.insert("vertical-alignment","top");
        break;
    }

    switch (ui->comboBox3->currentIndex())
    {
    case 1: attribs.insert("horizontal-alignment","middle");
        break;
    case 2: attribs.insert("horizontal-alignment","left");
        break;
    case 3: attribs.insert("horizontal-alignment","right");
        break;
    }

    switch (ui->comboBox3->currentIndex())
    {
    case 1: attribs.insert("justify-alignment","middle");
        break;
    case 2: attribs.insert("justify-alignment","left");
        break;
    case 3: attribs.insert("justify-alignment","right");
        break;
    }

}

/**
 * @brief EditStyleTextDialog::colorFill handles color button click event
 */
void EditStyleTextDialog::colorFill()
{
    QColor initColor;
    if (ui->lineEdit8->text() == "black")
        initColor=Qt::red;
    else
    {
        QString s=ui->lineEdit8->text();
        s.remove(0,4);
        s.remove(s.length()-1,1);

        if (s.split(",").count()!=3)
            initColor=Qt::yellow;
        else
        {
            initColor.setRed(s.split(",").at(0).toInt());
            initColor.setGreen(s.split(",").at(1).toInt());
            initColor.setBlue(s.split(",").at(2).toInt());
        }
    }
    QColor color = QColorDialog::getColor(initColor, this);
    if (color.isValid())
    {
        ui->lineEdit8->setText("rgb("+QString::number(color.red())+","+QString::number(color.green())+
                               ","+QString::number(color.blue())+")");
    }
}

/**
 * @brief EditStyleTextDialog::colorHalo handles color button click event
 */
void EditStyleTextDialog::colorHalo()
{
    QColor initColor;
    if (ui->lineEdit9->text() == "black")
        initColor=Qt::red;
    else
    {
        QString s=ui->lineEdit9->text();
        s.remove(0,4);
        s.remove(s.length()-1,1);
        if (s.split(",").count()!=3)
            initColor=Qt::yellow;
        else
        {
            initColor.setRed(s.split(",").at(0).toInt());
            initColor.setGreen(s.split(",").at(1).toInt());
            initColor.setBlue(s.split(",").at(2).toInt());
        }
    }
    QColor color = QColorDialog::getColor(initColor, this);
    if (color.isValid())
    {
        ui->lineEdit9->setText("rgb("+QString::number(color.red())+","+QString::number(color.green())+
                               ","+QString::number(color.blue())+")");
    }
}
