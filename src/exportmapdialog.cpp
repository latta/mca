/**
  * @file exportmapdialog.cpp
  * @author Martin Latta
  * Map Creation Application from OpenStreetMap Project Data
  * Faculty of Information Technology
  * Brno University of Technology
  * 2013
  * @brief dialog to export map image
  */

#include "exportmapdialog.h"
#include "ui_exportmapdialog.h"

/**
 * @brief ExportMapDialog::ExportMapDialog constructor method
 * @param pix pixmap with map
 * @param sc map scale
 * @param listOther list with text names
 * @param listWiki list with URLs
 * @param parent
 */
ExportMapDialog::ExportMapDialog(QPixmap pix, double sc, double res, double rat, QStringList listOther, QStringList listWiki, QWidget *parent):
    QDialog(parent),
    ui(new Ui::ExportMapDialog)
{
    ui->setupUi(this);

    infoMapOther = listOther;
    infoMapWiki = listWiki;
    ratio = rat;
    pixmapMap=pix;
    resolution=res;

    this->setWindowTitle("Export map");

    marginSize=ui->spinBoxMargin->value();
    titleTextSize=ui->spinBoxTitle->value();
    infoTextSize=ui->spinBoxInfoTextSize->value();
    infoSize=ui->spinBoxInfo->value();
    wikiOnly = ui->checkBoxWikiURL->isChecked();
    deleteNewlines = ui->checkBoxDeleteNewlines->isChecked();

    scale=sc;

    w=pixmapMap.width()+2*marginSize;
    h=ratio*w;

    http = new QHttp(this);
    reqId=0;
    listCounter=0;
    access=false;

    fileformat="png";
    fontTitle.setFamily("DejaVu Sans");
    fontTitle.setPointSize(titleTextSize);
    fontInfo.setFamily("DejaVu Sans");
    fontInfo.setPointSize(infoTextSize);

    sizeOfView=QSize(300,300);

    filename=QString();

    scene= new QGraphicsScene();
    ui->graphicsView->setScene(scene);

    rectitem = new QGraphicsRectItem();
    rectitem->setPen(QPen(QColor(Qt::black)));
    rectitem->setBrush(QBrush(QColor(Qt::transparent)));

    pixmapitem = new QGraphicsPixmapItem();


    QRegExp rx("\\d{1,4}");
    QRegExpValidator *validator = new QRegExpValidator(rx);
    ui->lineEditH->setValidator(validator);
    ui->lineEditW->setValidator(validator);
    ui->lineEditH->setText(QString::number(h));
    ui->lineEditW->setText(QString::number(w));

    scene->setSceneRect(0,0,300,300);
    scene->addItem(rectitem);
    scene->addItem(pixmapitem);

    connect(ui->listWidget, SIGNAL(itemSelectionChanged()), this, SLOT(listSelectionChanged()));
    connect(ui->textEdit, SIGNAL(textChanged()), this, SLOT(textEditChanged()));
    connect(ui->comboBoxFormat, SIGNAL(currentIndexChanged(QString)), this, SLOT(comboBoxFormat(QString)));
    connect(ui->lineEditH, SIGNAL(textEdited(QString)),this, SLOT(heightChange(QString)));
    connect(ui->lineEditW, SIGNAL(textEdited(QString)),this, SLOT(widthChange(QString)));
    connect(ui->buttonBox, SIGNAL(accepted()),this, SLOT(save()));

    connect(http, SIGNAL(requestFinished(int,bool)), this, SLOT(downloadFinished(int,bool)));

    ui->pushButtonAdd->setIcon(QIcon(":/icons/add.png"));
    ui->pushButtonAdd->setToolTip(tr("Add item"));
    ui->pushButtonRemove->setIcon(QIcon(":/icons/remove.png"));
    ui->pushButtonRemove->setToolTip(tr("Remove item"));
    ui->pushButtonMoveup->setIcon(QIcon(":/icons/moveup.png"));
    ui->pushButtonMoveup->setToolTip(tr("Move up"));
    ui->pushButtonMovedown->setIcon(QIcon(":/icons/movedown.png"));
    ui->pushButtonMovedown->setToolTip(tr("Move down"));

    ui->spinBoxScale->setValue((int)scale);

    adjustMiniature();
}

/**
 * @brief ExportMapDialog::~ExportMapDialog destructor
 */
ExportMapDialog::~ExportMapDialog()
{
    http->close();
    http->abort();
    http->clearPendingRequests();

    delete pixmapitem;
    delete rectitem;
    delete scene;
    delete http;

    delete ui;
}


/**
 * @brief ExportMapDialog::widthChange handles width change
 * @param s new value
 */
void ExportMapDialog::widthChange(QString s)
{
    if (s.isEmpty() || s.toInt() < 100)
        return;

    if (!ui->checkBoxRotate->isChecked())
    {
        w=s.toInt();
        h=ratio*w;
    }
    else
    {
        w=s.toInt();
        h=w/ratio;
    }

    ui->lineEditH->setText(QString::number(h));
    adjustMiniature();
}

/**
 * @brief ExportMapDialog::heightChange handles height change
 * @param s new value
 */
void ExportMapDialog::heightChange(QString s)
{
    if (s.isEmpty() || s.toInt() < 100)
        return;

    if (!ui->checkBoxRotate->isChecked())
    {
        h=s.toInt();
        w=h/ratio;
    }
    else
    {
        h=s.toInt();
        w=h*ratio;
    }
    ui->lineEditW->setText(QString::number(w));
    adjustMiniature();
}

/**
 * @brief ExportMapDialog::getFilename get output filename
 */
void ExportMapDialog::getFilename()
{
    filename = QFileDialog::getSaveFileName(this, tr("Save picture as .."),QDir::homePath(),
                                            tr(QString("Image Files (*."+ fileformat + ")").toAscii().data() ));

}


/**
 * @brief ExportMapDialog::comboBoxFormat change output image file format
 * @param s selected value
 */
void ExportMapDialog::comboBoxFormat(QString s)
{
    fileformat=s;
}

/**
 * @brief ExportMapDialog::save saves image into file
 */
void ExportMapDialog::save()
{
    if (w < 100 || h < 100)
    {
        QMessageBox::information(this,tr("Image is too small"),tr("Minimum size is 100 px"));
        return;
    }

    if (filename.isEmpty())
        getFilename();

    if (filename.isEmpty())
        return;

    updatePixmap();
    QImage image = pixmapOut.toImage();
    image.setDotsPerMeterX((int)(ui->doubleSpinBoxRes->value() / 0.0254));
    image.setDotsPerMeterY((int)(ui->doubleSpinBoxRes->value() / 0.0254));
    image.save(filename,fileformat.toAscii().data());

    filename.clear();
    //done(1);
}

/**
 * @brief ExportMapDialog::downloadFinished handles download finished event
 * @param id download request ID
 * @param error
 */
void ExportMapDialog::downloadFinished(int id, bool error)
{
    if(!error)
    {
        if (http->lastResponse().statusCode()== 200)
        {
            QString data=trUtf8(http->readAll().data());
            if (!data.isEmpty())
                arrayList << data;
        }
    }
    if (id == reqId)
    {
        allDownloadFinished();
    }

    ui->labelStatus->setText(QString("Downloading data (")+QString::number(id)+QString(" from ")+QString::number(reqId)+QString(")"));
}


/**
 * @brief ExportMapDialog::listSelectionChanged handles list selection change
 */
void ExportMapDialog::listSelectionChanged()
{
    if (ui->listWidget->count() == 0)
        return;
    if (ui->listWidget->selectedItems().isEmpty())
        return;

    QListWidgetItem *item= ui->listWidget->selectedItems().first();

    ui->textEdit->setText(itemDataList.value(item));
}

/**
 * @brief ExportMapDialog::textEditChanged handles text editation
 */
void ExportMapDialog::textEditChanged()
{
    if (!access)
    {
        QListWidgetItem *item= ui->listWidget->selectedItems().first();
        itemDataList.insert(item,ui->textEdit->toPlainText());
    }
}

/**
 * @brief ExportMapDialog::updatePixmap updates output pixmap
 */
void ExportMapDialog::updatePixmap()
{
    QPixmap p(w,h);
    p.fill(QColor(Qt::white));

    QPainter painter(&p);

    painter.setBackground(QBrush(QColor(Qt::white)));
    painter.setFont(fontTitle);
    painter.drawText(QPoint(marginSize,titleTextSize+marginSize),ui->lineEditTitle->text());

    QSize s(w-(marginSize*2),h-(4*marginSize+titleTextSize+infoSize));
    QPixmap scaledCopy(pixmapMap.scaled(s,Qt::KeepAspectRatio, Qt::SmoothTransformation));

    double new_res=((double)(ui->spinBoxScale->value()) / scale)*resolution;

    ui->doubleSpinBoxRes->setValue(new_res*(double)scaledCopy.width()/ (double)pixmapMap.width());

    QString str("1:"+QString::number(ui->spinBoxScale->value()));
    QFontMetrics fm(fontTitle);
    painter.drawText(QPoint(w - fm.width(str) - marginSize, titleTextSize+marginSize),str);

    if (!infoitem.isEmpty())
    {
        QTextOption opt;
        opt.setWrapMode(QTextOption::WrapAtWordBoundaryOrAnywhere);
        painter.setFont(fontInfo);
        painter.drawText(QRect(marginSize, titleTextSize+marginSize*3+scaledCopy.height(), w-(marginSize*2),
                               h-(marginSize*4+titleTextSize+scaledCopy.height())),infoitem,opt);
    }

    int offset=((w-2*marginSize)-scaledCopy.width())/2;
    QPoint target(marginSize + offset, marginSize*2+titleTextSize);

    painter.drawPixmap(target, scaledCopy);
    painter.end();
    pixmapOut=p;
}

/**
 * @brief ExportMapDialog::allDownloadFinished handles allDownloadFinished event
 */
void ExportMapDialog::allDownloadFinished()
{
    http->close();
    http->abort();
    http->clearPendingRequests();

    access=true;

    ui->textEdit->clear();

    for (int i=0; i<arrayList.count(); i++)
    {
        QString str=arrayList.at(i);
        QDomDocument doc,doc2;

        QDomElement e = doc2.createElement("div");
        doc2.appendChild(e);
        QString title;
        if (doc.setContent(str))
        {
            QDomElement elem=doc.documentElement();
            QDomNodeList elements= elem.elementsByTagName("div");
            for (int i=0; i<elements.count(); i++)
            {
                if (elements.at(i).toElement().attribute("id") == "mw-content-text")
                {
                    doc2.appendChild(elements.at(i));
                    break;
                }
            }
            elements= elem.elementsByTagName("h1");
            for (int i=0; i<elements.count(); i++)
            {
                if (elements.at(i).toElement().attribute("id") == "firstHeading")
                {
                    title = elements.at(i).toElement().text();
                    break;
                }
            }
        }

        if (doc2.isNull())
        {
            ui->textEdit->setHtml(arrayList.at(i));
        }
        else
        {
            ui->textEdit->setHtml(doc2.toString());
        }

        QString s=ui->textEdit->toPlainText();
        s.replace(QRegExp("^[^\\d\\w]*"),""); // erase whitespace from beginning
        ui->textEdit->setText(s);

        if (ui->listWidget->findItems(title,Qt::MatchExactly).isEmpty())
        {
            QListWidgetItem *item=new QListWidgetItem(title,ui->listWidget);
            item->setFlags(item->flags()|Qt::ItemIsUserCheckable|Qt::ItemIsEditable);
            item->setCheckState(Qt::Checked);

            itemDataList.insert(item,s);
        }
    }

    access=false;
    ui->labelStatus->setText("");
}


/**
 * @brief ExportMapDialog::adjustMiniature adjusts pixmap miniature
 */
void ExportMapDialog::adjustMiniature()
{
    QRectF r(0,0,0,0);
    double x=(double)w/(double)h;

    if (ui->checkBoxRotate->isChecked())
    {
        r.setWidth(w*sizeOfView.width()/w);
        r.setHeight(r.width()/x);
    }
    else
    {
        r.setHeight(h*sizeOfView.height()/h);
        r.setWidth(r.height()*x);
    }

    rectitem->setRect(r);

    updatePixmap();
    pixmapitem->setPixmap(pixmapOut.scaled(r.size().toSize(),Qt::KeepAspectRatio, Qt::SmoothTransformation));


    if (!ui->checkBoxRotate->isChecked())
    {
        rectitem->setPos((sizeOfView.width() - rectitem->rect().width())/2, 0);
        pixmapitem->setPos((sizeOfView.width() - rectitem->rect().width())/2, 0);
    }
    else
    {
        rectitem->setPos(0,(sizeOfView.height() - rectitem->rect().height())/2);
        pixmapitem->setPos(0,(sizeOfView.height() - rectitem->rect().height())/2);
    }
    rectitem->setZValue(1.0);
}

/**
 * @brief ExportMapDialog::on_spinBoxTitle_valueChanged handles title size change
 * @param i new value
 */
void ExportMapDialog::on_spinBoxTitle_valueChanged(int i)
{
    titleTextSize=i;
    fontTitle.setPointSize(i);
    adjustMiniature();
}

/**
 * @brief ExportMapDialog::on_spinBoxMargin_valueChanged handles margin size change
 * @param i new value
 */
void ExportMapDialog::on_spinBoxMargin_valueChanged(int i)
{
    marginSize=i;
    adjustMiniature();
}


/**
 * @brief ExportMapDialog::on_checkBoxRotate_stateChanged handles rotate checkbox change
 * @param i
 */
void ExportMapDialog::on_checkBoxRotate_stateChanged(int i)
{
    Q_UNUSED(i)

    int x=w;
    w=h;
    h=x;

    ui->lineEditW->setText(QString::number(w));
    ui->lineEditH->setText(QString::number(h));
    adjustMiniature();
}

/**
 * @brief ExportMapDialog::on_spinBoxInfo_valueChanged handles info size change
 * @param i new value
 */
void ExportMapDialog::on_spinBoxInfo_valueChanged(int i)
{
    if (i > h - ( 4* marginSize + titleTextSize  ))
        i = h - ( 4* marginSize + titleTextSize );

    infoSize=i;
    adjustMiniature();
}


/**
 * @brief ExportMapDialog::on_pushButtonImport_clicked handles import button click event
 */
void ExportMapDialog::on_pushButtonImport_clicked()
{
    QString s= ui->lineEditURL->text();
    QList<QUrl> listWiki;
    QList<QUrl> listOther;

    QRegExp reg("\\D?\\d*");
    foreach (QString text, infoMapOther)
    {
        if (!reg.exactMatch(text))
            listOther<<QUrl(s+text);
    }

    foreach (QString text, infoMapWiki)
    {
        listWiki<<QUrl(text);
    }

    listWiki= listWiki.toSet().toList();
    listOther= listOther.toSet().toList();

    arrayList.clear();

    foreach(QUrl url, listWiki)
    {
        http->setHost(url.host());
        reqId=http->get(url.toString().toUtf8());
    }
    if (!wikiOnly)
    {
        foreach(QUrl url, listOther)
        {
            http->setHost(url.host());
            reqId=http->get(url.toString().toUtf8());
        }

    }
    ui->labelStatus->setText(QString("Downloading data (")+QString::number(0)+QString(" from ")+
                             QString::number(reqId)+QString(")"));
}

/**
 * @brief ExportMapDialog::on_pushButtonRef_clicked handles refresh button click event
 */
void ExportMapDialog::on_pushButtonRef_clicked()
{
    infoitem.clear();

    for (int i=0; i< ui->listWidget->count(); i++)
    {
        QListWidgetItem* item = ui->listWidget->item(i);
        if (item->checkState() == Qt::Checked)
        {
            QString str=itemDataList.value(item);

            QString headline= (item->text().contains("Custom item")) ? QString("") : item->text() + "\n";

            if (deleteNewlines)
                infoitem += headline + str.replace("\n", "   ") + "\n\n";
            else
                infoitem += headline + str + "\n\n";
        }
    }
    updatePixmap();
    adjustMiniature();
}

/**
 * @brief ExportMapDialog::on_pushButtonAdd_clicked handles add button click event
 */
void ExportMapDialog::on_pushButtonAdd_clicked()
{
    QListWidgetItem *item=new QListWidgetItem("Custom item " + QString::number(++listCounter),ui->listWidget);
    item->setFlags(item->flags()|Qt::ItemIsUserCheckable|Qt::ItemIsEditable);
    item->setCheckState(Qt::Checked);

    itemDataList.insert(item,QString("empty!"));

}

/**
 * @brief ExportMapDialog::on_pushButtonRemove_clicked handles remove button click event
 */
void ExportMapDialog::on_pushButtonRemove_clicked()
{
    if (ui->listWidget->count() == 0)
        return;

    if (ui->listWidget->selectedItems().isEmpty())
        return;

    QListWidgetItem *item= ui->listWidget->selectedItems().first();

//    qDebug()<<"remove item "<< item->text();

    itemDataList.remove(item);
    delete item;

    if (ui->listWidget->count() == 0)
        ui->textEdit->clear();
}

/**
 * @brief ExportMapDialog::on_pushButtonMoveup_clicked handles moveup button click event
 */
void ExportMapDialog::on_pushButtonMoveup_clicked()
{
    if (ui->listWidget->count() == 0)
        return;
    if (ui->listWidget->selectedItems().isEmpty())
        return;

    int row=ui->listWidget->currentRow();
    if (row == 0)
        return;

    QListWidgetItem *item=ui->listWidget->takeItem(row);
    ui->listWidget->insertItem(row-1,item);
    ui->listWidget->setCurrentRow(row-1);
}

/**
 * @brief ExportMapDialog::on_pushButtonMovedown_clicked handles movedown button click event
 */
void ExportMapDialog::on_pushButtonMovedown_clicked()
{
    if (ui->listWidget->count() == 0)
        return;
    if (ui->listWidget->selectedItems().isEmpty())
        return;

    int row=ui->listWidget->currentRow();
    if (row == ui->listWidget->count()-1)
        return;

    QListWidgetItem *item=ui->listWidget->takeItem(row);
    ui->listWidget->insertItem(row+1,item);
    ui->listWidget->setCurrentRow(row+1);
}

/**
 * @brief ExportMapDialog::on_lineEditTitle_textEdited change title text
 * @param arg1
 */
void ExportMapDialog::on_lineEditTitle_textEdited(const QString &arg1)
{
    Q_UNUSED(arg1)
    updatePixmap();
    adjustMiniature();
}

/**
 * @brief ExportMapDialog::on_checkBoxWikiURL_stateChanged handles WIKI checkbox change
 * @param i
 */
void ExportMapDialog::on_checkBoxWikiURL_stateChanged(int i)
{
    Q_UNUSED(i)
    wikiOnly = (ui->checkBoxWikiURL->isChecked());
}

/**
 * @brief ExportMapDialog::on_spinBoxInfoTextSize_valueChanged handles info text size change
 * @param i
 */
void ExportMapDialog::on_spinBoxInfoTextSize_valueChanged(int i)
{
    infoTextSize=i;
    fontInfo.setPointSize(i);
    adjustMiniature();
}

/**
 * @brief ExportMapDialog::on_checkBoxDeleteNewlines_stateChanged handles checkbox change
 * @param i
 */
void ExportMapDialog::on_checkBoxDeleteNewlines_stateChanged(int i)
{
    Q_UNUSED(i)
    deleteNewlines=(ui->checkBoxDeleteNewlines->isChecked());
}

/**
 * @brief ExportMapDialog::on_spinBoxScale_valueChanged handles scale value change
 * @param i
 */
void ExportMapDialog::on_spinBoxScale_valueChanged(int i)
{
    Q_UNUSED(i)
    adjustMiniature();
}
