/**
  * @file editlayerdialog.cpp
  * @author Martin Latta
  * Map Creation Application from OpenStreetMap Project Data
  * Faculty of Information Technology
  * Brno University of Technology
  * 2013
  * @brief dialog to edit selected layer
  */

#include "editlayerdialog.h"
#include "ui_editlayerdialog.h"

/**
 * @brief EditLayerDialog::EditLayerDialog constructor method
 * @param elem element to edit
 * @param list list of styles
 * @param parent
 */
EditLayerDialog::EditLayerDialog(TLayerElement elem, QStringList list, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::EditLayerDialog)
{
    ui->setupUi(this);

    this->setWindowTitle("Edit layer");

    path=QDir::homePath();
    path = QApplication::applicationDirPath();

    element=elem;
    listOfStyles=list;

    ui->lineEditLayerName->setText(element.name);

    for (int i=0; i < element.styleNames.count(); i++ )
    {
        QListWidgetItem *item= new QListWidgetItem( element.styleNames.at(i));
        ui->listWidgetStyles->addItem(item);
    }

    ui->textEditSrs->setText(element.attributes.value("srs", QString()));
    ui->lineEditAbstr->setText(element.attributes.value("abstract", QString()));
    ui->lineEditTitle->setText(element.attributes.value("title", QString()));


    ui->doubleSpinBoxMin->setValue(element.attributes.value("minzoom","0.0").toDouble());
    ui->doubleSpinBoxMax->setValue(element.attributes.value("maxzoom","0.0").toDouble());

    if (element.attributes.value("status","on") == "off")
        ui->checkBox1->setChecked(false);
    else
        ui->checkBox1->setChecked(true);

    if (element.attributes.value("clear-label-cache","off") == "on")
        ui->checkBox2->setChecked(true);
    else
        ui->checkBox2->setChecked(false);

    if (element.attributes.value("cache-features","off") == "on")
        ui->checkBox3->setChecked(true);
    else
        ui->checkBox3->setChecked(false);

    if (element.attributes.value("queryable","false") == "true")
        ui->checkBox4->setChecked(true);
    else
        ui->checkBox4->setChecked(false);

    if (element.datasources.contains("estimate_extent"))
    {
        if (element.datasources.value("estimate_extent")== "true")
            ui->checkBoxExtent->setChecked(true);
        else
            ui->checkBoxExtent->setChecked(false);
    }

    ui->lineEditExtent->setText(element.datasources.value("extent", QString()));

    // default type
    if (!element.datasources.contains("type"))
        element.datasources.insert("type","osm");

    QString datatype = element.datasources.value("type");

    if (datatype == "shape")
    {
        ui->tabWidget->setCurrentIndex(0);
        ui->lineEditShapefile->setText(element.datasources.value("file",QString()));
        ui->lineEditShapeenc->setText(element.datasources.value("encoding",QString()));

    }
    else if (datatype == "postgis")
    {
        ui->tabWidget->setCurrentIndex(1);
        ui->lineEditPGhost->setText(element.datasources.value("host",QString()));
        ui->lineEditPGname->setText(element.datasources.value("dbname",QString()));
        ui->lineEditPGpwd->setText(element.datasources.value("password",QString()));
        ui->lineEditPGusr->setText(element.datasources.value("user",QString()));
        ui->spinBoxPGport->setValue(element.datasources.value("port","0").toInt());
        ui->textEditPGtable->setText(element.datasources.value("table",QString()));

    }
    else if (datatype == "raster")
    {
        ui->tabWidget->setCurrentIndex(2);
        ui->lineEditRaster->setText(element.datasources.value("file",QString()));

    }
    else if (datatype == "gdal")
    {
        ui->tabWidget->setCurrentIndex(3);
        ui->lineEditGDALfile->setText(element.datasources.value("file",QString()));
        ui->lineEditGDALbase->setText(element.datasources.value("base",QString()));
        ui->doubleSpinBoxGDAL->setValue(element.datasources.value("filter_factor","0.0").toDouble());

    }
    else if (datatype == "ogr")
    {
        ui->tabWidget->setCurrentIndex(4);
        ui->lineEditOGRfile->setText(element.datasources.value("file",QString()));
        ui->lineEditOGRbase->setText(element.datasources.value("base",QString()));
        ui->lineEditOGRlayer->setText(element.datasources.value("layer",QString()));
        ui->spinBoxOGR->setValue(element.datasources.value("layer_by_index","0").toInt());
    }
    else if (datatype == "osm")
    {
        ui->tabWidget->setCurrentIndex(5);
        ui->lineEditOSMfile->setText(element.datasources.value("file",QString()));
        ui->lineEditOSMurl->setText(element.datasources.value("url",QString()));
        ui->lineEditOSMbbox->setText(element.datasources.value("bbox",QString()));
        ui->doubleSpinBoxOSM->setValue(element.datasources.value("filter_factor","0.0").toDouble());
    }

    connect(ui->pushButtonAddStyle, SIGNAL(clicked()),this, SLOT(addStyle()));
    connect(ui->pushButtonRemoveStyle, SIGNAL(clicked()),this, SLOT(removeStyle()));

    connect(ui->pushButtonSrs, SIGNAL(clicked()),this, SLOT(selectSrs()));
    connect(ui->pushButtonShape, SIGNAL(clicked()), this, SLOT(browseShape()));
    connect(ui->pushButtonRaster, SIGNAL(clicked()), this, SLOT(browseRaster()));
    connect(ui->pushButtonGDAL, SIGNAL(clicked()), this, SLOT(browseGdal()));
    connect(ui->pushButtonOGR, SIGNAL(clicked()), this, SLOT(browseOgr()));
    connect(ui->pushButtonOSM, SIGNAL(clicked()), this, SLOT(browseOsm()));
    connect(this, SIGNAL(accepted()),this, SLOT(save()));
}

/**
 * @brief EditLayerDialog::~EditLayerDialog destructor
 */
EditLayerDialog::~EditLayerDialog()
{
    delete ui;
}

/**
 * @brief EditLayerDialog::save saves dialog content
 */
void EditLayerDialog::save()
{

    element.name=ui->lineEditLayerName->text();

    element.attributes.clear();

    if (!ui->textEditSrs->toPlainText().isEmpty())
        element.attributes.insert("srs",ui->textEditSrs->toPlainText());
    else
        element.attributes.remove("srs");

    if (!ui->lineEditAbstr->text().isEmpty())
        element.attributes.insert("abstract",ui->lineEditAbstr->text());
    else
        element.attributes.remove("abstract");

    if (!ui->lineEditTitle->text().isEmpty())
        element.attributes.insert("title",ui->lineEditTitle->text());
    else
        element.attributes.remove("title");

    if (!ui->checkBox1->isChecked())
        element.attributes.insert("status","off");
    else
        element.attributes.remove("status");

    if (ui->checkBox2->isChecked())
        element.attributes.insert("clear-label-cache","on");
    else
        element.attributes.remove("clear-label-cache");

    if (ui->checkBox3->isChecked())
        element.attributes.insert("cache-features","on");
    else
        element.attributes.remove("cache-features");

    if (ui->checkBox4->isChecked())
        element.attributes.insert("queryable","true");
    else
        element.attributes.remove("queryable");


    if (ui->doubleSpinBoxMin->value() != 0.0)
        element.attributes.insert("minzoom",QString::number(ui->doubleSpinBoxMin->value()));
    else
        element.attributes.remove("minzoom");

    if (ui->doubleSpinBoxMax->value() != 0.0)
        element.attributes.insert("maxzoom",QString::number(ui->doubleSpinBoxMax->value()));
    else
        element.attributes.remove("maxzoom");

    if (ui->lineEditExtent->text().isEmpty())
    {
        element.datasources.insert("estimate_extent","true");
        element.datasources.remove("extent");
    }
    else
    {
        element.datasources.insert("estimate_extent","false");
        element.datasources.insert("extent",ui->lineEditExtent->text());
    }

    switch (ui->tabWidget->currentIndex())
    {
    case 0:
        if (!ui->lineEditShapefile->text().isEmpty())
            element.datasources.insert("file",ui->lineEditShapefile->text());
        else
            element.datasources.remove("file");
        if (!ui->lineEditShapeenc->text().isEmpty())
            element.datasources.insert("encoding",ui->lineEditShapeenc->text());
        else
            element.datasources.remove("encoding");

        element.datasources.insert("type","shape");

        break;
    case 1:
        if (!ui->lineEditPGhost->text().isEmpty())
            element.datasources.insert("host",ui->lineEditPGhost->text());
        else
            element.datasources.remove("host");

        if (!ui->lineEditPGname->text().isEmpty())
            element.datasources.insert("dbname",ui->lineEditPGname->text());
        else
            element.datasources.remove("dbname");

        if (!ui->lineEditPGusr->text().isEmpty())
            element.datasources.insert("user",ui->lineEditPGusr->text());
        else
            element.datasources.remove("user");

        if (!ui->lineEditPGpwd->text().isEmpty())
            element.datasources.insert("password",ui->lineEditPGpwd->text());
        else
            element.datasources.remove("password");

        if (ui->spinBoxPGport->value() != 0)
            element.datasources.insert("port",QString::number(ui->spinBoxPGport->value()));
        else
            element.datasources.remove("port");

        if (!ui->textEditPGtable->toPlainText().isEmpty())
            element.datasources.insert("table",ui->textEditPGtable->toPlainText());
        else
            element.datasources.remove("table");

        element.datasources.insert("type","postgis");
        break;
    case 2:
        if (!ui->lineEditRaster->text().isEmpty())
            element.datasources.insert("file",ui->lineEditRaster->text());
        element.datasources.insert("type","raster");
        break;
    case 3:
        if (!ui->lineEditGDALfile->text().isEmpty())
            element.datasources.insert("file",ui->lineEditGDALfile->text());
        else
            element.datasources.remove("file");
        if (!ui->lineEditGDALbase->text().isEmpty())
            element.datasources.insert("base",ui->lineEditGDALbase->text());
        else
            element.datasources.remove("base");

        if (ui->doubleSpinBoxGDAL->value() != 0.0)
            element.datasources.insert("filter_factor",QString::number(ui->doubleSpinBoxGDAL->value()));
        else
            element.datasources.remove("filter_factor");

        element.datasources.insert("type","gdal");
        break;
    case 4:
        if (!ui->lineEditOGRfile->text().isEmpty())
            element.datasources.insert("file",ui->lineEditOGRfile->text());
        else
            element.datasources.remove("file");
        if (!ui->lineEditOGRbase->text().isEmpty())
            element.datasources.insert("base",ui->lineEditOGRbase->text());
        else
            element.datasources.remove("base");
        if (!ui->lineEditOGRlayer->text().isEmpty())
            element.datasources.insert("layer",ui->lineEditOGRlayer->text());
        else
            element.datasources.remove("layer");

        if (ui->spinBoxOGR->value() != 0)
            element.datasources.insert("layer_by_index",QString::number(ui->spinBoxOGR->value()));
        else
            element.datasources.remove("layer_by_index");

        element.datasources.insert("type","ogr");
        break;
    case 5:
        if (!ui->lineEditOSMfile->text().isEmpty())
            element.datasources.insert("file",ui->lineEditOSMfile->text());
        else
            element.datasources.remove("file");
        if (!ui->lineEditOSMurl->text().isEmpty())
            element.datasources.insert("url",ui->lineEditOSMurl->text());
        else
            element.datasources.remove("url");
        if (!ui->lineEditOSMbbox->text().isEmpty())
            element.datasources.insert("bbox",ui->lineEditOSMbbox->text());
        else
            element.datasources.remove("bbox");

        if (ui->doubleSpinBoxOSM->value() != 0.0)
            element.datasources.insert("filter_factor",QString::number(ui->doubleSpinBoxOSM->value()));
        else
            element.datasources.remove("filter_factor");

        element.datasources.insert("type","osm");
        break;

    }
}


/**
 * @brief EditLayerDialog::addStyle add new style
 */
void EditLayerDialog::addStyle()
{
    bool ok;
    QString selectedStyle = QInputDialog::getItem(this, tr("Add style"), tr("select style:"),listOfStyles, 0, false, &ok);

    if (ok && !selectedStyle.isEmpty())
    {
        if (!element.styleNames.contains(selectedStyle))
        {
            QListWidgetItem *item= new QListWidgetItem(selectedStyle);
            ui->listWidgetStyles->addItem(item);
            element.styleNames.append(selectedStyle);
        }
    }
}

/**
 * @brief EditLayerDialog::removeStyle remove selected style
 */
void EditLayerDialog::removeStyle()
{
    if (ui->listWidgetStyles->selectedItems().count() !=1)
        return;

    QString name = ui->listWidgetStyles->currentItem()->text();

    int row=ui->listWidgetStyles->currentRow();
    ui->listWidgetStyles->takeItem(row);

    element.styleNames.removeOne(name);
}

/**
 * @brief EditLayerDialog::browseShape handles browse file button
 */
void EditLayerDialog::browseShape()
{
    QString name = QFileDialog::getOpenFileName(this, tr("Choose file"), path+"/", tr("All files (*.*)"));
    if (!name.isEmpty())
    {
        ui->lineEditShapefile->setText(name);
    }
}

/**
 * @brief EditLayerDialog::browseRaster handles browse file button
 */
void EditLayerDialog::browseRaster()
{
    QString name = QFileDialog::getOpenFileName(this, tr("Choose file"), path, tr("All files (*.*)"));
    if (!name.isEmpty())
    {
        ui->lineEditRaster->setText(name);
    }
}

/**
 * @brief EditLayerDialog::browseGdal handles browse file button
 */
void EditLayerDialog::browseGdal()
{
    QString name = QFileDialog::getOpenFileName(this, tr("Choose file"), path, tr("All files (*.*)"));
    if (!name.isEmpty())
    {
        ui->lineEditGDALfile->setText(name);
    }
}

/**
 * @brief EditLayerDialog::browseOgr handles browse file button
 */
void EditLayerDialog::browseOgr()
{
    QString name = QFileDialog::getOpenFileName(this, tr("Choose file"), path, tr("All files (*.*)"));
    if (!name.isEmpty())
    {
        ui->lineEditOGRfile->setText(name);
    }
}

/**
 * @brief EditLayerDialog::browseOsm handles browse file button
 */
void EditLayerDialog::browseOsm()
{
    QString name = QFileDialog::getOpenFileName(this, tr("Choose file"), path,  tr("All files (*.*)"));
    if (!name.isEmpty())
    {
        ui->lineEditOSMfile->setText(name);
    }
}

/**
 * @brief EditLayerDialog::selectSrs user can choose from default srs strings
 */
void EditLayerDialog::selectSrs()
{
    QStringList items;
    items << "+proj=merc +a=6378137 +b=6378137 +lat_ts=0.0 +lon_0=0.0 +x_0=0.0 +y_0=0 +k=1.0 +units=m +nadgrids=@null +no_defs +over";
    items << "+proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs";
    items << "+proj=merc +lon_0=0 +k=1 +x_0=0 +y_0=0 +ellps=WGS84 +datum=WGS84 +units=m +no_defs";

    bool ok;
    QString item = QInputDialog::getItem(this, tr("Select srs"),
                                         tr("Spatial reference system"), items, 0, false, &ok);
    if (ok && !item.isEmpty())
        ui->textEditSrs->setPlainText(item);
}
