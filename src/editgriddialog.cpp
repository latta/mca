/**
  * @file editgriddialog.cpp
  * @author Martin Latta
  * Map Creation Application from OpenStreetMap Project Data
  * Faculty of Information Technology
  * Brno University of Technology
  * 2013
  * @brief dialog to set grid parameters
  */

#include "editgriddialog.h"
#include "ui_editgriddialog.h"

/**
 * @brief EditGridDialog::EditGridDialog constructor method
 * @param params grid parameters
 * @param parent
 */
EditGridDialog::EditGridDialog(QHash<QString, QString> params, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::EditGridDialog)
{
    ui->setupUi(this);

    this->setWindowTitle("Grid");

    if (params.empty())
    {
        ui->lineEditColor->setText("black");
        ui->doubleSpinBoxOpacity->setValue(1.0);
        //ui->doubleSpinBoxStep->setValue(1.0);
        ui->doubleSpinBoxWidth->setValue(1.0);
        ui->doubleSpinBoxSec->setValue(0.0);
        ui->spinBoxMin->setValue(1);
        ui->spinBoxDeg->setValue(0);
    }
    else
    {
        ui->groupBox->setChecked(true);
        ui->lineEditColor->setText(params.value("color"));
        ui->doubleSpinBoxOpacity->setValue(params.value("opacity","1.0").toDouble());
        //ui->doubleSpinBoxStep->setValue(params.value("step","1.0").toDouble());
        ui->doubleSpinBoxWidth->setValue(params.value("width","1.0").toDouble());
        ui->doubleSpinBoxSec->setValue(params.value("sec","0.0").toDouble());
        ui->spinBoxMin->setValue(params.value("min","1").toInt());
        ui->spinBoxDeg->setValue(params.value("deg","0").toInt());
    }


    connect(ui->pushButtonColor, SIGNAL(clicked()), this, SLOT(editColor()));
    connect(this, SIGNAL(accepted()),this, SLOT(save()));
}

/**
 * @brief EditGridDialog::~EditGridDialog destructor
 */
EditGridDialog::~EditGridDialog()
{
    delete ui;
}

/**
 * @brief EditGridDialog::save saves dialog content
 */
void EditGridDialog::save()
{
    gridParams.clear();

    if (ui->groupBox->isChecked())
    {
        if (ui->lineEditColor->text() != QString())
            gridParams.insert("color",ui->lineEditColor->text());
        else
            gridParams.insert("color","black");

        if (ui->doubleSpinBoxOpacity->value() != 1.0)
            gridParams.insert("opacity",QString::number(ui->doubleSpinBoxOpacity->value()));
        else
            gridParams.insert("opacity","1.0");

        if (ui->doubleSpinBoxWidth->value() != 1.0)
            gridParams.insert("width",QString::number(ui->doubleSpinBoxWidth->value()));
        else
            gridParams.insert("width","1.0");

        if (ui->doubleSpinBoxSec->value() != 0.0)
            gridParams.insert("sec",QString::number(ui->doubleSpinBoxSec->value()));
        else
            gridParams.insert("sec","0.0");

        if (ui->spinBoxMin->value() != 0)
            gridParams.insert("min",QString::number(ui->spinBoxMin->value()));
        else
            gridParams.insert("min","0");

        if (ui->spinBoxDeg->value() != 0)
            gridParams.insert("deg",QString::number(ui->spinBoxDeg->value()));
        else
            gridParams.insert("deg","0");
    }
}


/**
 * @brief EditGridDialog::editColor handles edit color button
 */
void EditGridDialog::editColor()
{
    QColor initColor;
    if (ui->lineEditColor->text() == "black")
        initColor=Qt::black;
    else
    {
        QString s=ui->lineEditColor->text();
        s.remove(0,4);
        s.remove(s.length()-1,1);
        if (s.split(",").count()!=3)
            initColor=Qt::black;
        else
        {
            initColor.setRed(s.split(",").at(0).toInt());
            initColor.setGreen(s.split(",").at(1).toInt());
            initColor.setBlue(s.split(",").at(2).toInt());
        }
    }
    QColor color = QColorDialog::getColor(initColor, this);
    if (color.isValid())
    {
        ui->lineEditColor->setText("rgb("+QString::number(color.red())+","+QString::number(color.green())+
                                   ","+QString::number(color.blue())+")");
    }
}
