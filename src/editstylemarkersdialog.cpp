/**
  * @file editstylemarkersdialog.cpp
  * @author Martin Latta
  * Map Creation Application from OpenStreetMap Project Data
  * Faculty of Information Technology
  * Brno University of Technology
  * 2013
  * @brief dialog to edit MarkersSymbolizer
  */

#include "editstylemarkersdialog.h"
#include "ui_editstylemarkersdialog.h"

/**
 * @brief EditStyleMarkersDialog::EditStyleMarkersDialog constructor method
 * @param a default dialog content
 * @param parent
 */
EditStyleMarkersDialog::EditStyleMarkersDialog(QHash<QString, QString> a,QWidget *parent) :
    QDialog(parent),
    ui(new Ui::EditStyleMarkersDialog)
{
    ui->setupUi(this);

    this->setWindowTitle("Edit MarkersSymbolizer");

    ui->lineEditFile->setText(a.value("file",QString()));
    ui->lineEditFill->setText(a.value("fill",QString()));
    ui->lineEditStroke->setText(a.value("stroke",QString()));
    ui->lineEditTransform->setText(a.value("transform",QString()));

    ui->spinBoxSpacing->setValue(a.value("spacing","100").toInt());
    ui->spinBoxWidth->setValue(a.value("width","10").toInt());
    ui->spinBoxHeight->setValue(a.value("height","10").toInt());

    ui->doubleSpinBoxMerror->setValue(a.value("max-error","0.2").toDouble());
    ui->doubleSpinBoxOpacity->setValue(a.value("opacity","1.0").toDouble());
    ui->doubleSpinBoxSWidth->setValue(a.value("stroke-width","1.0").toDouble());
    ui->doubleSpinBoxSOpacity->setValue(a.value("stroke-opacity","1.0").toDouble());


    if (a.value("ignore-placement") == "true")
        ui->checkBoxIgnoreplacement->setChecked(true);
    else
        ui->checkBoxIgnoreplacement->setChecked(false);


    if (a.value("allow-overlap") == "true")
        ui->checkBoxAllowoverlap->setChecked(true);
    else
        ui->checkBoxAllowoverlap->setChecked(false);

    QString s;
    s=a.value("placement");
    if (s == "line")
        ui->comboBoxPlacement->setCurrentIndex(1);
    else
        ui->comboBoxPlacement->setCurrentIndex(0);


    s=a.value("marker-type");
    if (s == "arrow")
        ui->comboBoxType->setCurrentIndex(1);
    else
        ui->comboBoxType->setCurrentIndex(0);

    connect(ui->pushButtonFill, SIGNAL(clicked()),this, SLOT(colorFill()));
    connect(ui->pushButtonStroke, SIGNAL(clicked()),this, SLOT(colorStroke()));

    connect(ui->pushButtonBrowse, SIGNAL(clicked()), this, SLOT(browseFile()));
    connect(this, SIGNAL(accepted()),this, SLOT(save()));
}

/**
 * @brief EditStyleMarkersDialog::~EditStyleMarkersDialog destructor
 */
EditStyleMarkersDialog::~EditStyleMarkersDialog()
{
    delete ui;
}

/**
 * @brief EditStyleMarkersDialog::save saves dialog content
 */
void EditStyleMarkersDialog::save()
{

    if (!ui->lineEditFile->text().isEmpty())
        attribs.insert("file",ui->lineEditFile->text());
    if (!ui->lineEditFill->text().isEmpty())
        attribs.insert("fill",ui->lineEditFill->text());
    if (!ui->lineEditStroke->text().isEmpty())
        attribs.insert("stroke",ui->lineEditStroke->text());
    if (!ui->lineEditTransform->text().isEmpty())
        attribs.insert("transform",ui->lineEditTransform->text());


    if (ui->spinBoxSpacing->value()!=100)
        attribs.insert("spacing",QString::number(ui->spinBoxSpacing->value()));
    if (ui->spinBoxWidth->value()!=10)
        attribs.insert("width",QString::number(ui->spinBoxWidth->value()));
    if (ui->spinBoxHeight->value()!=10)
        attribs.insert("height",QString::number(ui->spinBoxHeight->value()));

    if (ui->doubleSpinBoxMerror->value()!=0.2)
        attribs.insert("max-error",QString::number(ui->doubleSpinBoxMerror->value()));
    if (ui->doubleSpinBoxOpacity->value()!=1.0)
        attribs.insert("opacity",QString::number(ui->doubleSpinBoxOpacity->value()));
    if (ui->doubleSpinBoxSWidth->value()!=1.0)
        attribs.insert("stroke-width",QString::number(ui->doubleSpinBoxSWidth->value()));
    if (ui->doubleSpinBoxSOpacity->value()!=1.0)
        attribs.insert("stroke-opacity",QString::number(ui->doubleSpinBoxSOpacity->value()));


    if (ui->checkBoxIgnoreplacement->isChecked())
        attribs.insert("ignore-placement","true");
    if (ui->checkBoxAllowoverlap->isChecked())
        attribs.insert("allow-overlap","true");

    if (ui->comboBoxPlacement->currentIndex() == 1)
    {
        attribs.insert("placement","line");
    }

    if (ui->comboBoxType->currentIndex() == 1)
    {
        attribs.insert("marker-type","arrow");

    }
}

/**
 * @brief EditStyleMarkersDialog::browseFile handles browse file button click event
 */
void EditStyleMarkersDialog::browseFile()
{
    QString name = QFileDialog::getOpenFileName(this, tr("Choose file"), QDir::homePath()+"/",
                                                tr("Pictures (*.jpg *.jpeg *.gif *.png *.svg);;All files (*.*)"));
    if (!name.isEmpty())
    {
        ui->lineEditFile->setText(name);
    }
}


/**
 * @brief EditStyleMarkersDialog::colorFill handles color button click event
 */
void EditStyleMarkersDialog::colorFill()
{
    QColor initColor;
    if (ui->lineEditFill->text() == "blue")
        initColor=Qt::blue;
    else
    {
        QString s=ui->lineEditFill->text();
        s.remove(0,4);
        s.remove(s.length()-1,1);

        if (s.split(",").count()!=3)
            initColor=Qt::yellow;
        else
        {
            initColor.setRed(s.split(",").at(0).toInt());
            initColor.setGreen(s.split(",").at(1).toInt());
            initColor.setBlue(s.split(",").at(2).toInt());
        }
    }
    QColor color = QColorDialog::getColor(initColor, this);
    if (color.isValid())
    {
        ui->lineEditFill->setText("rgb("+QString::number(color.red())+","+QString::number(color.green())+
                                  ","+QString::number(color.blue())+")");
    }
}

/**
 * @brief EditStyleMarkersDialog::colorStroke handles color button click event
 */
void EditStyleMarkersDialog::colorStroke()
{
    QColor initColor;
    if (ui->lineEditStroke->text() == "black")
        initColor=Qt::red;
    else
    {
        QString s=ui->lineEditStroke->text();
        s.remove(0,4);
        s.remove(s.length()-1,1);
        if (s.split(",").count()!=3)
            initColor=Qt::yellow;
        else
        {
            initColor.setRed(s.split(",").at(0).toInt());
            initColor.setGreen(s.split(",").at(1).toInt());
            initColor.setBlue(s.split(",").at(2).toInt());
        }
    }
    QColor color = QColorDialog::getColor(initColor, this);
    if (color.isValid())
    {
        ui->lineEditStroke->setText("rgb("+QString::number(color.red())+","+QString::number(color.green())+
                                    ","+QString::number(color.blue())+")");
    }
}
