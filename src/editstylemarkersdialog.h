/**
  * @file editstylemarkersdialog.h
  * @author Martin Latta
  * Map Creation Application from OpenStreetMap Project Data
  * Faculty of Information Technology
  * Brno University of Technology
  * 2013
  * @brief dialog to edit MarkersSymbolizer
  */

#ifndef EDITSTYLEMARKERSDIALOG_H
#define EDITSTYLEMARKERSDIALOG_H

#include <QDialog>
#include <QtCore>
#include <QtGui>

namespace Ui {
class EditStyleMarkersDialog;
}

/**
 * @brief The EditStyleMarkersDialog class representing dialog to edit MarkersSymbolizer
 */
class EditStyleMarkersDialog : public QDialog
{
    Q_OBJECT
    
public:
    explicit EditStyleMarkersDialog(QHash<QString, QString> a, QWidget *parent = 0);
    ~EditStyleMarkersDialog();

    QHash<QString, QString> attribs; /**< all dialog paramaters */

private slots:
    void save() ;
    void browseFile();
    void colorFill();
    void colorStroke();

private:
    Ui::EditStyleMarkersDialog *ui;
};

#endif // EDITSTYLEMARKERSDIALOG_H
