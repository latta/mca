/**
  * @file main.cpp
  * @author Martin Latta
  * Map Creation Application from OpenStreetMap Project Data
  * Faculty of Information Technology
  * Brno University of Technology
  * 2013
  * @brief
  */

#include <QApplication>
#include <QtCore>
#include "mainwindow.h"

#include <mapnik/datasource_cache.hpp>
#include <mapnik/font_engine_freetype.hpp>

/**
 * @brief main function
 * @param argc
 * @param argv
 * @return
 */
int main(int argc, char *argv[])
{

    std::string mapnik_input("/usr/local/lib/mapnik/input");
    std::string mapnik_fonts("/usr/local/lib/mapnik/fonts");

    try
    {
        if (argc == 2)
        {
            mapnik_input.assign(argv[1]);
        }
        else if (argc == 3)
        {
            mapnik_input.assign(argv[1]);
            mapnik_fonts.assign(argv[2]);
        }
    }
    catch (...)
    {
        std::cerr << "Unknown parametr";
    }

    mapnik::datasource_cache::instance().register_datasources(mapnik_input);

    QDir dir;
    dir.setPath(QString::fromStdString(mapnik_fonts));

    QFileInfoList list = dir.entryInfoList();
    for (int i = 0; i < list.size(); ++i)
    {
        if (list.at(i).isDir())
        {
            QDir subdir(list.at(i).absoluteFilePath());
            QFileInfoList sublist = subdir.entryInfoList();
            for (int j = 0; j < sublist.size(); ++j)
            {
                mapnik::freetype_engine::register_font(sublist.at(j).absoluteFilePath().toStdString());
            }
        }
        else
        {
            mapnik::freetype_engine::register_font(list.at(i).absoluteFilePath().toStdString());
        }
    }

    QApplication a(argc, argv);
    MainWindow w;
    w.show();
    
    return a.exec();
}
