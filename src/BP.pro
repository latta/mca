#-------------------------------------------------
#
# Project created by QtCreator 2013-01-03T17:17:31
#
#-------------------------------------------------


#QMAKE_CXX = clang++
QMAKE_CXXFLAGS += $$system(mapnik-config --cxxflags)
QMAKE_LFLAGS += $$system(mapnik-config --libs)
QMAKE_LFLAGS += $$system(mapnik-config --ldflags --dep-libs)
QMAKE_LFLAGS += -lboost_timer
INCLUDEPATH += /usr/local/include/mapnik /usr/include/cairo /usr/local/include/mapnik/agg /usr/include/freetype2

QT       += core gui xml network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = BP
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    stylexmlparser.cpp \
    editstyledialog.cpp \
    stylesheet.cpp \
    editlayerdialog.cpp \
    render.cpp \
    editstylesheetdialog.cpp \
    editstyletextdialog.cpp \
    editstyleshielddialog.cpp \
    editstylemarkersdialog.cpp \
    editgriddialog.cpp \
    exportmapdialog.cpp \
    optionsdialog.cpp \
    editmapsizedialog.cpp

HEADERS  += mainwindow.h \
    stylexmlparser.h \
    editstyledialog.h \
    stylesheet.h \
    editlayerdialog.h \
    render.h \
    editstylesheetdialog.h \
    editstyletextdialog.h \
    editstyleshielddialog.h \
    editstylemarkersdialog.h \
    editgriddialog.h \
    exportmapdialog.h \
    optionsdialog.h \
    editmapsizedialog.h

FORMS    += mainwindow.ui \
    editstyledialog.ui \
    editlayerdialog.ui \
    editstylesheetdialog.ui \
    editstyletextdialog.ui \
    editstyleshielddialog.ui \
    editstylemarkersdialog.ui \
    editgriddialog.ui \
    exportmapdialog.ui \
    optionsdialog.ui \
    editmapsizedialog.ui

RESOURCES += \
    icons.qrc
