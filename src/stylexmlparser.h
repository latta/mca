/**
  * @file stylexmlparser.h
  * @author Martin Latta
  * Map Creation Application from OpenStreetMap Project Data
  * Faculty of Information Technology
  * Brno University of Technology
  * 2013
  * @brief this class converts XML file into StyleSheet class representation and back
  */

#ifndef STYLEXMLPARSER_H
#define STYLEXMLPARSER_H

#include "stylesheet.h"

#include <QtCore>
#include <QtGui>
#include <QDomDocument>

#include <iostream>

/**
 * @brief The StyleXmlParser class converts XML file into StyleSheet class representation and back
 */
class StyleXmlParser : public QObject
{
public:
    StyleXmlParser(QListWidget *styleWidget, QListWidget *layerWidget, StyleSheet *slist, QString sname);
    bool readFile();
    bool writeFile(QString filename);
    void createFile();
    void parseStyleSheetElements();

    QDomDocument updateStyleSheet();

    QDomElement root; /**< root element of XML document */

private:
    QDomDocument doc; /**< parent XML document */
    QListWidget *styleListWidget; /**< QListWidget with styles */
    QListWidget *layerListWidget; /**< QListWidget with layers */
    StyleSheet *styleSheet; /**< StyleSheet object */

    QString defaultStylesheetName; /**< default stylesheet name */

};

#endif // STYLEXMLPARSER_H
