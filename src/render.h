/**
  * @file render.h
  * @author Martin Latta
  * Map Creation Application from OpenStreetMap Project Data
  * Faculty of Information Technology
  * Brno University of Technology
  * 2013
  * @brief this class renders image using Mapnik library
  */

#ifndef RENDER_H
#define RENDER_H

#include <QtCore>
#include <QtGui>
#include <QDomDocument>
#include <QMessageBox>

#include "math.h"

#include <mapnik/map.hpp>
#include <mapnik/layer.hpp>
#include <mapnik/rule.hpp>
#include <mapnik/line_symbolizer.hpp>
#include <mapnik/polygon_symbolizer.hpp>
#include <mapnik/text_symbolizer.hpp>
#include <mapnik/feature_type_style.hpp>
#include <mapnik/graphics.hpp>
#include <mapnik/agg_renderer.hpp>
#include <mapnik/expression.hpp>
#include <mapnik/scale_denominator.hpp>
#include <mapnik/color_factory.hpp>
#include <mapnik/image_util.hpp>
#include <mapnik/config_error.hpp>
#include <mapnik/load_map.hpp>

#include <mapnik/datasource_cache.hpp>
#include <mapnik/font_engine_freetype.hpp>

#define PI 3.14159265
#define MAPNIK_PPI 90.7

/**
 * @brief The Render class renders image using Mapnik library
 */
class Render : public QThread
{
    Q_OBJECT
public:
    Render(QMutex *mutex1, QObject *parent=0);
    ~Render();


    void render(QString fname, QString sname, int ww, int hh, int aa, double minlon, double minlat,
                              double maxlon, double maxlat, QHash<QString, QString> gridParams);

signals:
    void renFinished(const QImage &image,QString,double, double);
    void renFailed(QString);
    void renStatus(QString);

protected:
    void run();

private:
    void generateGrid(QHash<QString, QString> params, double minlon, double minlat, double maxlon, double maxlat);

    double minlon, minlat, maxlon, maxlat; /**< bounding box */
    int w,h,a; /**< map parameters */
    QHash<QString, QString> gridParams; /**< grid parameters */

    QString defStyleFile; /**< default stylesheet filename */
    QString defOsmFile; /**< default OSM filename */

    QMutex *mutex; /**< shared mutex */
};

#endif // RENDER_H
