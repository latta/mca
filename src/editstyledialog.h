/**
  * @file editstyledialog.h
  * @author Martin Latta
  * Map Creation Application from OpenStreetMap Project Data
  * Faculty of Information Technology
  * Brno University of Technology
  * 2013
  * @brief dialog to edit selected style
  */

#ifndef EDITSTYLEDIALOG_H
#define EDITSTYLEDIALOG_H

#include "stylesheet.h"
#include "editstyletextdialog.h"
#include "editstyleshielddialog.h"
#include "editstylemarkersdialog.h"

#include <QDialog>


namespace Ui {
class EditStyleDialog;
}

/**
 * @brief The EditStyleDialog class representing dialog to edit selected style
 */
class EditStyleDialog : public QDialog
{
    Q_OBJECT
    
public:
    explicit EditStyleDialog(TStyleElement elem, QWidget *parent = 0);
    ~EditStyleDialog();


    TStyleElement element; /**< element with style information */
    
private:
    Ui::EditStyleDialog *ui;
    void updateDialog();

    bool moving;
    int ruleCounter;
    int currentRule;

    QHash<QString, QString> textHash; /**< TextSymbolizer parameters */
    QHash<QString, QString> shieldHash; /**< ShieldSymbolizer parameters */
    QHash<QString, QString> markersHash; /**< MarkersSymbolizer parameters */

private slots:
    void styleNameEdited(QString s);
    void addNewRule();
    void removeRule();
    void saveRule();
    void currentSelectionChanged();
    void moveUp();
    void moveDown();

    void editTextSymbolizer();
    void editShieldSymbolizer();
    void editMarkersSymbolizer();
    void colorLineSymbolizer();
    void colorLineSymbolizer2();
    void colorLineSymbolizer3();
    void colorPolygonSymbolizer();
    void colorBuildingSymbolizer();
    void browsePointSymbolizer();
    void browseLinePatternSymbolizer();
    void browsePolygonPatternSymbolizer();

};

#endif // EDITSTYLEDIALOG_H
