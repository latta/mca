/**
  * @file exportmapdialog.h
  * @author Martin Latta
  * Map Creation Application from OpenStreetMap Project Data
  * Faculty of Information Technology
  * Brno University of Technology
  * 2013
  * @brief dialog to export map image
  */

#ifndef EXPORTMAPDIALOG_H
#define EXPORTMAPDIALOG_H

#include <QDialog>
#include <QtCore>
#include <QtGui>
#include <QtNetwork/QHttp>
#include <QDomDocument>

#define MAPNIK_PPI 90.7

namespace Ui {
class ExportMapDialog;
}

/**
 * @brief The ExportMapDialog class representing dialog to export map image
 */
class ExportMapDialog : public QDialog
{
    Q_OBJECT
    
public:
    explicit ExportMapDialog(QPixmap pix, double sc,double res, double rat, QStringList listOther,
                             QStringList listWiki, QWidget *parent = 0);
    ~ExportMapDialog();

    QGraphicsRectItem *getRect() const;
    void setRect(QGraphicsRectItem *value);

private:
    Ui::ExportMapDialog *ui;
    void adjustMiniature();
    void getFilename();
    void updatePixmap();
    void allDownloadFinished();

    QPixmap pixmapMap; /**< input pixmap */
    QPixmap pixmapOut; /**< output pixmap */
    QGraphicsScene *scene; /**< graphics scene */

    double scale; /**< map scale */
    double ratio; /** map ratio */
    double resolution; /**< map resolution */

    int w,h; /**< map size */
    int titleTextSize; /**< title text size */
    int marginSize; /**< margin size */
    int infoSize;   /**< additional information */
    int infoTextSize;

    QGraphicsRectItem *rectitem; /**< rectangle item */
    QGraphicsPixmapItem *pixmapitem; /**< pixmap item */
    QString infoitem; /**< string with text informations */

    QSize sizeOfView; /**< size of view */
    QFont fontTitle; /**< title font */
    QFont fontInfo; /**< font of informations */

    QHttp *http; /**< http requests */
    QList<QString> arrayList; /**< array of downloaded data */
    QHash<QListWidgetItem*, QString> itemDataList; /**< list of data items */

    int reqId,listCounter; /**< counters */
    bool access; /**< access */

    QString filename; /**< output filename */
    QString fileformat; /**< output file format */
    QStringList infoMapOther; /**< list of names from map */
    QStringList infoMapWiki; /**< list of URLs from map */
    bool wikiOnly; /**< enables wiki only */
    bool deleteNewlines; /** enables deleting newlines */


private slots:
    void widthChange(QString s);
    void heightChange(QString s);
    void comboBoxFormat(QString s);
    void save();
    void downloadFinished(int id, bool error);
    void listSelectionChanged();
    void textEditChanged();

    void on_spinBoxTitle_valueChanged(int i);
    void on_spinBoxMargin_valueChanged(int i);
    void on_checkBoxRotate_stateChanged(int i);
    void on_spinBoxInfo_valueChanged(int i);
    void on_pushButtonImport_clicked();
    void on_pushButtonRef_clicked();
    void on_pushButtonAdd_clicked();
    void on_pushButtonRemove_clicked();
    void on_pushButtonMoveup_clicked();
    void on_pushButtonMovedown_clicked();
    void on_lineEditTitle_textEdited(const QString &arg1);
    void on_checkBoxWikiURL_stateChanged(int i);
    void on_spinBoxInfoTextSize_valueChanged(int i);
    void on_checkBoxDeleteNewlines_stateChanged(int i);
    void on_spinBoxScale_valueChanged(int i);
};

#endif // EXPORTMAPDIALOG_H
