/**
  * @file optionsdialog.h
  * @author Martin Latta
  * Map Creation Application from OpenStreetMap Project Data
  * Faculty of Information Technology
  * Brno University of Technology
  * 2013
  * @brief dialog to set application settings
  */

#ifndef OPTIONSDIALOG_H
#define OPTIONSDIALOG_H

#include <QDialog>

namespace Ui {
class OptionsDialog;
}

/**
 * @brief The OptionsDialog class representing dialog to set application settings
 */
class OptionsDialog : public QDialog
{
    Q_OBJECT
    
public:
    explicit OptionsDialog(QString s1, QString s2, QString url, double ratio, QWidget *parent = 0);
    ~OptionsDialog();

    QString defaultOSMfilename; /**< default OSM filename */
    QString defaultStylesheetName; /**< default Stylesheet name */
    QString defaultURL; /**< default URL */
    double exportRatio; /** export map ratio */
    
private:
    Ui::OptionsDialog *ui;

private slots:
    void save();
    void on_pushButton_clicked();
};

#endif // OPTIONSDIALOG_H
