/**
  * @file editgriddialog.h
  * @author Martin Latta
  * Map Creation Application from OpenStreetMap Project Data
  * Faculty of Information Technology
  * Brno University of Technology
  * 2013
  * @brief dialog to set grid parameters
  */

#ifndef EDITGRIDDIALOG_H
#define EDITGRIDDIALOG_H

#include <QDialog>
#include <QtCore>
#include <QtGui>

namespace Ui {
class EditGridDialog;
}

/**
 * @brief The EditGridDialog class representing dialog to set grid parameters
 */
class EditGridDialog : public QDialog
{
    Q_OBJECT
    
public:
    explicit EditGridDialog(QHash<QString, QString> params, QWidget *parent = 0);
    ~EditGridDialog();

    QHash<QString, QString> gridParams; /**< parameters of grid */
    
private:
    Ui::EditGridDialog *ui;

private slots:
    void save();
    void editColor();

};

#endif // EDITGRIDDIALOG_H
