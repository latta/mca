/**
  * @file editstylesheetdialog.h
  * @author Martin Latta
  * Map Creation Application from OpenStreetMap Project Data
  * Faculty of Information Technology
  * Brno University of Technology
  * 2013
  * @brief dialog to edit stylesheet
  */

#ifndef EDITSTYLESHEETDIALOG_H
#define EDITSTYLESHEETDIALOG_H

#include <QtCore>
#include <QtGui>
#include <QDialog>

namespace Ui {
class EditStylesheetDialog;
}

/**
 * @brief The EditStylesheetDialog class representing dialog to edit stylesheet
 */
class EditStylesheetDialog : public QDialog
{
    Q_OBJECT
    
public:
    explicit EditStylesheetDialog(QString name, QHash<QString, QString> a, QWidget *parent = 0);
    ~EditStylesheetDialog();
    
    QHash<QString, QString> attribs; /**< all dialog paramaters */

private slots:
    void save();
    void colorEdit();
    void browseFile();
    void browseDir();
    void selectSrs();

private:
    Ui::EditStylesheetDialog *ui;


};

#endif // EDITSTYLESHEETDIALOG_H
