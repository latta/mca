/**
  * @file editstyleshielddialog.h
  * @author Martin Latta
  * Map Creation Application from OpenStreetMap Project Data
  * Faculty of Information Technology
  * Brno University of Technology
  * 2013
  * @brief dialog to edit ShieldSymbolizer
  */

#ifndef EDITSTYLESHIELDDIALOG_H
#define EDITSTYLESHIELDDIALOG_H

#include <QDialog>
#include <QtCore>
#include <QtGui>

namespace Ui {
class EditStyleShieldDialog;
}

/**
 * @brief The EditStyleShieldDialog class representing dialog to edit ShieldSymbolizer
 */
class EditStyleShieldDialog : public QDialog
{
    Q_OBJECT
    
public:
    explicit EditStyleShieldDialog(QHash<QString, QString> a, QWidget *parent = 0);
    ~EditStyleShieldDialog();

    QHash<QString, QString> attribs; /**< all dialog paramaters */

private slots:
    void save();
    void browseFile();
    void colorFill();
    void colorHalo();

private:
    Ui::EditStyleShieldDialog *ui;
};

#endif // EDITSTYLESHIELDDIALOG_H
