/**
  * @file editmapsizedialog.cpp
  * @author Martin Latta
  * Map Creation Application from OpenStreetMap Project Data
  * Faculty of Information Technology
  * Brno University of Technology
  * 2013
  * @brief dialog to adjust map size
  */

#include "editmapsizedialog.h"
#include "ui_editmapsizedialog.h"

/**
 * @brief EditMapSizeDialog::EditMapSizeDialog constructor method
 * @param w1 map width
 * @param h1 map height
 * @param a1 map aspect fix mode
 * @param parent
 */
EditMapSizeDialog::EditMapSizeDialog(int w1, int h1, int a1, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::EditMapSizeDialog)
{
    ui->setupUi(this);

    this->setWindowTitle("Edit map size");

    ui->spinBoxH->setValue(h1);
    ui->spinBoxW->setValue(w1);
    ui->comboBox->setCurrentIndex(a1);

    connect(this, SIGNAL(accepted()),this, SLOT(save()));
}

/**
 * @brief EditMapSizeDialog::~EditMapSizeDialog destructor
 */
EditMapSizeDialog::~EditMapSizeDialog()
{
    delete ui;
}

/**
 * @brief EditMapSizeDialog::save saves dialog content
 */
void EditMapSizeDialog::save()
{
    w=ui->spinBoxW->value();
    h=ui->spinBoxH->value();
    a=ui->comboBox->currentIndex();
}

