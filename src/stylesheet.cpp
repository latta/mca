/**
  * @file stylesheet.cpp
  * @author Martin Latta
  * Map Creation Application from OpenStreetMap Project Data
  * Faculty of Information Technology
  * Brno University of Technology
  * 2013
  * @brief this class represents Mapnik XML file structure
  */

#include "stylesheet.h"

/**
 * @brief StyleSheet::StyleSheet constructor method
 */
StyleSheet::StyleSheet()
{

}

/**
 * @brief StyleSheet::clearAll clears content of object
 */
void StyleSheet::clearAll()
{
    styleList.clear();
    layerList.clear();
    attribs.clear();
}

/**
 * @brief StyleSheet::addStyle adds new element into stylelist
 * @param domElem new element
 */
void StyleSheet::addStyle(QDomElement domElem)
{
    TStyleElement element;
    element.name=domElem.attribute("name");

    QDomNodeList ruleList =domElem.childNodes();

    // foreach rule
    for (int i=0; i<ruleList.count(); i++)
    {
        classRule rule;


        QDomNode ruleNode=ruleList.at(i);
        QDomNodeList ruleItemList =ruleNode.childNodes();


        for (int j=0; j<ruleItemList.count(); j++)
        {
            QDomElement ruleElem= ruleItemList.at(j).toElement();
            QString nodename=ruleElem.nodeName();

            if (nodename == "Filter")
            {
                rule.filter=ruleElem.text();
            }

            else if (nodename == "ElseFilter")
            {
                rule.elseFilter=true;
            }

            else if (nodename == "MinScaleDenominator")
            {
                rule.minScaleDenominator=ruleElem.text().toInt();
            }

            else if (nodename == "MaxScaleDenominator")
            {
                rule.maxScaleDenominator=ruleElem.text().toInt();
            }

            else if (nodename == "PointSymbolizer")
            {
                QDomNamedNodeMap attribs=ruleElem.attributes();
                for (int i=0; i<attribs.count(); i++)
                    rule.pointSymbolizer.insert(attribs.item(i).nodeName(), attribs.item(i).nodeValue());

            }

            else if (nodename == "LinePatternSymbolizer")
            {
                rule.linePatternSymbolizer=ruleElem.attributes().item(0).nodeValue();
            }

            else if (nodename == "PolygonPatternSymbolizer")
            {
                rule.polygonPatternSymbolizer=ruleElem.attributes().item(0).nodeValue();
            }

            else if (nodename == "TextSymbolizer")
            {
                QDomNamedNodeMap attribs=ruleElem.attributes();
                for (int i=0; i<attribs.count(); i++)
                    rule.textSymbolizer.insert(attribs.item(i).nodeName(), attribs.item(i).nodeValue());

                rule.textSymbolizer.insert("name" ,ruleElem.text());
            }

            else if (nodename == "ShieldSymbolizer")
            {
                QDomNamedNodeMap attribs=ruleElem.attributes();
                for (int i=0; i<attribs.count(); i++)
                    rule.shieldSymbolizer.insert(attribs.item(i).nodeName(), attribs.item(i).nodeValue());

                rule.shieldSymbolizer.insert("name" ,ruleElem.text());
            }

            else if (nodename == "LineSymbolizer")
            {
                if (rule.lineSymbolizer1.isEmpty())
                {
                    QDomNamedNodeMap attribs=ruleElem.attributes();
                    for (int i=0; i<attribs.count(); i++)
                        rule.lineSymbolizer1.insert(attribs.item(i).nodeName(), attribs.item(i).nodeValue());
                }
                else if (rule.lineSymbolizer2.isEmpty())
                {
                    QDomNamedNodeMap attribs=ruleElem.attributes();
                    for (int i=0; i<attribs.count(); i++)
                        rule.lineSymbolizer2.insert(attribs.item(i).nodeName(), attribs.item(i).nodeValue());
                }
                else if (rule.lineSymbolizer3.isEmpty())
                {
                    QDomNamedNodeMap attribs=ruleElem.attributes();
                    for (int i=0; i<attribs.count(); i++)
                        rule.lineSymbolizer3.insert(attribs.item(i).nodeName(), attribs.item(i).nodeValue());
                }
            }

            else if (nodename == "PolygonSymbolizer")
            {
                QDomNamedNodeMap attribs=ruleElem.attributes();
                for (int i=0; i<attribs.count(); i++)
                    rule.polygonSymbolizer.insert(attribs.item(i).nodeName(), attribs.item(i).nodeValue());
            }

            else if (nodename == "BuildingSymbolizer")
            {
                QDomNamedNodeMap attribs=ruleElem.attributes();
                for (int i=0; i<attribs.count(); i++)
                    rule.buildingSymbolizer.insert(attribs.item(i).nodeName(), attribs.item(i).nodeValue());
            }

            else if (nodename == "RasterSymbolizer")
            {
                // dunno
            }

            else if (nodename == "MarkersSymbolizer")
            {
                QDomNamedNodeMap attribs=ruleElem.attributes();
                for (int i=0; i<attribs.count(); i++)
                    rule.markersSymbolizer.insert(attribs.item(i).nodeName(), attribs.item(i).nodeValue());
            }

            else if (nodename == "DebugSymbolizer")
            {
                rule.debugSymbolizer=true;
            }

            else
                std::cerr <<"Unknown node detected!";

        }

        element.rules.append(rule);
    }


    styleList.append(element);
}

/**
 * @brief StyleSheet::addStyle adds new empty style
 * @param name new style name
 */
void StyleSheet::addStyle(QString name)
{
    TStyleElement element;
    element.name=name;
    styleList.append(element);
}


/**
 * @brief StyleSheet::addLayer adds new element into layerlist
 * @param domElem new element
 */
void StyleSheet::addLayer(QDomElement domElem)
{
    TLayerElement element;
    element.name=domElem.attribute("name");

    QDomNamedNodeMap nodemap= domElem.attributes();
    for (int i=0; i<nodemap.count(); i++)
    {
        if (nodemap.item(i).nodeName() == "name")
            continue;

        element.attributes.insert(nodemap.item(i).nodeName(), nodemap.item(i).nodeValue());
    }

    QDomNodeList childList =domElem.childNodes();
    for (int i=0; i<childList.count(); i++)
    {
        if (childList.at(i).nodeName() == "StyleName")
        {
            element.styleNames.append(childList.at(i).toElement().text());
        }

        if (childList.at(i).nodeName() == "Datasource")
        {
            QDomNodeList paramList =childList.at(i).childNodes();

            for (int j=0; j<paramList.count(); j++)
            {
                element.datasources.insert(paramList.at(j).attributes().item(0).nodeValue(), paramList.at(j).toElement().text());
            }
        }
    }

    layerList.append(element);
}

/**
 * @brief StyleSheet::addLayer adds new empty layer
 * @param name new layer name
 */
void StyleSheet::addLayer(QString name)
{
    TLayerElement element;
    element.name=name;
    layerList.append(element);
}

/**
 * @brief StyleSheet::modifyStyle modifies style
 * @param s name of the style to be replaced by new element
 * @param elem  new element
 */
void StyleSheet::modifyStyle(QString s, TStyleElement elem)
{
    int i;
    for (i=0; i<styleList.count(); i++)
    {
        if (styleList.at(i).name == s)
            break;
    }
    styleList.removeAt(i);
    styleList.insert(i,elem);
}

/**
 * @brief StyleSheet::modifyLayer modifies layer
 * @param s name of the layer to be replaced by new element
 * @param elem new element
 */
void StyleSheet::modifyLayer(QString s, TLayerElement elem)
{
    int i;
    for (i=0; i<layerList.count(); i++)
    {
        if (layerList.at(i).name == s)
            break;
    }
    layerList.removeAt(i);
    layerList.insert(i,elem);
}

/**
 * @brief StyleSheet::removeStyle removes style
 * @param s style name
 */
void StyleSheet::removeStyle(QString s)
{
    int i;
    for (i=0; i<styleList.count(); i++)
    {
        if (styleList.at(i).name == s)
            break;
    }
    styleList.removeAt(i);
}

/**
 * @brief StyleSheet::removeLayer removes layer
 * @param s layer name
 */
void StyleSheet::removeLayer(QString s)
{
    int i;
    for (i=0; i<layerList.count(); i++)
    {
        if (layerList.at(i).name == s)
            break;
    }
    layerList.removeAt(i);
}

/**
 * @brief StyleSheet::containsStyle checks existence of style
 * @param s name of style
 * @return true on success
 */
bool StyleSheet::containsStyle(QString s)
{
    for (int i=0; i<styleList.count(); i++)
    {
        if (styleList.at(i).name == s)
            return true;
    }
    return false;
}

/**
 * @brief StyleSheet::containsLayer checks existence of layer
 * @param s name of layer
 * @return true on success
 */
bool StyleSheet::containsLayer(QString s)
{
    for (int i=0; i<layerList.count(); i++)
    {
        if (layerList.at(i).name == s)
            return true;
    }
    return false;
}

/**
 * @brief StyleSheet::moveLayerUp moves layer up
 * @param row index of row
 */
void StyleSheet::moveLayerUp(int row)
{
    layerList.move(row, row-1);
}

/**
 * @brief StyleSheet::moveLayerDown moves layer down
 * @param row index of row
 */
void StyleSheet::moveLayerDown(int row)
{
    layerList.move(row, row+1);
}

/**
 * @brief StyleSheet::moveStyleUp moves style up
 * @param row index of row
 */
void StyleSheet::moveStyleUp(int row)
{
    styleList.move(row, row-1);
}

/**
 * @brief StyleSheet::moveStyleDown moves style down
 * @param row index of row
 */
void StyleSheet::moveStyleDown(int row)
{
    styleList.move(row, row+1);
}


/**
 * @brief StyleSheet::convertRuleToElement converts rule to element
 * @param rule input rule
 * @param doc parent QDomDocument
 * @return new QDomElement
 */
QDomElement StyleSheet::convertRuleToElement(classRule rule, QDomDocument doc)
{
    QDomElement elem = doc.createElement("Rule");

    //########## Filter
    if (rule.filter != "")
    {
        QDomElement e = doc.createElement("Filter");
        QDomText t=doc.createTextNode(rule.filter);
        e.appendChild(t);
        elem.appendChild(e);
    }

    //########## ElseFilter
    if (rule.elseFilter != false)
    {
        QDomElement e = doc.createElement("ElseFilter");
        elem.appendChild(e);
    }

    //########## MinScaleDenominator
    if (rule.minScaleDenominator != 0)
    {
        QDomElement e = doc.createElement("MinScaleDenominator");
        QDomText t=doc.createTextNode(QString::number(rule.minScaleDenominator));
        e.appendChild(t);
        elem.appendChild(e);
    }

    //########## MaxScaleDenominator
    if (rule.maxScaleDenominator != 0)
    {
        QDomElement e = doc.createElement("MaxScaleDenominator");
        QDomText t=doc.createTextNode(QString::number(rule.maxScaleDenominator));
        e.appendChild(t);
        elem.appendChild(e);
    }

    //########## PointSymbolizer
    if (!rule.pointSymbolizer.isEmpty())
    {
        QDomElement e = doc.createElement("PointSymbolizer");

        foreach (const QString &s,rule.pointSymbolizer.keys())
        {
            e.setAttribute(s,rule.pointSymbolizer.value(s));
        }
        elem.appendChild(e);
    }

    //########## LinePatternSymbolizer
    if (!rule.linePatternSymbolizer.isEmpty())
    {
        QDomElement e = doc.createElement("LinePatternSymbolizer");
        e.setAttribute("file",rule.linePatternSymbolizer);
        elem.appendChild(e);
    }

    //########## PolygonPatternSymbolizer
    if (!rule.polygonPatternSymbolizer.isEmpty())
    {
        QDomElement e = doc.createElement("PolygonPatternSymbolizer");
        e.setAttribute("file",rule.polygonPatternSymbolizer);
        elem.appendChild(e);
    }

    //########## TextSymbolizer
    if (!rule.textSymbolizer.isEmpty())
    {
        QDomElement e = doc.createElement("TextSymbolizer");
        QDomText t=doc.createTextNode(rule.textSymbolizer.value("name"));
        e.appendChild(t);
        rule.textSymbolizer.remove("name");

        foreach (const QString &s,rule.textSymbolizer.keys())
            e.setAttribute(s,rule.textSymbolizer.value(s));
        elem.appendChild(e);
    }

    //########## ShieldSymbolizer
    if (!rule.shieldSymbolizer.isEmpty())
    {
        QDomElement e = doc.createElement("ShieldSymbolizer");
        QDomText t=doc.createTextNode(rule.shieldSymbolizer.value("name"));
        e.appendChild(t);
        rule.shieldSymbolizer.remove("name");

        foreach (const QString &s,rule.shieldSymbolizer.keys())
            e.setAttribute(s,rule.shieldSymbolizer.value(s));
        elem.appendChild(e);
    }

    //########## LineSymbolizer
    if (!rule.lineSymbolizer1.isEmpty())
    {
        QDomElement e = doc.createElement("LineSymbolizer");
        foreach (const QString &s,rule.lineSymbolizer1.keys())
            e.setAttribute(s,rule.lineSymbolizer1.value(s));
        elem.appendChild(e);
    }

    if (!rule.lineSymbolizer2.isEmpty())
    {
        QDomElement e = doc.createElement("LineSymbolizer");
        foreach (const QString &s,rule.lineSymbolizer2.keys())
            e.setAttribute(s,rule.lineSymbolizer2.value(s));
        elem.appendChild(e);
    }

    if (!rule.lineSymbolizer3.isEmpty())
    {
        QDomElement e = doc.createElement("LineSymbolizer");
        foreach (const QString &s,rule.lineSymbolizer3.keys())
            e.setAttribute(s,rule.lineSymbolizer3.value(s));
        elem.appendChild(e);
    }

    //########## PolygonSymbolizer
    if (!rule.polygonSymbolizer.isEmpty())
    {
        QDomElement e = doc.createElement("PolygonSymbolizer");
        foreach (const QString &s,rule.polygonSymbolizer.keys())
            e.setAttribute(s,rule.polygonSymbolizer.value(s));
        elem.appendChild(e);
    }

    //########## BuildingSymbolizer
    if (!rule.buildingSymbolizer.isEmpty())
    {
        QDomElement e = doc.createElement("BuildingSymbolizer");
        foreach (const QString &s,rule.buildingSymbolizer.keys())
            e.setAttribute(s,rule.buildingSymbolizer.value(s));
        elem.appendChild(e);
    }

    //########## MarkersSymbolizer
    if (!rule.markersSymbolizer.isEmpty())
    {
        QDomElement e = doc.createElement("MarkersSymbolizer");
        foreach (const QString &s,rule.markersSymbolizer.keys())
            e.setAttribute(s,rule.markersSymbolizer.value(s));
        elem.appendChild(e);
    }

    //########## DebugSymbolizer
    if (rule.debugSymbolizer != false)
    {
        QDomElement e = doc.createElement("DebugSymbolizer");
        elem.appendChild(e);
    }

    return elem;
}


/**
 * @brief StyleSheet::convertStyleToElement converts style to element
 * @param style input style
 * @param doc parent QDomDocument
 * @return new QDomElement
 */
QDomElement StyleSheet::convertStyleToElement(TStyleElement style, QDomDocument doc)
{
    QDomElement elem = doc.createElement("Style");
    elem.setAttribute("name",style.name);

    // for each rule
    for (int i=0; i<style.rules.count(); i++)
    {
        QDomElement ruleelem = convertRuleToElement(style.rules.at(i),doc);
        elem.appendChild(ruleelem);
    }
    return elem;
}


/**
 * @brief StyleSheet::convertLayerToElement converts layer to element
 * @param layer input layer
 * @param doc parent QDomDocument
 * @return new QDomElement
 */
QDomElement StyleSheet::convertLayerToElement(TLayerElement layer, QDomDocument doc)
{
    QDomElement elem = doc.createElement("Layer");

    elem.setAttribute("name",layer.name);

    foreach (const QString &s,layer.attributes.keys())
        elem.setAttribute(s, layer.attributes.value(s));

    foreach (const QString &s,layer.styleNames)
    {
        QDomElement e = doc.createElement("StyleName");
        QDomText t=doc.createTextNode(s);
        e.appendChild(t);
        elem.appendChild(e);
    }

    QDomElement elemData = doc.createElement("Datasource");
    elem.appendChild(elemData);

    foreach (const QString &s,layer.datasources.keys())
    {
        QDomElement elemParam = doc.createElement("Parameter");
        QDomText t=doc.createTextNode(layer.datasources.value(s));
        elemParam.appendChild(t);
        elemParam.setAttribute("name",s);
        elemData.appendChild(elemParam);
    }

    return elem;
}

/**
 * @brief StyleSheet::getStyleElem returns desired style
 * @param s name of style
 * @return desired style
 */
TStyleElement StyleSheet::getStyleElem(QString s)
{
    TStyleElement empty;
    for (int i=0; i<styleList.count(); i++)
    {
        if (styleList.at(i).name == s)
            return styleList.at(i);
    }
    return empty;
}

/**
 * @brief StyleSheet::getLayerElem returns desired layer
 * @param s name of layer
 * @return desired layer
 */
TLayerElement StyleSheet::getLayerElem(QString s)
{
    TLayerElement empty;
    for (int i=0; i<layerList.count(); i++)
    {
        if (layerList.at(i).name == s)
            return layerList.at(i);
    }
    return empty;
}

/**
 * @brief classRule::classRule constructor method sets default values
 */
classRule::classRule()
{
    filter=QString();
    elseFilter=false;
    minScaleDenominator=0;
    maxScaleDenominator=0;

    linePatternSymbolizer=QString();
    polygonPatternSymbolizer=QString();
    debugSymbolizer=false;
}
