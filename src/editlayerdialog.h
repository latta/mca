/**
  * @file editlayerdialog.h
  * @author Martin Latta
  * Map Creation Application from OpenStreetMap Project Data
  * Faculty of Information Technology
  * Brno University of Technology
  * 2013
  * @brief dialog to edit selected layer
  */

#ifndef EDITLAYERDIALOG_H
#define EDITLAYERDIALOG_H

#include "stylesheet.h"

#include <QDialog>

namespace Ui {
class EditLayerDialog;
}

/**
 * @brief The EditLayerDialog class representing dialog to edit selected layer
 */
class EditLayerDialog : public QDialog
{
    Q_OBJECT
    
public:
    explicit EditLayerDialog(TLayerElement elem, QStringList list, QWidget *parent = 0);
    ~EditLayerDialog();

    TLayerElement element; /**< element with layer information */

private:
    Ui::EditLayerDialog *ui;

    QStringList listOfStyles; /**< list of styles */
    QString path;

private slots:
    void save();
    void addStyle();
    void removeStyle();
    void browseShape();
    void browseRaster();
    void browseGdal();
    void browseOgr();
    void browseOsm();
    void selectSrs();

};

#endif // EDITLAYERDIALOG_H
