/**
  * @file optionsdialog.cpp
  * @author Martin Latta
  * Map Creation Application from OpenStreetMap Project Data
  * Faculty of Information Technology
  * Brno University of Technology
  * 2013
  * @brief dialog to set application settings
  */

#include "optionsdialog.h"
#include "ui_optionsdialog.h"

/**
 * @brief OptionsDialog::OptionsDialog constructor method
 * @param s1 default OSM filename
 * @param s2 default Stylesheet name
 * @param url default URL
 * @param ratio export ration
 * @param parent
 */
OptionsDialog::OptionsDialog(QString s1, QString s2, QString url, double ratio, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::OptionsDialog)
{
    ui->setupUi(this);

    this->setWindowTitle("Options");

    defaultOSMfilename = s1;
    defaultStylesheetName= s2;
    defaultURL = url;
    exportRatio = ratio;
    ui->lineEdit1->setText(s1);
    ui->lineEdit2->setText(s2);
    ui->textEdit->setPlainText(url);
    ui->doubleSpinBox->setValue(ratio);

    connect(this, SIGNAL(accepted()),this, SLOT(save()));
}

/**
 * @brief OptionsDialog::~OptionsDialog destructor
 */
OptionsDialog::~OptionsDialog()
{
    delete ui;
}

/**
 * @brief OptionsDialog::save saves dialog content
 */
void OptionsDialog::save()
{
    defaultOSMfilename=ui->lineEdit1->text();
    defaultStylesheetName=ui->lineEdit2->text();
    defaultURL= ui->textEdit->toPlainText();
    exportRatio=ui->doubleSpinBox->value();
}

/**
 * @brief OptionsDialog::on_pushButton_clicked handle button event
 */
void OptionsDialog::on_pushButton_clicked()
{
    ui->lineEdit1->setText("tmp_map.osm");
    ui->lineEdit2->setText("tmp_osm.xml");
    ui->textEdit->setPlainText("http://www.openstreetmap.org/api/0.6/map?bbox=minlon,minlat,maxlon,maxlat");
    ui->doubleSpinBox->setValue(1.414213);
}
